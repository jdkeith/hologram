﻿using System;
using System.IO;
using System.Linq;
using JLabs.Structures;

namespace Driver
{
	public static class AddressTypeExtensions
	{
		private const byte SizeMask = 0x3F;
		private const byte SizeTypeMask = 0xC0;

		public static void ExtractSize(this AddressType typeAndLength, out AddressType rawAddressType, out byte length)
		{
			if ((byte) typeAndLength < 0x40)
			{
				rawAddressType = typeAndLength;
				length = 0;
				return;
			}

			rawAddressType = (AddressType) ((byte) typeAndLength & SizeTypeMask);
			length = (byte) ((byte) typeAndLength & SizeMask);
		}

		public static AddressType CombineWithSize(this AddressType rawAddressType, byte length)
		{
			if ((byte) rawAddressType < 0x40)
			{
				// these types do not have sizes
				if( ! Enum.IsDefined(typeof(AddressType), rawAddressType) )
					throw new ArgumentException("Given address type is not defined", "rawAddressType");

				return rawAddressType;
			}

			if (length > SizeMask)
			{
				throw new ArgumentOutOfRangeException(
					string.Format("Length must be less than or equal to {0}", SizeMask)
				);
			}

			return (AddressType) ((((byte) rawAddressType) & SizeTypeMask) | length);
		}
	}

	// this also doubles as length
	public enum AddressType : byte
	{
		SingleDefault = 0x00,
		SingleExplicit = 0x10,
		MultiSigOneOfTwo = 0x12,
		MultiSigOneOfThree = 0x13,
		MultiSigTwoOfTwo = 0x14,
		MultiSigTwoOfThree = 0x15,
		MultiSigThreeOfThree = 0x17,

		Anonymous = 0x20,
		Script = 0x21,
		Vote = 0x3f,

		Registration = 0x40, // + ?
		MinterRegistration = 0x80, // + ?
		Destroy = 0xC0 // + ?
	}
}