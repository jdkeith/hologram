﻿using System;

namespace JLabs.Cryptography.EC
{
	/// <summary>
	/// The exception which is thrown when an operation is performed between
	/// two points which are on different curves
	/// </summary>
	public class PointMismatchException : Exception
	{
		/// <summary>
		/// Creates a new PointMismatchException
		/// </summary>
		/// <param name="message">A human-readable error message</param>
		/// <param name="innerException">An exception which is the cause of the exception</param>
		public PointMismatchException(string message = null, Exception innerException = null)
			: base(message, innerException) { }
	}

	/// <summary>
	/// The exception which is thrown when a decryption checksum fails
	/// </summary>
	public class CannotDecryptException : Exception
	{
		/// <summary>
		/// Creates a new CannotDecryptException
		/// </summary>
		/// <param name="message">A human-readable error message</param>
		/// <param name="innerException">An exception which is the cause of the exception</param>
		public CannotDecryptException(string message = null, Exception innerException = null)
			: base(message, innerException) { }
	}
}