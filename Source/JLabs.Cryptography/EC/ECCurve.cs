﻿using System;
using System.Globalization;
using System.IO;
using System.Numerics;

using JLabs.Structures;

namespace JLabs.Cryptography.EC
{
	// ReSharper disable once InconsistentNaming
	public enum ECCurveType : byte
	{
		Custom = 0x00,

		// ReSharper disable InconsistentNaming
		SECP256R1 = 0x10,
		SECP256K1 = 0x11,
		SECP384R1 = 0x14,
		SECP521R1 = 0x18,
		// ReSharper restore InconsistentNaming

		BrainPoolP256R1 = 0x40,
		BrainPoolP320R1 = 0x44,
		BrainPoolP384R1 = 0x48,
		BrainPoolP512R1 = 0x4C
	}

	/// <summary>
	/// Represents an elliptical curve
	/// </summary>
	// ReSharper disable InconsistentNaming
	public class ECCurve : IEquatable<ECCurve>, IDeserializer<ECPoint>
	{
		#region Well-Known Parameters

		public static ECCurve GetByCurveType(ECCurveType type)
		{
			switch (type)
			{
				case ECCurveType.SECP256R1:
					return SECP256R1;

				case ECCurveType.SECP256K1:
					return SECP256K1;

				case ECCurveType.SECP384R1:
					return SECP384R1;

				case ECCurveType.SECP521R1:
					return SECP521R1;

				case ECCurveType.BrainPoolP256R1:
					return BrainPoolP256R1;

				case ECCurveType.BrainPoolP320R1:
					return BrainPoolP320R1;

				case ECCurveType.BrainPoolP384R1:
					return BrainPoolP384R1;

				case ECCurveType.BrainPoolP512R1:
					return BrainPoolP512R1;

				default:
					throw new ArgumentException("Cannot get domain parameters for custom curve");
			}
		}

		public static ECCurve SECP256R1
		{
			get
			{
				var q = BigInteger.Parse("00FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF", NumberStyles.HexNumber);
				var a = BigInteger.Parse("00FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC", NumberStyles.HexNumber);
				var b = BigInteger.Parse("005AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B", NumberStyles.HexNumber);

				var n = BigInteger.Parse("00FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551", NumberStyles.HexNumber);
				var h = BigInteger.One;

				var gx = BigInteger.Parse("006B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296", NumberStyles.HexNumber);
				var gy = BigInteger.Parse("004FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5", NumberStyles.HexNumber);

				return new ECCurve(ECCurveType.SECP256R1, q, n, h, a, b, gx, gy);
			}
		}

		public static ECCurve SECP256K1
		{
			get
			{
				var q = BigInteger.Parse("00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F", NumberStyles.HexNumber);
				var a = BigInteger.Zero;
				var b = (BigInteger)7;

				var n = BigInteger.Parse("00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141", NumberStyles.HexNumber);
				var h = BigInteger.One;

				var gx = BigInteger.Parse("0079BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798", NumberStyles.HexNumber);
				var gy = BigInteger.Parse("00483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8", NumberStyles.HexNumber);

				return new ECCurve(ECCurveType.SECP256K1, q, n, h, a, b, gx, gy);
			}
		}

		public static ECCurve SECP384R1
		{
			get
			{
				var q = BigInteger.Parse(
					"00FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"00FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"005AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"00FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"006B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"004FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5",
					NumberStyles.HexNumber
				);

				return new ECCurve(ECCurveType.SECP384R1, q, n, h, a, b, gx, gy);
			}
		}

		public static ECCurve SECP521R1
		{
			get
			{
				var q = BigInteger.Parse(
					"00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFF",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFC",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"00B3312FA7E23EE7E4988E056BE3F82D19181D9C6EFE8141120314088F5013875AC656398D8A2ED19D2A85C8EDD3EC2AEF",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7634D81F4372DDF581A0DB248B0A77AECEC196ACCC52973",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"00AA87CA22BE8B05378EB1C71EF320AD746E1D3B628BA79B9859F741E082542A385502F25DBF55296C3A545E3872760AB7",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"003617DE4A96262C6F5D9E98BF9292DC29F8F41DBD289A147CE9DA3113B5F0B8C00A60B1CE1D7E819D7A431D7C90EA0E5F",
					NumberStyles.HexNumber
				);

				return new ECCurve(ECCurveType.SECP521R1, q, n, h, a, b, gx, gy);
			}
		}

		public static ECCurve BrainPoolP256R1
		{
			get
			{
				var q = BigInteger.Parse("00A9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377", NumberStyles.HexNumber);
				var a = BigInteger.Parse("007D5A0975FC2C3057EEF67530417AFFE7FB8055C126DC5C6CE94A4B44F330B5D9", NumberStyles.HexNumber);
				var b = BigInteger.Parse("0026DC5C6CE94A4B44F330B5D9BBD77CBF958416295CF7E1CE6BCCDC18FF8C07B6", NumberStyles.HexNumber);

				var n = BigInteger.Parse("00A9FB57DBA1EEA9BC3E660A909D838D718C397AA3B561A6F7901E0E82974856A7", NumberStyles.HexNumber);
				var h = BigInteger.One;

				var gx = BigInteger.Parse("008BD2AEB9CB7E57CB2C4B482FFC81B7AFB9DE27E1E3BD23C23A4453BD9ACE3262", NumberStyles.HexNumber);
				var gy = BigInteger.Parse("00547EF835C3DAC4FD97F8461A14611DC9C27745132DED8E545C1D54C72F046997", NumberStyles.HexNumber);

				return new ECCurve(ECCurveType.BrainPoolP256R1, q, n, h, a, b, gx, gy);
			}
		}

		public static ECCurve BrainPoolP320R1
		{
			get
			{
				var q = BigInteger.Parse(
					"00D35E472036BC4FB7E13C785ED201E065F98FCFA6F6F40DEF4F92B9EC7893EC28FCD412B1F1B32E27",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"003EE30B568FBAB0F883CCEBD46D3F3BB8A2A73513F5EB79DA66190EB085FFA9F492F375A97D860EB4",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"00520883949DFDBC42D3AD198640688A6FE13F41349554B49ACC31DCCD884539816F5EB4AC8FB1F1A6",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"00D35E472036BC4FB7E13C785ED201E065F98FCFA5B68F12A32D482EC7EE8658E98691555B44C59311",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"0043BD7E9AFB53D8B85289BCC48EE5BFE6F20137D10A087EB6E7871E2A10A599C710AF8D0D39E20611",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"0014FDD05545EC1CC8AB4093247F77275E0743FFED117182EAA9C77877AAAC6AC7D35245D1692E8EE1",
					NumberStyles.HexNumber
				);

				return new ECCurve(ECCurveType.BrainPoolP320R1, q, n, h, a, b, gx, gy);
			}
		}


		public static ECCurve BrainPoolP384R1
		{
			get
			{
				var q = BigInteger.Parse(
					"008CB91E82A3386D280F5D6F7E50E641DF152F7109ED5456B412B1DA197FB71123ACD3A729901D1A71874700133107EC53",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"007BC382C63D8C150C3C72080ACE05AFA0C2BEA28E4FB22787139165EFBA91F90F8AA5814A503AD4EB04A8C7DD22CE2826",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"004A8C7DD22CE28268B39B55416F0447C2FB77DE107DCD2A62E880EA53EEB62D57CB4390295DBC9943AB78696FA504C11",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"008CB91E82A3386D280F5D6F7E50E641DF152F7109ED5456B31F166E6CAC0425A7CF3AB6AF6B7FC3103B883202E9046565",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"001D1C64F068CF45FFA2A63A81B7C13F6B8847A3E77EF14FE3DB7FCAFE0CBD10E8E826E03436D646AAEF87B2E247D4AF1E",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"008ABE1D7520F9C2A45CB1EB8E95CFD55262B70B29FEEC5864E19C054FF99129280E4646217791811142820341263C5315",
					NumberStyles.HexNumber
				);

				return new ECCurve(ECCurveType.BrainPoolP384R1, q, n, h, a, b, gx, gy);
			}
		}

		public static ECCurve BrainPoolP512R1
		{
			get
			{
				var q = BigInteger.Parse(
					"00AADD9DB8DBE9C48B3FD4E6AE33C9FC07CB308DB3B3C9D20ED6639CCA703308717D4D9B009BC66842AECDA12AE6A380E62881FF2F2D82C68528AA6056583A48F3",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"007830A3318B603B89E2327145AC234CC594CBDD8D3DF91610A83441CAEA9863BC2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A72BF2C7B9E7C1AC4D77FC94CA",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"003DF91610A83441CAEA9863BC2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A72BF2C7B9E7C1AC4D77FC94CADC083E67984050B75EBAE5DD2809BD638016F723",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"00AADD9DB8DBE9C48B3FD4E6AE33C9FC07CB308DB3B3C9D20ED6639CCA70330870553E5C414CA92619418661197FAC10471DB1D381085DDADDB58796829CA90069",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"0081AEE4BDD82ED9645A21322E9C4C6A9385ED9F70B5D916C1B43B62EEF4D0098EFF3B1F78E2D0D48D50D1687B93B97D5F7C6D5047406A5E688B352209BCB9F822",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"007DDE385D566332ECC0EABFA9CF7822FDF209F70024A57B1AA000C55B881F8111B2DCDE494A5F485E5BCA4BD88A2763AED1CA2B2FA8F0540678CD1E0F3AD80892",
					NumberStyles.HexNumber
				);

				return new ECCurve(ECCurveType.BrainPoolP512R1, q, n, h, a, b, gx, gy);
			}
		}


		#endregion Well-Known Parameters

		#region Constructors

		/// <summary>
		/// Creates a new elliptic curve
		/// </summary>
		/// <param name="q">The domain of the curve's finite field</param>
		/// <param name="n">The number of points on the curve</param>
		/// <param name="h">To cofactor of n</param>
		/// <param name="a">The A coefficient in Y^2 = x^3 + ax + b</param>
		/// <param name="b">The B coefficient in Y^2 = x^3 + ax + b</param>
		/// <param name="gx">The x coordinate of the generator point</param>
		/// <param name="gy">The y coordinate of the generator point</param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// <para>Thrown if</para>
		/// <para>* q is not a positive number</para>
		/// <para>* n is not a positive number</para>
		/// <para>* a is not a non-negative number</para>
		/// <para>* a is greater than or equal to q</para>
		/// <para>* b is not a non-negative number</para>
		/// <para>* b is greater than or equal to q</para>
		/// <para>* h is not a non-negative number</para>
		/// <para>* h is greater than or equal to n</para>
		/// </exception>
		public ECCurve(BigInteger q, BigInteger n, BigInteger h, BigInteger a, BigInteger b, BigInteger gx, BigInteger gy)
		{
			if (q.Sign <= 0)
				throw new ArgumentOutOfRangeException("q", "Q must be a positive number");

			if (n.Sign <= 0)
				throw new ArgumentOutOfRangeException("n", "N must be a positive number");

			if (a.Sign < 0)
				throw new ArgumentOutOfRangeException("a", "A must be a non-negative number");

			if (a >= q)
				throw new ArgumentOutOfRangeException("a", "A must be less than Q");

			if (b.Sign < 0)
				throw new ArgumentOutOfRangeException("b", "B must be a non-negative number");

			if (b >= q)
				throw new ArgumentOutOfRangeException("b", "B must be less than Q");

			if (h <= 0)
				throw new ArgumentOutOfRangeException("h", "H must be a positive number");

			if (h >= n)
				throw new ArgumentOutOfRangeException("h", "H must be less than N");

			Q = q;
			N = n;
			H = h;
			
			A = a;
			B = b;

			G = CreatePoint(gx, gy);
			Infinity = new ECPoint(this);

			Type = ECCurveType.Custom;
		}

		private ECCurve(
			ECCurveType type,
			BigInteger q, BigInteger n, BigInteger h,
			BigInteger a, BigInteger b, BigInteger gx, BigInteger gy
		) : this(q, n, h, a, b, gx, gy)
		{
			Type = type;
		}

		#endregion Constructors

		#region Non-Point Parameters

		/// <summary>
		/// The domain of the curve's finite field
		/// </summary>
		public BigInteger Q { get; private set; }

		/// <summary>
		/// The number of points on the curve
		/// </summary>
		public BigInteger N { get; private set; }

		/// <summary>
		/// The cofactor of N
		/// </summary>
		public BigInteger H { get; private set; }

		/// <summary>
		/// The A coefficient in Y^2 = x^3 + ax + b
		/// </summary>
		public BigInteger A { get; private set; }

		/// <summary>
		/// The B coefficient in Y^2 = x^3 + ax + b
		/// </summary>
		public BigInteger B { get; private set; }

		/// <summary>
		/// The well-known type of the curve
		/// </summary>
		public ECCurveType Type { get; private set; }

		#endregion Non-Point Parameters

		#region Points

		/// <summary>
		/// The point at infinity for the curve
		/// </summary>
		public ECPoint Infinity { get; private set; }

		/// <summary>
		/// The generator point for the curve
		/// </summary>
		public ECPoint G { get; private set; }

		/// <summary>
		/// Creates a point on the curve
		/// </summary>
		/// <param name="x">The x coordinate of the point</param>
		/// <param name="y">The y coordinate of the point</param>
		/// <returns>A new point associated with the curve</returns>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the point is not on the given curve
		/// </exception>
		public ECPoint CreatePoint(BigInteger x, BigInteger y)
		{
			var fex = new FieldElement(x, Q);
			var fey = new FieldElement(y, Q);
			var notOnCurveEx = new ArgumentException("Point is not on the given curve");
			
			try
			{
				var testY = ComputeY(fex);

				if (testY != fey && -testY != fey)
					throw notOnCurveEx;

				return new ECPoint(this, fex, fey);
			}
			catch (ArithmeticException)
			{
				throw notOnCurveEx;
			}
		}

		/// <summary>
		/// Creates a point on the curve
		/// </summary>
		/// <param name="x">The x coordinate of the point</param>
		/// <param name="yIsOdd">
		/// True if the odd value of y is to be used, false if the even value of y is to be used
		/// </param>
		/// <returns>A new point associated with the curve</returns>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the point is not on the given curve
		/// </exception>
		public ECPoint CreatePoint(BigInteger x, bool yIsOdd = false)
		{
			var fex = new FieldElement(x, Q);

			try
			{
				var fey = ComputeY(fex);

				if (fey.Value.IsEven ^ !yIsOdd)
					fey = -fey;

				return new ECPoint(this, fex, fey);
			}
			catch(ArithmeticException)
			{
				throw new ArgumentException("Point is not on the curve");
			}
		}

		/// <summary>
		/// Decodes a serialized point into a point on the curve
		/// </summary>
		/// <param name="serialized">The serialized point</param>
		/// <returns>The point represented by the serialized data if it lies on the curve</returns>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the point representation is invalid or
		/// if the point does not lie along the curve
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the serialized parameter is null
		/// </exception>
		public ECPoint Deserialize(byte[] serialized)
		{
			if (serialized == null )
				throw new ArgumentNullException("serialized");

			using (var ms = new MemoryStream())
			{
				ms.Write(serialized, 0, serialized.Length);
				ms.Seek(0, SeekOrigin.Begin);
				return ReadFrom(ms);
			}
		}

		public ECPoint ReadFrom(Stream s)
		{
			if( s == null )
				throw new ArgumentNullException("s");

			var format = s.ReadByte();

			var invalidEx = new FormatException("Invalid point representation");

			if (format < 0x02 || format > 0x04)
				throw invalidEx;

			var br = new BinaryReader(s);

			var domainSizeInBytes = (int)Math.Ceiling((double)Q.GetBitLength() / 8);

			var x = br.ReadBytes(domainSizeInBytes).ToNonNegativeBigInteger(reverse: true);
			var fex = new FieldElement(x, Q);

			if (format != 0x04)
				return CreatePoint(fex, format == 0x03);

			var y = br.ReadBytes(domainSizeInBytes).ToNonNegativeBigInteger(reverse: true);

			return CreatePoint(fex, new FieldElement(y, Q));
		}

		/// <summary>
		/// Determines whether or not a given point is on the curve
		/// </summary>
		/// <param name="point">The point to test</param>
		/// <returns>
		/// True if the point is not null and is on the curve, false otherwise
		/// </returns>
		public bool IsOnCurve(ECPoint point)
		{
			if (point == null)
				return false;

			return point.Curve == this;
		}

		// Negate the Y coordinate to get the other possible value</remarks>
		private FieldElement ComputeY(FieldElement x)
		{
			if (x == null)
				throw new ArgumentNullException("x");

			// x*(x^2 + a)+b = x^3+ax+b
			return (x * (x.Square() + A) + B).SquareRoot();
		}

		#endregion Points

		#region Equality

		/// <summary>
		/// Compares two curves for equality
		/// </summary>
		/// <param name="a">The first curve to compare</param>
		/// <param name="b">The second curve to compare</param>
		/// <returns>
		/// True if both curves have the same domain and A and B values
		/// or if both are null, false otherwise
		/// </returns>
		public static bool operator ==(ECCurve a, ECCurve b)
		{
			if (ReferenceEquals(a, null))
				return ReferenceEquals(b, null);

			return a.Equals(b);
		}

		/// <summary>
		/// Compares two curves for equality
		/// </summary>
		/// <param name="a">The first curve to compare</param>
		/// <param name="b">The second curve to compare</param>
		/// <returns>
		/// False if both curves have the same domain and A and B values
		/// or if both are null, true otherwise
		/// </returns>
		public static bool operator !=(ECCurve a, ECCurve b)
		{
			return !(a == b);
		}

		/// <summary>
		/// Compares the curve to another curve
		/// </summary>
		/// <param name="other">The other curve to compare to</param>
		/// <returns>
		/// True if the other curve is not null and has
		/// the same domain and A and B values as the invocant
		/// </returns>
		public bool Equals(ECCurve other)
		{
			if (ReferenceEquals(other, null))
				return false;

			return Q == other.Q
				&& A == other.A
				&& B == other.B;
		}

		/// <summary>
		/// Compares the curve to another object
		/// </summary>
		/// <param name="obj">The other curve to compare to</param>
		/// <returns>
		/// True if the other object is a non-null curve and has
		/// the same domain and A and B values as the invocant
		/// </returns>
		public override bool Equals(object obj)
		{
			return Equals(obj as ECCurve);
		}

		/// <summary>
		/// Serves as a hash function for organizing curves
		/// </summary>
		/// <returns>A number which is determined by the domain and A and B values of the curve</returns>
		public override int GetHashCode()
		{
			return Q.GetHashCode() ^ A.GetHashCode() ^ B.GetHashCode();
		}

		#endregion Equality

		#region Conversion

		/// <summary>
		/// Returns a System.String that represents the curve
		/// </summary>
		/// <returns>A string-representation of the curve</returns>
		public override string ToString()
		{
			if( Type == ECCurveType.Custom )
				return string.Format("Y\u00B2 = X\u00B3 + {0}X + {1}", A, B);

			return Type.ToString();
		}

		#endregion Conversion
	} 
}