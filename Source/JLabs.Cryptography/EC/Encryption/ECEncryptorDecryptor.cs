﻿using System;
using System.Numerics;
using System.Security.Cryptography;

namespace JLabs.Cryptography.EC.Encryption
{
	/// <summary>
	/// Provides high(ish) level methods for encrypting and decrypting data
	/// </summary>
	// ReSharper disable once InconsistentNaming
	public class ECEncryptorDecryptor
	{
		#region Fields

		private static readonly RandomNumberGenerator _defaultRng;

		private readonly SymmetricEncryptorDecryptor _ed;
		private readonly ECCurve _curve;
		private readonly RandomNumberGenerator _rng;

		#endregion Fields

		#region Constructors

		static ECEncryptorDecryptor()
		{
			_defaultRng = new RNGCryptoServiceProvider();
		}

		/// <summary>
		/// Creates a new encrypter with the given domain parameters
		/// </summary>
		/// <param name="curve">The elliptic curve to use for the encrypter</param>
		/// <param name="rng">
		/// The random number generator to use for generating K values. If unspecifed,
		/// a singleton instance of RNGCryptoServiceProvider is used.
		/// </param>
		/// <param name="symmetricEncryptor">
		/// The symmetric encryptor/decryptor to perform encryption/decryption
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the parameters parameter is null
		/// </exception>
		public ECEncryptorDecryptor(
			ECCurve curve, RandomNumberGenerator rng = null,
			SymmetricEncryptorDecryptor symmetricEncryptor = null
		)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			_curve = curve;
			_rng = rng ?? _defaultRng;
			_ed = symmetricEncryptor ?? SymmetricEncryptorDecryptor.Rijnadael;
		}

		/// <summary>
		/// Creates a new signer with the given well-known domain parameters
		/// </summary>
		/// <param name="curveType">The type of elliptic curve to use for the signer</param>
		/// <param name="rng">
		/// The random number generator to use for generating K values. If unspecifed,
		/// a singleton instance of RNGCryptoServiceProvider is used.
		/// </param>
		/// <param name="symmetricEncryptor">
		/// The symmetric encryptor/decryptor to perform encryption/decryption
		/// </param>
		/// <exception cref="System.ArgumentException">
		/// Thrown if curveType parameter is Custom
		/// </exception>
		public ECEncryptorDecryptor(
			ECCurveType curveType, RandomNumberGenerator rng = null,
			SymmetricEncryptorDecryptor symmetricEncryptor = null
		)
			: this(ECCurve.GetByCurveType(curveType), rng, symmetricEncryptor)
		{ }

		#endregion Constructors

		#region Encrypt / Decrypt

		/// <summary>
		/// Encrypts a message to a given public key
		/// </summary>
		/// <param name="message">The data to encrypt</param>
		/// <param name="publicKey">
		/// The public key to encrypt the message to, only the associated private key
		/// will be able to decrypt it
		/// </param>
		/// <returns>An encrypted payload</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message or public key parameters are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on encrypter construction
		/// </exception>
		public ECEncryptedMessage EncryptTo(byte[] message, ECPoint publicKey)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			if (publicKey == null)
				throw new ArgumentNullException("publicKey");

			if( ! _curve.IsOnCurve(publicKey) )
				throw new PointMismatchException("Public key must be on the curve specified in the constructor parameters");

			var biasBuffer = new byte[32];
			_rng.GetBytes(biasBuffer);
			var bias = biasBuffer.ToNonNegativeBigInteger();

			var secretPoint = publicKey * bias;
			var publicPoint = _curve.G * bias;

			var key = new byte[_ed.KeySize];
			var copyIntoKey = secretPoint.X.Value.ToByteArray();
			Array.Copy(copyIntoKey, key, Math.Min(key.Length, copyIntoKey.Length));

			var cipherMessage = _ed.Encrypt(message, key);

			return new ECEncryptedMessage(publicPoint, cipherMessage);
		}

		/// <summary>
		/// Decrypts an encrypted message
		/// </summary>
		/// <param name="payload">The encrypted payload to decrypt</param>
		/// <param name="privateKey">
		/// The private key associated with the public key the message was encrypted to
		/// </param>
		/// <returns>The decrypted messsage</returns>
		/// <exception cref="CannotDecryptException">
		/// Thrown if there are any problems with the decryption process
		/// or the checksum does not match
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the payload argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the encrypter
		/// or if it is less than or equal to zero
		/// </exception>
		public byte[] Decrypt(ECEncryptedMessage payload, BigInteger privateKey)
		{
			if (payload == null)
				throw new ArgumentNullException("payload");

			if (privateKey <= 0 || privateKey >= _curve.Q)
				throw new ArgumentOutOfRangeException("privateKey", "Private key must be within the domain specified in the constructor parameters");

			if (payload.SharedSecret.Curve != _curve)
				throw new ArgumentException(
					"The message was encrypted on a different curve than the curve this decryptor was constructed with"
				);

			var secretPoint = payload.SharedSecret * privateKey;

			var key = new byte[_ed.KeySize];
			var copyIntoKey = secretPoint.X.Value.ToByteArray();
			Array.Copy(copyIntoKey, key, Math.Min(key.Length, copyIntoKey.Length));

			return _ed.Decrypt(payload.SymmetricEncryptedMessage, key);
		}

		#endregion Encrypt / Decrypt
	}
}