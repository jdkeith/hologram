﻿using System;
using System.IO;
using System.Linq;
using System.Numerics;
using JLabs.Structures;

// ReSharper disable InconsistentNaming
namespace JLabs.Cryptography.EC.Signing
{
	/// <summary>
	/// Provides a container for the information required
	/// to verify a signature
	/// </summary>
	public class ECDSASignature : ISerializable
	{
		private const byte StartSignatureFlag = 48;

		public static ECDSASignature Deserialize(byte[] serialized, bool strict = true)
		{
			if (serialized == null)
				throw new ArgumentNullException("serialized");

			using (var ms = new MemoryStream())
			{
				ms.Write(serialized, 0, serialized.Length);
				ms.Seek(0, SeekOrigin.Begin);
				return ReadFrom(ms, strict);
			}
		}

		/// <summary>
		/// Deserializes and signature from a stream
		/// </summary>
		/// <param name="s">The stream to read from</param>
		/// <param name="strict">Whether DER integers padded with leading zeros are disallowed</param>
		/// <returns>A deserialized signature</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public static ECDSASignature ReadFrom(Stream s, bool strict = true)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			const string formatExceptionMessage = "Invalid signature format";

			if (s.ReadByte() != StartSignatureFlag)
				throw new FormatException(formatExceptionMessage);

			try
			{
				var signatureLength = s.ReadByte();

				var rBytes = ReadDERBytes(s);
				var sBytes = ReadDERBytes(s);

				if (rBytes.Length + sBytes.Length != signatureLength)
					throw new Exception("Invalid signature length");

				var rVal = rBytes.FromDEREncoding(strict);
				var sVal = sBytes.FromDEREncoding(strict);

				return new ECDSASignature(rVal, sVal);
			}
			catch (FormatException fex)
			{
				throw new FormatException("Invalid signature format", fex);
			}
		}

		private static byte[] ReadDERBytes(Stream s)
		{
			var formatException = new FormatException("Not a valid DER integer encoding");

			if (s.ReadByte() != 0x02)
				throw formatException;

			var vLen = (byte) s.ReadByte();

			if (vLen == 0)
				throw formatException;

			var vBytes = new byte[vLen];

			s.Read(vBytes, 0, vLen);

			return new byte[] { 2, vLen }.Concat(vBytes).ToArray();
		}

		/// <summary>
		/// Creates a new ECDSASignature
		/// </summary>
		/// <param name="r">The R value of the signature</param>
		/// <param name="s">The S value of the signature</param>
		public ECDSASignature(BigInteger r, BigInteger s)
		{
			R = r;
			S = s;
		}

		/// <summary>
		/// The R value of the signature
		/// </summary>
		public BigInteger R { get; private set; }

		/// <summary>
		/// The S value of the signature
		/// </summary>
		public BigInteger S { get; private set; }

		/// <summary>
		/// Converts the item to a byte array
		/// </summary>
		/// <returns>A serialized representation of the item</returns>
		public virtual byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream is null
		/// </exception>
		public virtual void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			var rDER = R.ToDEREncoding();
			var sDER = S.ToDEREncoding();

			s.WriteByte(StartSignatureFlag);
			s.WriteByte((byte)(rDER.Length + sDER.Length));
			s.Write(rDER, 0, rDER.Length);
			s.Write(sDER, 0, sDER.Length);
		}
	}
}
// ReSharper restore InconsistentNaming