﻿using System;
using System.Numerics;
using System.Security.Cryptography;
using JLabs.Structures;
using JLabs.Structures.Cryptography;

// ReSharper disable InconsistentNaming
namespace JLabs.Cryptography.EC.Signing
{
	/// <summary>
	/// Provides high(ish) level methods for signing and verifying digital signatures
	/// </summary>
	/// <see cref="http://cs.ucsb.edu/~koc/ccs130h/notes/ecdsa-cert.pdf"/>
	public class ECDSASigner
	{
		#region Fields

		private static readonly RandomNumberGenerator _defaultRng;
		private static readonly IHasher _defaultHasher;
		
		private readonly ECCurve _curve;
		private readonly RandomNumberGenerator _rng;
		private readonly IHasher _hasher;

		#endregion Fields

		#region Constructors

		static ECDSASigner()
		{
			_defaultRng = new RNGCryptoServiceProvider();
			_defaultHasher = SHA256Hash.Factory;
		}

		/// <summary>
		/// Creates a new signer with the given domain parameters
		/// </summary>
		/// <param name="curve">The elliptic curve to use for the signer</param>
		/// <param name="rng">
		/// The random number generator to use for generating K values. If unspecifed,
		/// a singleton instance of RNGCryptoServiceProvider is used.
		/// </param>
		/// <param name="hasher">The hash method to use when computing message summaries</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the parameters parameter is null
		/// </exception>
		public ECDSASigner(ECCurve curve, RandomNumberGenerator rng = null, IHasher hasher = null)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			_curve = curve;
			_rng = rng ?? _defaultRng;
			_hasher = hasher ?? _defaultHasher;
		}

		/// <summary>
		/// Creates a new signer with the given well-known domain parameters
		/// </summary>
		/// <param name="curveType">The type of eliptic curve to use for the signer</param>
		/// <param name="rng">
		/// The random number generator to use for generating K values. If unspecifed,
		/// a singleton instance of RNGCryptoServiceProvider is used.
		/// </param>
		/// <param name="hasher">The hash method to use when computing message summaries</param>
		/// <exception cref="System.ArgumentException">
		/// Thrown if curveType parameter is Custom
		/// </exception>
		public ECDSASigner(ECCurveType curveType, RandomNumberGenerator rng = null, IHasher hasher = null)
			: this(ECCurve.GetByCurveType(curveType), rng, hasher)
		{ }

		#endregion Constructors

		#region Sign / Verify

		/// <summary>
		/// Verifies that a given message was signed by the private
		/// key associated with the given public key
		/// </summary>
		/// <param name="serializable">The message summary to verify</param>
		/// <param name="signature">The signature of the message summary</param>
		/// <param name="publicKey">
		/// The public key associated with the signing private key
		/// </param>
		/// <returns>
		/// True if the message was signed by the private key associated
		/// with the given public key and neither the message nor the
		/// signature has been tampered with, almost certainly false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any of the parameters are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on signer construction
		/// </exception>
		public bool Verify(ISerializable serializable, ECDSASignature signature, ECPoint publicKey)
		{
			if (serializable == null)
				throw new ArgumentNullException("serializable");

			var messageHash = serializable.GetType() == _hasher.HashType
				? serializable.ToByteArray()
				: _hasher.Of(serializable).ToByteArray();

			return VerifyInternal(messageHash, signature, publicKey);
		}

		/// <summary>
		/// Verifies that a given message was signed by the private
		/// key associated with the given public key
		/// </summary>
		/// <param name="message">The message to verify</param>
		/// <param name="signature">The signature of the message</param>
		/// <param name="publicKey">
		/// The public key associated with the signing private key
		/// </param>
		/// <returns>
		/// True if the message was signed by the private key associated
		/// with the given public key and neither the message nor the
		/// signature has been tampered with, almost certainly false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any of the parameters are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on signer construction
		/// </exception>
		public bool Verify(byte[] message, ECDSASignature signature, ECPoint publicKey)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return VerifyInternal(
				_hasher.Of(message).ToByteArray(), signature, publicKey
			);
		}

		private bool VerifyInternal(byte[] hashMessage, ECDSASignature signature, ECPoint publicKey)
		{
			if (signature == null)
				throw new ArgumentNullException("signature");

			if (publicKey == null)
				throw new ArgumentNullException("publicKey");

			if (! _curve.IsOnCurve(publicKey))
				throw new PointMismatchException("Public Key is not on the curve specified by the constructor parameters");

			var n = _curve.N;

			if( signature.R < 1 || signature.R > n || signature.S < 1 || signature.S > n )
				return false;

			var e = CalculateE(hashMessage);

			var w = signature.S.ModInverse(n);
			var u1 = (e * w) % n;
			var u2 = (signature.R * w) % n;

			var X = _curve.G * u1 + publicKey * u2;

			if (X.IsInfinity)
				return false;

			var v = X.X.Value % n;

			return v == signature.R;
		}

		/// <summary>
		/// Produces a signature for a message/privateKey pair
		/// </summary>
		/// <param name="serializable">The message summary to sign</param>
		/// <param name="privateKey">The private key to use for signing</param>
		/// <returns>
		/// A digital signature which can be used to verify the
		/// integrity of a message or identity of a signer
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the messageSummary argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the signer
		/// or if it is less than or equal to zero
		/// </exception>
		public ECDSASignature Sign(ISerializable serializable, BigInteger privateKey)
		{
			if (serializable == null)
				throw new ArgumentNullException("serializable");

			var hashMessage = serializable.GetType() == _hasher.HashType
				? serializable.ToByteArray()
				: _hasher.Of(serializable).ToByteArray();

			return SignInternal(hashMessage, privateKey);
		}

		/// <summary>
		/// Produces a signature for a message/privateKey pair
		/// </summary>
		/// <param name="message">The message to sign</param>
		/// <param name="privateKey">The private key to use for signing</param>
		/// <returns>
		/// A digital signature which can be used to verify the
		/// integrity of a message or identity of a signer
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the signer
		/// or if it is less than or equal to zero
		/// </exception>
		public ECDSASignature Sign(byte[] message, BigInteger privateKey)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return SignInternal(_hasher.Of(message).ToByteArray(), privateKey);
		}

		private ECDSASignature SignInternal(byte[] hashMessage, BigInteger privateKey)
		{
			if (privateKey <= 0 || privateKey > _curve.Q)
				throw new ArgumentOutOfRangeException("privateKey", "Private key is outside of domain in constructor parameters");

			var n = _curve.N;
			var e = CalculateE(hashMessage);

			BigInteger k;
			ECPoint K;
			BigInteger r;
			BigInteger s;

			do
			{
				k = _rng.NextBigInteger(BigInteger.One, n);
				K = _curve.G * k;
				r = K.X.Value % n;
	
				s = (k.ModInverse(n) * (e + privateKey * r)) % n;
			}
			while (s == 0);

			return new ECDSASignature(r, s);
		}

		#endregion Sign / Verify

		#region Utilities

		private BigInteger CalculateE(byte[] message)
		{
			var messageBitLength = message.Length * 8;

			var messageAsNumber = message.ToNonNegativeBigInteger(reverse: true);
			var domainBitLength = _curve.N.GetBitLength();

			if (domainBitLength < messageBitLength)
				messageAsNumber = messageAsNumber >> (messageBitLength - domainBitLength);

			return messageAsNumber;
		}

		#endregion Utilities
	}
}