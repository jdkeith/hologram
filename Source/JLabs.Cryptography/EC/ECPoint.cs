﻿using System;
using System.IO;
using System.Numerics;

using JLabs.Structures;

namespace JLabs.Cryptography.EC
{
	/// <summary>
	/// Represents a point on an elliptical curve
	/// </summary>
	// ReSharper disable InconsistentNaming
	public class ECPoint : IEquatable<ECPoint>, ISerializable
	// ReSharper restore InconsistentNaming
	{
		#region Fields

		private readonly FieldElement _x;
		private readonly FieldElement _y;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates a new point on the given curve
		/// </summary>
		/// <param name="curve">The curve to create the point on</param>
		/// <param name="x">The x coordinate of the point</param>
		/// <param name="y">The y coordinate of the point</param>
		internal ECPoint(ECCurve curve, FieldElement x, FieldElement y)
		{
			Curve = curve;
			_x = x;
			_y = y;
		}

		/// <summary>
		/// Creates a new point at infinity for the given curve
		/// </summary>
		/// <param name="curve">The curve to create the point on</param>
		internal ECPoint(ECCurve curve)
		{
			Curve = curve;
			IsInfinity = true;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// The X coordinate of the point
		/// </summary>
		/// <exception cref="System.NotSupportedException">
		/// Thrown on access attempts if the point is a point at infinity
		/// </exception>
		public FieldElement X
		{
			get
			{
				if (IsInfinity)
					throw new NotSupportedException("Cannot get X coordinate of point at infinity");

				return _x;
			}
		}

		/// <summary>
		/// The Y coordinate of the point
		/// </summary>
		/// <exception cref="System.NotSupportedException">
		/// Thrown on access attempts if the point is a point at infinity
		/// </exception>
		public FieldElement Y
		{
			get
			{
				if (IsInfinity)
					throw new NotSupportedException("Cannot get X coordinate of point at infinity");

				return _y;
			}
		}

		/// <summary>
		/// The curve the point is on
		/// </summary>
		public ECCurve Curve { get; private set; }

		/// <summary>
		/// Whether or not the point represents the point at infinity for a curve
		/// </summary>
		public bool IsInfinity { get; private set; }

		#endregion Properties

		#region Operators

		/// <summary>
		/// Adds two points
		/// </summary>
		/// <param name="a">The first point to add</param>
		/// <param name="b">The second point to add</param>
		/// <returns>The sum of the two points</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either of the points are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the two points are not on the same curve
		/// </exception>
		public static ECPoint operator +(ECPoint a, ECPoint b)
		{
			if (a == null)
				throw new ArgumentNullException("a");

			if (b == null)
				throw new ArgumentNullException("b");

			if (!a.Curve.IsOnCurve(b))
				throw new PointMismatchException();

			// laws of identity
			if (a.IsInfinity)
				return b;

			if (b.IsInfinity)
				return a;

			if (a.X == b.X)
			{
				// a + a = 2a
				if (a.Y == b.Y)
					return a.Double();

				// a - a = 0
				return a.Curve.Infinity;
			}

			var slope = (b.Y - a.Y) / (b.X - a.X);
			var x = slope.Square() - a.X - b.X;
			var y = slope * (a.X - x) - a.Y;

			return new ECPoint(a.Curve, x, y);
		}

		/// <summary>
		/// Subtracts a point from another
		/// </summary>
		/// <param name="a">The first point to add</param>
		/// <param name="b">The second point to add</param>
		/// <returns>The difference between the first point and the second point</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either of the points are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the two points are not on the same curve
		/// </exception>
		public static ECPoint operator -(ECPoint a, ECPoint b)
		{
			if (a == null)
				throw new ArgumentNullException("a");

			if (b == null)
				throw new ArgumentNullException("b");

			return a + (-b);
		}

		/// <summary>
		/// Negates a point
		/// </summary>
		/// <param name="point">The point to negate</param>
		/// <returns>A negated point</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the point parameter is null
		/// </exception>
		public static ECPoint operator -(ECPoint point)
		{
			if (point == null)
				throw new ArgumentNullException("point");

			if( point.IsInfinity )
				return point;
	
			return new ECPoint(point.Curve, point.X, -point.Y);
		}

		/// <summary>
		/// Multiplies the point by a non-negative integer
		/// </summary>
		/// <param name="point">The point to multiply</param>
		/// <param name="multiplicand">The value to multiply the point by</param>
		/// <returns>A point multiplied by the multiplicand</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the point is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the multiplicand is negative
		/// </exception>
		public static ECPoint operator *(ECPoint point, BigInteger multiplicand)
		{
			if (point == null)
				throw new ArgumentNullException("point");

			if (multiplicand < 0)
				throw new ArgumentOutOfRangeException("multiplicand", "Multiplicand must be non-negative");

			var result = point.Curve.Infinity;

			var bits = multiplicand.ToBitArray();

			for (var n = bits.Length-1; n>=0; n--)
			{
				if (bits.Get(n))
					result += point;

				if( n > 0)
					result = result.Double();
			}

			return result;
		}

		#endregion Operators

		#region Other Mathematical Operations

		/// <summary>
		/// Doubles the given point
		/// </summary>
		/// <returns>The value of the point added to itself</returns>
		public ECPoint Double()
		{
			if (IsInfinity)
				return this;

			if (Y.Value == 0)
				return Curve.Infinity;

			var fe1 = (X.Square() * 3 + Curve.A) / (Y * 2);	
			var x = fe1.Square() - X * 2;
			var y = fe1 * (X - x) - Y;

			return new ECPoint(Curve, x, y);
		}

		#endregion Other Mathematical Operations

		#region ISerializable

		/// <summary>
		/// Converts the item to a byte array
		/// </summary>
		/// <returns>A serialized representation of the item</returns>
		public byte[] ToByteArray()
		{
			return ToByteArray(false);
		}

		/// <summary>
		/// Converts the item to a byte array
		/// </summary>
		/// <param name="compressed">
		/// Whether or not to return a compressed point representation
		/// </param>
		/// <returns>A serialized representation of the item</returns>
		public byte[] ToByteArray(bool compressed)
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms, compressed);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			WriteTo(s, false);
		}

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write to</param>
		/// <param name="compressed">
		/// Whether or not to write a compressed point representation
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream is null
		/// </exception>
		public void WriteTo(Stream s, bool compressed)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			if( ! compressed )
			{
				s.WriteByte(0x04);
			}

			var coordinateSize = (int)Math.Ceiling((double)Curve.Q.GetBitLength() / 8);

			var buffer = X.Value.ToFixedSizeByteArray(coordinateSize, reverse: true, trimLeadingZeros: true);

			if( ! compressed )
			{
				s.Write(buffer, 0, buffer.Length);

				buffer = Y.Value.ToFixedSizeByteArray(coordinateSize, reverse: true, trimLeadingZeros: true);
				s.Write(buffer, 0, buffer.Length);

				return;
			}

			s.WriteByte(Y.Value.IsEven ? (byte) 0x02 : (byte) 0x03);
			s.Write(buffer, 0, buffer.Length);
		}

		#endregion ISerializable

		#region Equality

		/// <summary>
		/// Compares two points for equality
		/// </summary>
		/// <param name="a">The first point to compare</param>
		/// <param name="b">The second point to compare</param>
		/// <returns>
		/// True if both points have the same X, Y, and Curve
		/// properties or both are null, false otherwise
		/// </returns>
		public static bool operator ==(ECPoint a, ECPoint b)
		{
			if (ReferenceEquals(a, null))
				return ReferenceEquals(b, null);

			return a.Equals(b);
		}

		/// <summary>
		/// Compares two points for equality
		/// </summary>
		/// <param name="a">The first point to compare</param>
		/// <param name="b">The second point to compare</param>
		/// <returns>
		/// False if both points have the same X, Y, and Curve
		/// properties or both are null, true otherwise
		/// </returns>
		public static bool operator !=(ECPoint a, ECPoint b)
		{
			return !(a == b);
		}

		/// <summary>
		/// Compares the point to another point
		/// </summary>
		/// <param name="other">The other point to compare to</param>
		/// <returns>
		/// True if the other point is not null and
		/// both points have the same X, Y, and Curve properties
		/// </returns>
		public bool Equals(ECPoint other)
		{
			if (ReferenceEquals(other, null))
				return false;

			return Curve == other.Curve
				&& X == other.X
				&& Y == other.Y;
		}

		/// <summary>
		/// Compares the point to another object
		/// </summary>
		/// <param name="obj">The object to compare the point to</param>
		/// <returns>
		/// True if the other object is a non-null point and both points have
		/// the same X, Y, and Curve properties
		/// </returns>
		public override bool Equals(object obj)
		{
			return Equals(obj as ECPoint);
		}

		/// <summary>
		/// Serves as a hash function for organizing points
		/// </summary>
		/// <returns>A number which is determined by the coordinates and curve of the point</returns>
		public override int GetHashCode()
		{
			return Curve.GetHashCode() ^ X.GetHashCode() ^ Y.GetHashCode();
		}

		#endregion Equality

		#region Conversion

		/// <summary>
		/// Returns a System.String that represents the point
		/// </summary>
		/// <returns>A string-representation of the point</returns>
		public override string ToString()
		{
			if (IsInfinity)
				return "Point at Infinity";

			return string.Format("{0} , {1}", X.Value, Y.Value);
		}

		#endregion Conversion
	}
}