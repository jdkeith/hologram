﻿using System;
using System.Globalization;
using System.Numerics;

// ReSharper disable InconsistentNaming
namespace JLabs.Cryptography.EC
{
	public enum ECCurveType : byte
	{
		Custom = 0x00,
		SECP256R1 = 0x10,
		SECP256K1 = 0x11,
		SECP384R1 = 0x14,
		SECP521R1 = 0x18,

		BrainPoolP256R1 = 0x40,
		BrainPoolP320R1 = 0x44,
		BrainPoolP384R1 = 0x48,
		BrainPoolP512R1 = 0x4C
	}

	/// <summary>
	/// A container for the curve, generator point,
	/// and other values required for digital signatures
	/// </summary>
	public class ECDomainParameters
	{
		#region Well-Known Parameters

		public static ECDomainParameters GetByCurveType(ECCurveType type)
		{
			switch (type)
			{
				case ECCurveType.SECP256R1:
					return SECP256R1;

				case ECCurveType.SECP256K1:
					return SECP256K1;
		
				case ECCurveType.SECP384R1:
					return SECP384R1;

				case ECCurveType.SECP521R1:
					return SECP521R1;

				case ECCurveType.BrainPoolP256R1:
					return BrainPoolP256R1;

				case ECCurveType.BrainPoolP320R1:
					return BrainPoolP320R1;

				case ECCurveType.BrainPoolP384R1:
					return BrainPoolP384R1;

				case ECCurveType.BrainPoolP512R1:
					return BrainPoolP512R1;

				default:
					throw new ArgumentException("Cannot get domain parameters for custom curve");
			}
		}

		public static ECDomainParameters SECP256R1
		{
			get
			{
				var q = BigInteger.Parse("00FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF", NumberStyles.HexNumber);
				var a = BigInteger.Parse("00FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC", NumberStyles.HexNumber);
				var b = BigInteger.Parse("005AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B", NumberStyles.HexNumber);

				var n = BigInteger.Parse("00FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551", NumberStyles.HexNumber);
				var h = BigInteger.One;

				var gx = BigInteger.Parse("006B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296", NumberStyles.HexNumber);
				var gy = BigInteger.Parse("004FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5", NumberStyles.HexNumber);

				return new ECDomainParameters(ECCurveType.SECP256R1, q, a, b, gx, gy, n, h);
			}
		}

		public static ECDomainParameters SECP256K1
		{
			get
			{
				var q = BigInteger.Parse("00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F", NumberStyles.HexNumber);
				var a = BigInteger.Zero;
				var b = (BigInteger)7;

				var n = BigInteger.Parse("00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141", NumberStyles.HexNumber);
				var h = BigInteger.One;

				var gx = BigInteger.Parse("0079BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798", NumberStyles.HexNumber);
				var gy = BigInteger.Parse("00483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8", NumberStyles.HexNumber);

				return new ECDomainParameters(ECCurveType.SECP256K1, q, a, b, gx, gy, n, h);
			}
		}

		public static ECDomainParameters SECP384R1
		{
			get
			{
				var q = BigInteger.Parse(
					"00FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"00FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"005AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"00FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"006B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"004FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5",
					NumberStyles.HexNumber
				);

				return new ECDomainParameters(ECCurveType.SECP384R1, q, a, b, gx, gy, n, h);
			}
		}

		public static ECDomainParameters SECP521R1
		{
			get
			{
				var q = BigInteger.Parse(
					"00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFF",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFC",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"00B3312FA7E23EE7E4988E056BE3F82D19181D9C6EFE8141120314088F5013875AC656398D8A2ED19D2A85C8EDD3EC2AEF",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7634D81F4372DDF581A0DB248B0A77AECEC196ACCC52973",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"00AA87CA22BE8B05378EB1C71EF320AD746E1D3B628BA79B9859F741E082542A385502F25DBF55296C3A545E3872760AB7",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"003617DE4A96262C6F5D9E98BF9292DC29F8F41DBD289A147CE9DA3113B5F0B8C00A60B1CE1D7E819D7A431D7C90EA0E5F",
					NumberStyles.HexNumber
				);

				return new ECDomainParameters(ECCurveType.SECP521R1, q, a, b, gx, gy, n, h);
			}
		}

		public static ECDomainParameters BrainPoolP256R1
		{
			get
			{
				var q = BigInteger.Parse("00A9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377", NumberStyles.HexNumber);
				var a = BigInteger.Parse("007D5A0975FC2C3057EEF67530417AFFE7FB8055C126DC5C6CE94A4B44F330B5D9", NumberStyles.HexNumber);
				var b = BigInteger.Parse("0026DC5C6CE94A4B44F330B5D9BBD77CBF958416295CF7E1CE6BCCDC18FF8C07B6", NumberStyles.HexNumber);
				
				var n = BigInteger.Parse("00A9FB57DBA1EEA9BC3E660A909D838D718C397AA3B561A6F7901E0E82974856A7", NumberStyles.HexNumber);
				var h = BigInteger.One;

				var gx = BigInteger.Parse("008BD2AEB9CB7E57CB2C4B482FFC81B7AFB9DE27E1E3BD23C23A4453BD9ACE3262", NumberStyles.HexNumber);
				var gy = BigInteger.Parse("00547EF835C3DAC4FD97F8461A14611DC9C27745132DED8E545C1D54C72F046997", NumberStyles.HexNumber);
				
				return new ECDomainParameters(ECCurveType.BrainPoolP256R1, q, a, b, gx, gy, n, h);
			}
		}

		public static ECDomainParameters BrainPoolP320R1
		{
			get
			{
				var q = BigInteger.Parse(
					"00D35E472036BC4FB7E13C785ED201E065F98FCFA6F6F40DEF4F92B9EC7893EC28FCD412B1F1B32E27",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"003EE30B568FBAB0F883CCEBD46D3F3BB8A2A73513F5EB79DA66190EB085FFA9F492F375A97D860EB4",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"00520883949DFDBC42D3AD198640688A6FE13F41349554B49ACC31DCCD884539816F5EB4AC8FB1F1A6",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"00D35E472036BC4FB7E13C785ED201E065F98FCFA5B68F12A32D482EC7EE8658E98691555B44C59311",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"0043BD7E9AFB53D8B85289BCC48EE5BFE6F20137D10A087EB6E7871E2A10A599C710AF8D0D39E20611",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"0014FDD05545EC1CC8AB4093247F77275E0743FFED117182EAA9C77877AAAC6AC7D35245D1692E8EE1",
					NumberStyles.HexNumber
				);

				return new ECDomainParameters(ECCurveType.BrainPoolP320R1, q, a, b, gx, gy, n, h);
			}
		}


		public static ECDomainParameters BrainPoolP384R1
		{
			get
			{
				var q = BigInteger.Parse(
					"008CB91E82A3386D280F5D6F7E50E641DF152F7109ED5456B412B1DA197FB71123ACD3A729901D1A71874700133107EC53",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"007BC382C63D8C150C3C72080ACE05AFA0C2BEA28E4FB22787139165EFBA91F90F8AA5814A503AD4EB04A8C7DD22CE2826",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"004A8C7DD22CE28268B39B55416F0447C2FB77DE107DCD2A62E880EA53EEB62D57CB4390295DBC9943AB78696FA504C11",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"008CB91E82A3386D280F5D6F7E50E641DF152F7109ED5456B31F166E6CAC0425A7CF3AB6AF6B7FC3103B883202E9046565",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"001D1C64F068CF45FFA2A63A81B7C13F6B8847A3E77EF14FE3DB7FCAFE0CBD10E8E826E03436D646AAEF87B2E247D4AF1E",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"008ABE1D7520F9C2A45CB1EB8E95CFD55262B70B29FEEC5864E19C054FF99129280E4646217791811142820341263C5315",
					NumberStyles.HexNumber
				);

				return new ECDomainParameters(ECCurveType.BrainPoolP384R1, q, a, b, gx, gy, n, h);
			}
		}

		public static ECDomainParameters BrainPoolP512R1
		{
			get
			{
				var q = BigInteger.Parse(
					"00AADD9DB8DBE9C48B3FD4E6AE33C9FC07CB308DB3B3C9D20ED6639CCA703308717D4D9B009BC66842AECDA12AE6A380E62881FF2F2D82C68528AA6056583A48F3",
					NumberStyles.HexNumber
				);

				var a = BigInteger.Parse(
					"007830A3318B603B89E2327145AC234CC594CBDD8D3DF91610A83441CAEA9863BC2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A72BF2C7B9E7C1AC4D77FC94CA",
					NumberStyles.HexNumber
				);

				var b = BigInteger.Parse(
					"003DF91610A83441CAEA9863BC2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A72BF2C7B9E7C1AC4D77FC94CADC083E67984050B75EBAE5DD2809BD638016F723",
					NumberStyles.HexNumber
				);

				var n = BigInteger.Parse(
					"00AADD9DB8DBE9C48B3FD4E6AE33C9FC07CB308DB3B3C9D20ED6639CCA70330870553E5C414CA92619418661197FAC10471DB1D381085DDADDB58796829CA90069",
					NumberStyles.HexNumber
				);

				var h = BigInteger.One;

				var gx = BigInteger.Parse(
					"0081AEE4BDD82ED9645A21322E9C4C6A9385ED9F70B5D916C1B43B62EEF4D0098EFF3B1F78E2D0D48D50D1687B93B97D5F7C6D5047406A5E688B352209BCB9F822",
					NumberStyles.HexNumber
				);

				var gy = BigInteger.Parse(
					"007DDE385D566332ECC0EABFA9CF7822FDF209F70024A57B1AA000C55B881F8111B2DCDE494A5F485E5BCA4BD88A2763AED1CA2B2FA8F0540678CD1E0F3AD80892",
					NumberStyles.HexNumber
				);

				return new ECDomainParameters(ECCurveType.BrainPoolP512R1, q, a, b, gx, gy, n, h);
			}
		}


		#endregion Well-Known Parameters

		#region Constructors

		/// <summary>
		/// Creates a new ECDomainParameters bundle
		/// </summary>
		/// <param name="generator">The generator point</param>
		/// <param name="n">The N value</param>
		/// <param name="h">The H value</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the generator parameter is null
		/// </exception>
		public ECDomainParameters(ECPoint generator, BigInteger n, BigInteger h)
			: this(ECCurveType.Custom, generator, n, h)
		{
		}

		private ECDomainParameters(ECCurveType type, ECPoint generator, BigInteger n, BigInteger h)
		{
			if (generator == null)
				throw new ArgumentNullException("generator");

			Curve = generator.Curve;
			G = generator;
			N = n;
			H = h;
			Type = type;
		}

		private ECDomainParameters(
			ECCurveType type, BigInteger q, BigInteger a, BigInteger b, BigInteger gx, BigInteger gy, BigInteger n, BigInteger h
		)
		{
			Curve = new ECCurve(q, a, b);
			G = Curve.CreatePoint(gx, gy);

			N = n;
			H = h;
			Type = type;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// The curve associated with this set. The generator G
		/// will lie alone this curve
		/// </summary>
		public ECCurve Curve { get; private set; }

		/// <summary>
		/// The generator point
		/// </summary>
		public ECPoint G { get; private set; }

		/// <summary>
		/// The N value
		/// </summary>
		public BigInteger N { get; private set; }

		/// <summary>
		/// The H value
		/// </summary>
		public BigInteger H { get; private set; }

		/// <summary>
		/// The type of the curve / parameters
		/// </summary>
		public ECCurveType Type { get; private set; }

		#endregion Properties
	}
}
// ReSharper restore InconsistentNaming