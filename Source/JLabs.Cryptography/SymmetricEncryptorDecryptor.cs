﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

using JLabs.Structures.Cryptography;

namespace JLabs.Cryptography
{
	public class SymmetricEncryptorDecryptor : IDisposable
	{
		#region Fields

		private static readonly RandomNumberGenerator _baseRng;

		private readonly SymmetricAlgorithm _algorithm;
		private readonly RandomNumberGenerator _rng;
		
		public readonly int KeySize;

		#endregion Fields

		#region Factories

		// ReSharper disable once InconsistentNaming
		public static SymmetricEncryptorDecryptor AES
		{
			get
			{
				return new SymmetricEncryptorDecryptor(
					new AesManaged
					{
						BlockSize = 128,
						KeySize = 128,
						Mode = CipherMode.CBC,
						Padding = PaddingMode.PKCS7,
					}
				);
			}
		}

		// ReSharper disable once InconsistentNaming
		public static SymmetricEncryptorDecryptor RijnadaelAESCompatible
		{
			get
			{
				return new SymmetricEncryptorDecryptor(
					new RijndaelManaged
					{
						BlockSize = 256,
						KeySize = 128,
						Mode = CipherMode.CBC,
						Padding = PaddingMode.PKCS7,
					}
				);
			}
		}

		public static SymmetricEncryptorDecryptor Rijnadael
		{
			get
			{
				return new SymmetricEncryptorDecryptor(
					new RijndaelManaged
					{
						BlockSize = 256,
						KeySize = 256,
						Mode = CipherMode.CBC,
						Padding = PaddingMode.PKCS7,
					}
				);
			}
		}

		#endregion Factories

		#region Constructors

		static SymmetricEncryptorDecryptor()
		{
			_baseRng = new RNGCryptoServiceProvider();
		}

		public SymmetricEncryptorDecryptor(SymmetricAlgorithm algorithm, RandomNumberGenerator rng = null)
		{
			if (algorithm == null)
				throw new ArgumentNullException("algorithm");

			_algorithm = algorithm;
			_rng = rng ?? _baseRng;
			KeySize = algorithm.KeySize / 8;
		}

		#endregion Constructors

		#region Methods

		public byte[] Encrypt(byte[] data, byte[] key)
		{
			var iv = new byte[KeySize];
			_rng.GetBytes(iv);

			return Encrypt(data, key, iv);
		}

		public byte[] Encrypt(byte[] data, byte[] key, byte[] iv)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			if (! data.Any())
				throw new ArgumentException("Data cannot be empty", "data");

			if (key == null)
				throw new ArgumentNullException("key");

			if (key.Length != KeySize)
				throw new ArgumentException("Invalid key size", "key");

			if (iv == null)
				throw new ArgumentNullException("iv");

			if (iv.Length != KeySize)
				throw new ArgumentException("Invalid initialization vector size", "iv");

			// hmac =  H(key + H(key + message)) 
			var hmac = SHA256Hash.Factory.HMAC(key, data);

			_algorithm.Clear();

			try
			{
				var encryptor = _algorithm.CreateEncryptor(key, iv);

				byte[] cipherBytes;

				using (var ms = new MemoryStream())
				{
					using (var es = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
					{
						es.Write(hmac.ToByteArray(), 0, SHA256Hash.Size);
						es.Write(data, 0, data.Length);
					}

					cipherBytes = ms.ToArray();
				}

				return iv.Concat(cipherBytes).ToArray();
			}
			finally
			{
				_algorithm.Clear();
			}
		}

		public byte[] Decrypt(byte[] encrypted, byte[] key)
		{
			if (encrypted == null)
				throw new ArgumentNullException("encrypted");

			if (key == null)
				throw new ArgumentNullException("key");

			if (encrypted.Length <= KeySize)
				throw new FormatException("Not enough encrypted bytes to define an encrypted message acceptible to this encrypter");

			if (key.Length != KeySize)
				throw new ArgumentException("Invalid key size", "key");

			var iv = new byte[KeySize];
			Array.Copy(encrypted, iv, iv.Length);

			// ReSharper disable once InconsistentNaming
			var encryptedWithoutIV = new byte[encrypted.Length - KeySize];
			Array.Copy(encrypted, KeySize, encryptedWithoutIV, 0, encryptedWithoutIV.Length);

			_algorithm.Clear();

			try
			{
				var decryptor = _algorithm.CreateDecryptor(key, iv);

				byte[] clearBytes;

				using (var ms = new MemoryStream())
				{
					using (var ds = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
					{
						ds.Write(encryptedWithoutIV, 0, encryptedWithoutIV.Length);
					}

					clearBytes = ms.ToArray();
				}

				// todo use slice
				var hmac = SHA256Hash.Factory.Deserialize(clearBytes.Take(32).ToArray());

				var message = new byte[clearBytes.Length - SHA256Hash.Size];
				Array.Copy(clearBytes, SHA256Hash.Size, message, 0, message.Length);

				// hmac =  H(key + H(key + message)) 
				// ReSharper disable once InconsistentNaming
				var compareHMAC = SHA256Hash.Factory.HMAC(key, message);

				if (hmac != compareHMAC)
					throw new FormatException("Invalid HMAC or message");

				return message;
			}
			finally
			{
				_algorithm.Clear();
			}		
		}

		public void Dispose()
		{
			_algorithm.Dispose();
		}

		#endregion Methods
	}
}