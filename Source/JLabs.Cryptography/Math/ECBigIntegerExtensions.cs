﻿using System.Linq;
using System.Security.Cryptography;

// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming
namespace System.Numerics
{
	/// <summary>
	/// Provides convenience and utility methods for or related to big integers in encryption
	/// </summary>
	public static class ECBigIntegerExtensions
	{
		/// <summary>
		/// Computes a random big integer within a given range
		/// </summary>
		/// <param name="rng">The random number generator used to produce the big integer</param>
		/// <param name="min">The inclusive minimum value of the returned result</param>
		/// <param name="max">The exclusive maximum value of the returned result</param>
		/// <returns>A random big integer in the range min &lt;= result &lt; max</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the random number generator is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the min value is greater than the max vaule
		/// </exception>
		public static BigInteger NextBigInteger(this RandomNumberGenerator rng, BigInteger min, BigInteger max)
		{
			if( rng == null )
				throw new ArgumentNullException("rng");

			if( min > max )
				throw new ArgumentOutOfRangeException("min", "Min cannot be larger than max");

			var range = max - min;

			var sz = (int)System.Math.Ceiling((double)range.GetBitLength() / 8);

			var buffer = new byte[sz * 2];

			rng.GetBytes(buffer);

			var rand = buffer.ToNonNegativeBigInteger();

			return min + (rand % range);
		}

		public static byte[] ToDEREncoding(this BigInteger value)
		{
			var vBytes = value.ToByteArray();

			Array.Reverse(vBytes);

			if (vBytes.Length > byte.MaxValue)
				throw new NotSupportedException("Value is to large to encode as a DER integer in this implementation");

			var vLen = (byte)vBytes.Length;

			return new byte[] { 0x02, vLen }.Concat(vBytes).ToArray();
		}

		public static BigInteger FromDEREncoding(this byte[] derEncoded, bool strict = true)
		{
			if (derEncoded == null)
				throw new ArgumentNullException("derEncoded");

			var formatException = new FormatException("Not a valid DER integer encoding");

			if (derEncoded.Length < 3)
				throw formatException;

			if (derEncoded[0] != 0x02)
				throw formatException;

			var vLen = derEncoded[1];

			// must be exactly the right length
			if (vLen == 0 || vLen + 2 != derEncoded.Length)
				throw formatException;

			// only one leading zero allowed if strict
			if (strict && derEncoded.Skip(2).TakeWhile(v => v == 0).Count() > 1)
				throw formatException;

			return new BigInteger(derEncoded.Skip(2).Reverse().ToArray());
		}
	}
}