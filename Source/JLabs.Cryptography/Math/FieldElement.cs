﻿using System.Numerics.Utilities;

// ReSharper disable once CheckNamespace
namespace System.Numerics
{
	/// <summary>
	/// Represents an element within a finite field
	/// </summary>
	public struct FieldElement : IComparable<FieldElement>, IEquatable<FieldElement>
	{
		#region Fields

		private readonly BigInteger _value;
		private BigInteger _domain;

		#endregion Fields

		#region Constructor

		/// <summary>
		/// Creates a new FieldElement with a value of zero in the given domain
		/// </summary>
		/// <param name="domain">The size of the finite field</param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the domain argument is zero or negative
		/// </exception>
		public FieldElement(BigInteger domain) : this(0, domain)
		{
		}

		/// <summary>
		/// Creates a new field element
		/// </summary>
		/// <param name="value">The value of the element</param>
		/// <param name="domain">The size of the finite field</param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the domain argument is zero or negative
		/// </exception>
		/// <remarks>
		/// The value will be automatically clamped to the domain value
		/// </remarks>
		public FieldElement(BigInteger value, BigInteger domain)
		{
			if (domain.Sign <= 0)
				throw new ArgumentOutOfRangeException("domain", "Domain must be positive");

			if (value < 0)
			{
				var count = BigInteger.Abs(value/domain);
				value += ((count + 1)*domain);
			}

			_value = value % domain;
			_domain = domain;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// The value in the finite field
		/// </summary>
		public BigInteger Value
		{
			get { return _value; }
		}

		/// <summary>
		/// The size of the fininite field the element is within
		/// </summary>
		public BigInteger Domain
		{
			get
			{
				if (_domain == 0)
					_domain = 1;

				return _domain;
			}
		}

		/// <summary>
		/// The number of bits required to represent the finite field
		/// </summary>
		public int FieldSize
		{
			get
			{
				return Domain.GetBitLength();
			}
		}

		#endregion Properties

		#region Operators

		/// <summary>
		/// Adds two field elements
		/// </summary>
		/// <param name="a">The first element</param>
		/// <param name="b">The second element</param>
		/// <returns>The sum of the two elements</returns>
		/// <exception cref="DomainMismatchException">
		/// Thrown if the field elements are in differently sized finite fields
		/// </exception>
		public static FieldElement operator +(FieldElement a, FieldElement b)
		{
			if (a.Domain != b.Domain)
				throw new DomainMismatchException();

			return new FieldElement(a._value + b._value, a.Domain);
		}

		/// <summary>
		/// Adds a scalar value to a FieldElement 
		/// </summary>
		/// <param name="a">The first element</param>
		/// <param name="b">The value to add</param>
		/// <returns>The clamped sum of the field element and the value</returns>
		public static FieldElement operator +(FieldElement a, BigInteger b)
		{
			return new FieldElement(a._value + b, a.Domain);
		}

		/// <summary>
		/// Subtracts two field elements
		/// </summary>
		/// <param name="a">The first element</param>
		/// <param name="b">The second element</param>
		/// <returns>The difference of the two elements</returns>
		/// <exception cref="DomainMismatchException">
		/// Thrown if the field elements are in differently sized finite fields
		/// </exception>
		public static FieldElement operator -(FieldElement a, FieldElement b)
		{
			if (a == null)
				throw new ArgumentNullException("a");

			if (b == null)
				throw new ArgumentNullException("b");

			if (a.Domain != b.Domain)
				throw new DomainMismatchException();

			var newVal = a._value - b._value;

			if (newVal < 0)
				newVal = a.Domain + newVal;
			else
				newVal = newVal % a.Domain;

			return new FieldElement(newVal, a.Domain);
		}

		/// <summary>
		/// Subtracts a scalar value from a FieldElement 
		/// </summary>
		/// <param name="a">The first element</param>
		/// <param name="b">The value to subtract</param>
		/// <returns>The clamped sum of the field element and the value</returns>
		public static FieldElement operator -(FieldElement a, BigInteger b)
		{
			return new FieldElement(a._value - b, a.Domain);
		}

		/// <summary>
		/// Multiplies two field elements
		/// </summary>
		/// <param name="a">The first element</param>
		/// <param name="b">The second element</param>
		/// <returns>The product of the two elements</returns>
		/// <exception cref="DomainMismatchException">
		/// Thrown if the field elements are in differently sized finite fields
		/// </exception>
		public static FieldElement operator *(FieldElement a, FieldElement b)
		{
			if (a == null)
				throw new ArgumentNullException("a");

			if (b == null)
				throw new ArgumentNullException("b");

			if (a.Domain != b.Domain)
				throw new DomainMismatchException();

			return new FieldElement(a._value * b._value, a.Domain);
		}

		/// <summary>
		/// Multiplies a scalar value from a FieldElement 
		/// </summary>
		/// <param name="a">The first element</param>
		/// <param name="b">The value to multiply</param>
		/// <returns>The clamped product of the field element and the value</returns>
		public static FieldElement operator *(FieldElement a, BigInteger b)
		{
			return new FieldElement(a._value * b, a.Domain);
		}

		/// <summary>
		/// Divides two field elements
		/// </summary>
		/// <param name="a">The first element</param>
		/// <param name="b">The second element</param>
		/// <returns>The quotient of the two elements</returns>
		/// <exception cref="DomainMismatchException">
		/// Thrown if the field elements are in differently sized finite fields
		/// </exception>
		public static FieldElement operator /(FieldElement a, FieldElement b)
		{
			if (a == null)
				throw new ArgumentNullException("a");

			if (b == null)
				throw new ArgumentNullException("b");

			if (a.Domain != b.Domain)
				throw new DomainMismatchException();

			return a * b.Invert();
		}

		/// <summary>
		/// Divides a scalar value from a FieldElement 
		/// </summary>
		/// <param name="a">The first element</param>
		/// <param name="b">The value to divide by</param>
		/// <returns>The clamped quotient of the field element and the value</returns>
		public static FieldElement operator /(FieldElement a, BigInteger b)
		{
			var bf = new FieldElement(b, a.Domain);

			return a/bf;
		}

		/// <summary>
		/// Negates the field element
		/// </summary>
		/// <param name="a">The element to negate</param>
		/// <returns>The negated field element</returns>
		public static FieldElement operator -(FieldElement a)
		{
			var newVal = BigInteger.Negate(a._value);

			return new FieldElement(a.Domain + newVal, a.Domain);
		}

		/// <summary>
		/// Adds one to the field element
		/// </summary>
		/// <param name="a">The element add one two</param>
		/// <returns>The new value clamped to the domain</returns>
		public static FieldElement operator ++(FieldElement a)
		{
			return new FieldElement(a._value + 1, a.Domain);
		}

		/// <summary>
		/// Subtracts one to the field element
		/// </summary>
		/// <param name="a">The element subtract one from</param>
		/// <returns>The new value clamped to the domain</returns>
		public static FieldElement operator --(FieldElement a)
		{
			return new FieldElement(a._value - 1, a.Domain);
		}

		#endregion Operators

		#region Other Mathematical Operations

		/// <summary>
		/// Squares the field element
		/// </summary>
		/// <returns>The squared field element</returns>
		public FieldElement Square()
		{
			return new FieldElement(_value * _value, Domain);
		}

		/// <summary>
		/// Gets the multiplicative inverse of the field element
		/// </summary>
		/// <returns>The multiplicative inverse of the field element</returns>
		public FieldElement Invert()
		{
			return new FieldElement(
				_value.ModInverse(Domain), Domain
			);
		}

		/// <summary>
		/// Gets the square root of the field element
		/// </summary>
		/// <returns>The square root of the field element</returns>
		/// <exception cref="System.ArithmeticException">
		/// Thrown if the element has no square root
		/// </exception>
		/// <remarks>Uses the Shanks-Tonelli algorithm</remarks>
		public FieldElement SquareRoot()
		{
			var value = ShanksTonelli.SquareRoot(_value, Domain);

			if (value.Sign < 0)
				throw new ArithmeticException("No square root exists of this field element");

			return new FieldElement(value, Domain);
		}

		#endregion Other Mathematical Operations

		#region Equality and Comparison

		/// <summary>
		/// Compares one field element to another
		/// </summary>
		/// <param name="other">The field element used for comparison</param>
		/// <returns>A number indicating the relative relation of the invocant to the parameter</returns>
		/// <remarks>Null field elements sort after non-null field elements</remarks>
		public int CompareTo(FieldElement other)
		{
			if (Domain != other.Domain)
				throw new InvalidOperationException("Cannot compare field elements from differing domains");

			return _value.CompareTo(other._value);
		}

		/// <summary>
		/// Compares two field elements
		/// </summary>
		/// <param name="a">The first field element</param>
		/// <param name="b">The second field element</param>
		/// <returns>True if the first field element is less than the second field element and neither argument is null</returns>
		public static bool operator <(FieldElement a, FieldElement b)
		{
			return a.CompareTo(b) == -1;
		}

		/// <summary>
		/// Compares two field elements
		/// </summary>
		/// <param name="a">The first field element</param>
		/// <param name="b">The second field element</param>
		/// <returns>True if the first field element is less than or equal to the second field element and neither argument is null</returns>
		public static bool operator <=(FieldElement a, FieldElement b)
		{
			return a.CompareTo(b) <= 0;
		}

		/// <summary>
		/// Compares two field elements
		/// </summary>
		/// <param name="a">The first field element</param>
		/// <param name="b">The second field element</param>
		/// <returns>
		/// True if the first field element is greater than the second
		/// field element and neither argument is null, false otherwise
		/// </returns>
		public static bool operator >(FieldElement a, FieldElement b)
		{
			return a.CompareTo(b) == 1;
		}

		/// <summary>
		/// Compares two field elements
		/// </summary>
		/// <param name="a">The first field element</param>
		/// <param name="b">The second field element</param>
		/// <returns>True if the first field element is greater than or equal to the second field element and neither argument is null</returns>
		public static bool operator >=(FieldElement a, FieldElement b)
		{
			return a.CompareTo(b) >= 0;
		}

		/// <summary>
		/// Compares two field elements
		/// </summary>
		/// <param name="a">The first field element</param>
		/// <param name="b">The second field element</param>
		/// <returns>
		/// True if the first field element has the same value and domain as the second
		/// field element and exactly one element is not null, false otherwise
		/// </returns>
		public static bool operator ==(FieldElement a, FieldElement b)
		{
			return a.Equals(b);
		}

		/// <summary>
		/// Compares two field elements
		/// </summary>
		/// <param name="a">The first field element</param>
		/// <param name="b">The second field element</param>
		/// <returns>
		/// True if the first field element has a different value or domain
		/// than the second field element or exactly one element is null,
		/// false otherwise
		/// </returns>
		public static bool operator !=(FieldElement a, FieldElement b)
		{
			return !(a == b);
		}

		/// <summary>
		/// Compares two field elements
		/// </summary>
		/// <param name="other">The field element used for comparison</param>
		/// <returns>
		/// True if the invocant has the same value and domain as the parameter
		/// and the parameter is not null
		/// </returns>
		public bool Equals(FieldElement other)
		{
			return Domain == other.Domain && _value == other._value;
		}

		/// <summary>
		/// Compares the field element to an object
		/// </summary>
		/// <param name="obj">The object used for comparison</param>
		/// <returns>
		/// True if the invocant has the same value and domain as the parameter
		/// and the parameter is not null or not a field element
		/// </returns>
		public override bool Equals(object obj)
		{
			if (! (obj is FieldElement))
				return false;

			return Equals((FieldElement) obj);
		}

		/// <summary>
		/// Serves as a hash function for organizing field elements
		/// </summary>
		/// <returns>A number which is determined by the domain and value of the field element</returns>
		public override int GetHashCode()
		{
			return Domain.GetHashCode() ^ _value.GetHashCode();
		}

		#endregion Equality and Comparison

		#region Conversion

		public static implicit operator BigInteger(FieldElement element)
		{
			return element._value;
		}

		/// <summary>
		/// Returns a System.String that represents the field element
		/// </summary>
		/// <returns>A string-representation of the field element</returns>
		public override string ToString()
		{
			return string.Format("{0} in {1}", _value, Domain);
		}

		#endregion
	}
}