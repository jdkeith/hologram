﻿using System;
using System.Collections.Generic;

using CAWG.Hologram.Core.Chain;

namespace CAWG.Hologram.Core.DB
{
	public enum AddBlockStatus : byte
	{
		Successful,

		UnsupportedVersion,
		ChannelMismatch,
		LowerIndexIgnored,
		KnownBadId,
		BlockTooLarge,
		InvalidNonce,
		InvalidTime,
		InvalidDifficulty,
		MerkleRootMismatch,
		MessagesSectionCompresssed,
		BalancesSectionCompressed,
		InvalidOperation,
		OperationTooLarge,
		OperationUsesDeadAccount,
		OperationAccountUnderflow,
		OperationReferenceNotFound,
		OperationPossibleReplayAttempt,
		InvalidDifficultyDiscount,
		MissingFallOffBlockAdjustements,
		AdjustmentsWithNoCBlockAdjustmentsAllowed,
		AdjustmentFromCBlockNotFound,
		AdjustmentsSectionNotFilled,
		AdjustmentsNotUndone,
		CBlockDoesNotConnect,
		InvalidCBlockReference
	}

	public class Channel
	{
		public Channel(
			IEnumerable<BlockHeaderSummary> sharedSummaries,
			IEnumerable<BlockHeader> inactiveBlocks,
			IEnumerable<Block> activeBlocks
		)
		{

		}

		public AddBlockStatus AddBlock(Block block)
		{
			throw new NotImplementedException();
		}
	}
}
