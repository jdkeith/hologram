﻿using System;
using System.Collections.Generic;
using System.Linq;

using CAWG.Hologram.Core.Chain;
using CAWG.Hologram.Core.User;
using CAWG.Hologram.Core.Currency;

using JLabs.Structures;
using JLabs.Structures.Cryptography;
using JLabs.Structures.Units;

namespace CAWG.Hologram.Core.DB
{
	/// <summary>
	/// Provides an interface for storing and retrieving client data
	/// </summary>
	/// <remarks>
	/// Implementations are expected to perform some basic sanity checking,
	/// but will not perform block validation.
	/// </remarks>
	public interface IDB : IDisposable
	{
		/// <summary>
		/// Returns a set of all channels the client is subscribed to
		/// </summary>
		IEnumerable<string> SubscribedToChannels { get; }

		/// <summary>
		/// Unsubscribes from the given channel
		/// </summary>
		/// <param name="channel">The channel to unsubscribe from</param>
		/// <remarks>
		/// <para>Has no effect if the channel is not already subscribed to</para>
		/// <para>Implementations are free to immediately delete unnecessary data after this call</para>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel parameter is null
		/// </exception>
		void UnsubscribeFrom(string channel);

		/// <summary>
		/// Subscribes to the given channel
		/// </summary>
		/// <param name="channel">The channel to subscribe to</param>
		/// <remarks>Has no effect if the channel is already subscribed to</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel parameter is null
		/// </exception>
		void SubscribeTo(string channel);

		/// <summary>
		/// Gets the block with the given id
		/// </summary>
		/// <param name="id">The id of the block to retrieve</param>
		/// <param name="includeTentative">Whether tentative blocks are included in the search</param>
		/// <returns>The block with the given id</returns>
		/// <remarks>
		/// The fullest type of block stored is always returned. This method may return
		/// Blocks, BlockHeaders, or BlockHeaderSummaries
		/// </remarks>
		BlockHeaderSummary GetBlock(SHA256Hash id); //, bool includeTentative);

		/// <summary>
		/// Gets the block with the given id and all ancestors back to the channel's genesis block
		/// </summary>
		/// <param name="id">The id of the block to retrieve</param>
		/// <param name="includeTentative">Whether tentative blocks are included in the search</param>
		/// <returns>The block with the given id and all its ancestors</returns>
		/// <remarks>
		/// The fullest type of block stored is always returned. This method may return
		/// Blocks, BlockHeaders, or BlockHeaderSummaries
		/// </remarks>
		IEnumerable<BlockHeader> GetBlocksFrom(SHA256Hash id); //, bool includeTentative);

		/// <summary>
		/// Gets the tail block of the highest proof-of-work branch on the given
		/// channel's chain
		/// </summary>
		/// <param name="channel">The channel of the chain</param>
		/// <param name="includeTentative">
		/// Whether tentative blocks are included in the proof-of-work calculation
		/// </param>
		/// <returns>
		/// The tail block of the highest proof-of-work branch on the given channel's chain
		/// </returns>
		/// <remarks>
		///		<para>
		///	The fullest type of block stored is always returned. This method may return
		/// Blocks, BlockHeaders, or BlockHeaderSummaries
		///		</para>
		///		<para>
		///	If includedTentative is set to false, the tail block will never be tentative
		///		</para>
		///		<para>
		///	Returns null if the channel is not subscribed to
		///		</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel argument is null
		/// </exception>
		BlockHeader GetBestTail(string channel, bool includeTentative);

		/// <summary>
		/// Gets the tail block of the highest proof-of-work branch on the given
		/// channel's chain and all ancestors to the channel's genesis block
		/// </summary>
		/// <param name="channel">The channel of the chain</param>
		/// <param name="includeTentative">
		/// Whether tentative blocks are included in the proof-of-work calculation
		/// </param>
		/// <returns>
		/// The tail block of the highest proof-of-work branch on the given channel's chain
		/// and all its ancestors
		/// </returns>
		/// <remarks>
		///		<para>
		///	The fullest type of block stored is always returned. This method may return
		/// Blocks, BlockHeaders, or BlockHeaderSummaries
		///		</para>
		///		<para>
		///	If includedTentative is set to false, tentative blocks will never be returned
		///		</para>
		///		<para>
		///	Returns an empty enumeration if the channel is not subscribed to
		///		</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel argument is null
		/// </exception>
		IEnumerable<BlockHeader> GetBlocksFromBestTail(string channel, bool includeTentative);

		/// <summary>
		/// Gets all non-trimmed blocks, non-tentative blocks
		/// which have operations referencing any of the given channels,
		/// pulled from the longest respective branches
		/// </summary>
		/// <param name="channels">
		/// The channels which operations must reference
		/// </param>
		/// <returns>
		/// An lookup of matching blocks with duplicates removed grouped by block channel
		/// (not search channel)
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channels parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any channels are null
		/// </exception>
		ILookup<string,BlockHeader> GetBlocksByChannelReferences(
			IEnumerable<string> channels
		);

		/// <summary>
		/// Gets the current balance of an account on the given channel
		/// </summary>
		/// <param name="channel">The channel to search for the account on</param>
		/// <param name="accountId">The account to search for</param>
		/// <returns>The most recent balance for the account</returns>
		/// <remarks>
		/// <para>Will return null if the channel is not subscribed to</para>
		/// <para>
		/// If the account doesn't exist on the channel,
		/// an empty balance (zero bias) is returned for it
		/// </para>
		/// <para>
		/// Implementations are not expected to reject accounts due to
		/// domain param/channel mismatches.
		/// </para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		Balance GetCurrentBalanceFor(string channel, SHA256Hash accountId);

		/// <summary>
		/// Stores a block header summary
		/// </summary>
		/// <param name="block">The summary to store</param>
		/// <returns>
		/// True if the block summary is stored, false otherwise
		/// </returns>
		/// <remarks>
		/// Blocks header summaries will be rejected for the following reasons
		/// <para>A block with the given id already exists</para>
		/// <para>The block does not have a parent block which exists (if it's not the genesis block)</para>
		/// <para>The block summary builds off of a non-summary block</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the block parameter is null
		/// </exception>
		bool StoreBlock(BlockHeaderSummary block);

		/// <summary>
		/// Stores a block header
		/// </summary>
		/// <param name="block">The header to store</param>
		/// <returns>
		/// True if the block header is stored, false otherwise
		/// </returns>
		/// <remarks>
		/// Block headers will be rejected for the following reasons
		/// <para>A block with the given id already exists</para>
		/// <para>The block does not have a parent block which exists (if it's not the genesis block)</para>
		/// <para>The block header builds off of a full block</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the block parameter is null
		/// </exception>
		bool StoreBlock(BlockHeader block);

		/// <summary>
		/// Stores a full block
		/// </summary>
		/// <param name="block">The full block to store</param>
		/// <param name="isTentative">
		/// Whether or not the block is tentative (not completely verified)
		/// </param>
		/// <returns>
		/// True if the block is stored, false otherwise
		/// </returns>
		/// <remarks>
		/// Blocks will be rejected for the following reasons
		/// <para>A block with the given id already exists</para>
		/// <para>The block does not have a parent block which exists (if it's not the genesis block)</para>
		/// <para>The block builds off of a non-full block</para>
		/// <para>
		/// The block is marked as not tentative but builds on a tentative
		/// block (use ClearTentativesFrom before adding)
		/// </para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the block parameter is null
		/// </exception>
		bool StoreBlock(Block block, bool isTentative);

		/// <summary>
		/// Marks the block with the given id and all of its ancestors as
		/// non-tentative (fully confirmed)
		/// </summary>
		/// <param name="id">The id of the starting (tail) block to make non-tentative</param>
		/// <returns>
		/// The number of blocks converted. Will be zero if the tail block is not tentative
		/// or the tail block is not found.
		/// </returns>
		int ClearTentativesFrom(SHA256Hash id);

		/// <summary>
		/// Trims (converts to a BlockHeaderSummary) the block with the given id and all of its ancestors
		/// </summary>
		/// <param name="id">The id of the starting (tail) block to trim</param>
		/// <returns>
		/// The number of blocks trimmed. Will be zero if the tail block is already a
		/// summary or the tail block is not found.
		/// </returns>
		int TrimFrom(SHA256Hash id);

		/// <summary>
		/// Trims (converts to a BlockHeaderSummary) the first block
		/// with a date before the given date and all of its ancestors
		/// </summary>
		/// <param name="channel">The channel to trim blocks on</param>
		/// <param name="maxDate">The date to trim blocks from</param>
		/// <returns>
		/// The number of blocks trimmed. Will be zero if the tail block is already a
		/// summary or no untrimmed blocks before that date are found.
		/// </returns>
		/// <remarks>
		/// All branches stemming from the trimmed block(s)
		/// (except for the one with the longest proof-of-work)
		/// are detatched and may be deleted. Tentative blocks are not
		/// counted in the proof-of-work.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel argument is null
		/// </exception>
		int TrimFrom(string channel, DateTime maxDate);

		/// <summary>
		/// Gets the wallet associated with the given key
		/// </summary>
		/// <param name="key">The key of the wallet to return</param>
		/// <returns>The wallet associated under the given key, null otherwise</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key parameter is null
		/// </exception>
		IWallet GetWallet(string key);

		/// <summary>
		/// Adds or overwrites a wallet under the given key
		/// </summary>
		/// <param name="key">The key of store the wallet under</param>
		/// <param name="wallet">The wallet to store under the key</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		void SetWallet(string key, IWallet wallet);
	}
}