﻿using System;
using System.IO;
using System.Numerics;
using JLabs.Structures;

namespace CAWG.Hologram.Core.User
{
	public class Balance : ISerializable
	{
		public Balance(Account account, double discount, uint lastUpdatedInBlockIndex)
		{
			if (account == null)
				throw new ArgumentNullException("account");

			if (discount < 0)
				throw new ArgumentOutOfRangeException("discount", "Discount must be a non-negative number");

			Account = account;
			Discount = discount;
			LastUpdatedInBlockIndex = lastUpdatedInBlockIndex;
		}

		private double? _cachedWorth;

		public Account Account { get; private set; }
		public double Discount { get; private set; }
		public uint LastUpdatedInBlockIndex { get; private set; }

		public double CurrentWorth
		{
			get
			{
				if (_cachedWorth.HasValue)
					return _cachedWorth.Value;

				var baseWorth = BigInteger.Log(Account.Worth, 2);

				_cachedWorth = baseWorth - Discount;

				return _cachedWorth.Value;
			}
		}

		#region Serialization

		public static Balance FromByteArray(byte[] balance)
		{
			if (balance == null)
				throw new ArgumentNullException("balance");

			using (var ms = new MemoryStream())
			{
				ms.Write(balance, 0, balance.Length);
				ms.Seek(0, SeekOrigin.Begin);
				return ReadFrom(ms);
			}
		}

		public static Balance ReadFrom(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			var account = Account.ReadFrom(s);

			var buffer = new byte[8];

			s.Read(buffer, 0, buffer.Length);
			var discount = BitConverter.ToDouble(buffer, 0);

			s.Read(buffer, 0, 4);
			var lastUpdatedInBlockIndex = BitConverter.ToUInt32(buffer, 0);

			return new Balance(account, discount, lastUpdatedInBlockIndex);
		}

		public byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			Account.WriteTo(s);

			var buffer = BitConverter.GetBytes(Discount);
			s.Write(buffer, 0, buffer.Length);

			buffer = BitConverter.GetBytes(LastUpdatedInBlockIndex);
			s.Write(buffer, 0, buffer.Length);
		}

		#endregion Serialization

		#region Conversion

		public override string ToString()
		{
			return string.Format(
				"Balance for {0}, -{1}",
				Account, Discount
			);
		}

		#endregion Conversion
	}
}