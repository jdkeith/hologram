﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

using JLabs.Cryptography.EC;
using JLabs.Structures;
using JLabs.Structures.Utilities;

namespace CAWG.Hologram.Core.User
{
	/// <summary>
	/// Provides a way to receive and send funds as well as a stakeable proof-of-work
	/// to participate in the system
	/// </summary>
	public class Account : ISerializable, IComparable<Account>, IEquatable<Account>
	{
		#region Inner Classes

		/// <summary>
		/// Provides methods to compare accounts
		/// </summary>
		public class Comparer : IComparer<Account>
		{
			/// <summary>
			/// Compares two accounts and returns a relative ordering
			/// based on worth
			/// </summary>
			/// <param name="x">The first account in the comparison</param>
			/// <param name="y">The second account in the comparison</param>
			/// <returns>
			/// An integer indicating the relative ranking of the first
			/// account relative to the second account
			/// </returns>
			/// <remarks>
			/// Null accounts are ranked after non-null accounts
			/// </remarks>
			public int Compare(Account x, Account y)
			{
				if (ReferenceEquals(x, null))
					return ReferenceEquals(y, null) ? 0 : 1;

				return x.CompareTo(y);
			}
		}

		#endregion Inner Classes

		#region Fields and Constants

		private const byte IncludePrivateKeyFlag = 0x80;
		private const int PrivateKeyArraySize = 32;

		private BigInteger _computedWorth;

		private readonly BigInteger _privateKey;

		/// <summary>
		/// The maximum worth, in powers of two, that an account can represent
		/// </summary>
		public const double LogMaxWorth = 256;

		/// <summary>
		/// The maximum worth, as an integer, that an account can represent
		/// </summary>
		public static readonly BigInteger MaxWorth;

		#endregion Fields and Constants

		#region Functions

		/// <summary>
		/// Computes the worth of an elliptical curve point
		/// </summary>
		/// <param name="point">The point to compute the worth of</param>
		/// <returns>
		/// The computed worth of the elliptical curve point
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the point argument is null
		/// </exception>
		/// <remarks>
		/// A point's worth is the estimated number of trials, random or sequential,
		/// to produce a point whose X value is as small as the given point's X value is
		/// </remarks>
		public static BigInteger ComputeWorth(ECPoint point, bool mustBePaidAccount = false)
		{
			if (point == null)
				throw new ArgumentNullException("point");

			var buffer = point.X.Value.ToFixedSizeByteArray(32, reverse: true, trimLeadingZeros: true);

			if (mustBePaidAccount && buffer[31] == 0 || buffer[30] == 0)
				return 0;

			var logWorth = BigInteger.Log(buffer.ToNonNegativeBigInteger(), 2);

			logWorth = LogMaxWorth - logWorth;

			return new BigInteger(Math.Pow(2, logWorth));
		}

		#endregion Functions

		#region Constructors

		static Account()
		{
			MaxWorth = BigInteger.Pow(2, (int) LogMaxWorth);
		}

		/// <summary>
		/// Creates an account from a well-known type given a public key / ec point
		/// </summary>
		/// <param name="accountType">
		/// The type of account, that is which domain parameters the point belongs to
		/// </param>
		/// <param name="publicKey">The ec point representing the public key for the account</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the publicKey parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the given point is not on the curve defined by accountType
		/// </exception>
		public Account(ECCurveType accountType, ECPoint publicKey)
		{
			if (publicKey == null)
				throw new ArgumentNullException("publicKey");

			if (! ECCurve.GetByCurveType(accountType).IsOnCurve(publicKey))
				throw new ArgumentException("Public key is not on the curve given by the account type", "publicKey");

			AccountType = accountType;
			_privateKey = -1;
			PublicKey = publicKey;
		}

		/// <summary>
		/// Creates an account from a well-known type and private key
		/// </summary>
		/// <param name="accountType">
		/// The type of account, that is which domain parameters the public key point belongs to
		/// </param>
		/// <param name="privateKey">
		/// The private key as an integer offset from the generator point
		/// </param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if
		/// <para>Private key is not a positive number</para>
		/// <para>Private key is outside of the range allowed by accountType</para>
		/// </exception>
		public Account(ECCurveType accountType, BigInteger privateKey)
		{
			if( privateKey <= 0 )
				throw new ArgumentOutOfRangeException("privateKey", "Private key must be a positive number");

			AccountType = accountType;
			_privateKey = privateKey;

			var curve = ECCurve.GetByCurveType(accountType);

			if( privateKey >= curve.N )
				throw new ArgumentOutOfRangeException("privateKey", "Private key must be within bounds defined by accountType");

			PublicKey = curve.G * privateKey;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// The type of account, that is which domain parameters the point belongs to
		/// </summary>
		public ECCurveType AccountType { get; private set; }

		/// <summary>
		/// The ec point representing the public key for the account
		/// </summary>
		public ECPoint PublicKey { get; private set; }

		/// <summary>
		/// The private key as an integer offset from the generator point
		/// </summary>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the account's private key is not known
		/// </exception>
		public BigInteger PrivateKey
		{
			get
			{
				if (!HasPrivateKey)
					throw new InvalidOperationException("Account's private key is not known");

				return _privateKey;
			}
		}
		
		/// <summary>
		/// Whether or not the account's private key is known
		/// </summary>
		public bool HasPrivateKey
		{
			get
			{
				return _privateKey != -1;
			}
		}

		/// <summary>
		/// Returns the computed worth of the account
		/// </summary>
		/// <remarks>
		/// An account's worth is the estimated number of trials, random or sequential,
		/// to produce a public key whose X value is as small as the given account's public key's X value is
		/// </remarks>
		public BigInteger Worth
		{
			get
			{
				if (_computedWorth == 0)
					_computedWorth = ComputeWorth(PublicKey);

				return _computedWorth;
			}
		}

		#endregion Properties

		#region Serialization

		public static Account FromByteArray(byte[] account)
		{
			if (account == null)
				throw new ArgumentNullException("account");

			using (var ms = new MemoryStream())
			{
				ms.Write(account, 0, account.Length);
				ms.Seek(0, SeekOrigin.Begin);
				return ReadFrom(ms);
			}
		}

		public static Account ReadFrom(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			var tv = s.ReadByte();
			var includesPrivateKey = (tv & IncludePrivateKeyFlag) != 0x00;
			tv &= ~IncludePrivateKeyFlag;

			if (!Enum.IsDefined(typeof(ECCurveType), (byte)tv))
				throw new FormatException("Account cannot be deserialized");

			var type = (ECCurveType)tv;

			var curve = ECCurve.GetByCurveType(type);

			if (!includesPrivateKey)
			{
				// todo var point = parms.Curve.DecodeFrom(s);
				// return new Account(type, point);
			}

			var privateKeyBuffer = new byte[PrivateKeyArraySize];
			s.Read(privateKeyBuffer, 0, privateKeyBuffer.Length);

			return new Account(type, privateKeyBuffer.ToNonNegativeBigInteger());
		}

		public byte[] ToByteArray()
		{
			return ToByteArray(false);
		}

		public byte[] ToByteArray(bool includePrivateKey)
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		public void WriteTo(Stream s)
		{
			WriteTo(s, false);
		}

		public void WriteTo(Stream s, bool includePrivateKey)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			if( includePrivateKey && ! HasPrivateKey )
				throw new InvalidOperationException("Account's private key is not known");

			var type = (byte) AccountType;

			if( includePrivateKey )
				type |= IncludePrivateKeyFlag;

			s.WriteByte(type);

			if (includePrivateKey)
			{
				s.Write(PrivateKey.ToFixedSizeByteArray(PrivateKeyArraySize, trimLeadingZeros: true), 0, PrivateKeyArraySize);
				return;
			}

			var publicKeyArray = PublicKey.ToByteArray(true);

			s.Write(publicKeyArray, 0, publicKeyArray.Length);
		}

		#endregion Serialization

		#region Comparison and Equality

		public static bool operator ==(Account a, Account b)
		{
			if (ReferenceEquals(a, null))
				return ReferenceEquals(b, null);

			return a.Equals(b);
		}

		public static bool operator !=(Account a, Account b)
		{
			return !(a == b);
		}

		public static bool operator <(Account a, Account b)
		{
			if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
				return false;

			return a.CompareTo(b) < 0;
		}

		public static bool operator >(Account a, Account b)
		{
			if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
				return false;

			return a.CompareTo(b) > 0;
		}

		public int CompareTo(Account other)
		{
			if (ReferenceEquals(other, null))
				return -1;

			if (Equals(other))
				return 0;

			return Worth.CompareTo(other.Worth);
		}

		public bool Equals(Account other)
		{
			if (ReferenceEquals(other, null))
				return false;

			return PublicKey == other.PublicKey;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as Account);
		}

		public override int GetHashCode()
		{
			return PublicKey.GetHashCode();
		}

		#endregion Comparison and Equality

		#region Conversion

		public override string ToString()
		{
			return string.Format(
				"{0} {1}",
				AccountType,
				PublicKey.ToByteArray(true).ToHexString()
			);
		}

		#endregion Conversion
	}
}
