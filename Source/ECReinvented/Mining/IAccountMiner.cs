﻿using System;
using System.Numerics;
using System.Security.Cryptography;

using CAWG.Hologram.Core.User;
using JLabs.Cryptography.EC;

namespace CAWG.Hologram.Core.Mining
{
	public class AccountFoundEventArgs : EventArgs
	{
		public AccountFoundEventArgs(ECPoint basePoint, BigInteger? basePrivateKey, BigInteger multiplicand, Account foundAccount)
		{
			BasePoint = basePoint;
			BasePrivateKey = basePrivateKey;
			Multiplicand = multiplicand;
			FoundAccount = foundAccount;
		}

		public ECPoint BasePoint { get; private set; }
		public BigInteger? BasePrivateKey { get; private set; }
		public BigInteger Multiplicand{ get; private set; }
		public Account FoundAccount { get; private set; }
	}

	public interface IAccountMiner : IMiner
	{
		bool Start(double targetWorth, ECCurveType accountType, bool isPaidAccount = false, RandomNumberGenerator rnd = null);
		bool Start(BigInteger targetWorth, ECCurveType accountType, bool isPaidAccount = false, RandomNumberGenerator rnd = null);
		bool StartFrom(double targetWorth, Account buildFrom, bool isPaidAccount = false);
		bool StartFrom(BigInteger targetWorth, Account buildFrom, bool isPaidAccount = false);

		double OperationsPerSecond { get; }
		ulong TotalOperationsSinceStart { get; }
		
		event EventHandler<AccountFoundEventArgs> OnAccountFound;
	}
}