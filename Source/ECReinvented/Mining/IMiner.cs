﻿using System;

namespace CAWG.Hologram.Core.Mining
{
	public interface IMiner
	{
		void Stop();
		bool IsRunning { get; }

		event EventHandler OnStart;
		event EventHandler OnStop;
	}
}
