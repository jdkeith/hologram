﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;
using CAWG.Hologram.Core.DB;
using JLabs.Structures;
using JLabs.Structures.Cryptography;
using JLabs.Structures.Units;

namespace CAWG.Hologram.Core.Chain
{
	public class BlockHeaderSummary : ISerializable
	{
		public static double ComputeWorth(double startingWorth, params BlockHeaderSummary[] blocks)
		{
			if (startingWorth < 0)
				throw new ArgumentOutOfRangeException("startingWorth");

			double runningTotal;

			if (startingWorth == 0)
				runningTotal = 0;
			else
				runningTotal = Math.Pow(2, startingWorth);

			foreach (var block in blocks)
			{
				runningTotal += Math.Pow(
					2, Constants.MaxBlockLogDifficulty - BigInteger.Log(block.Id.ToByteArray().ToNonNegativeBigInteger(), 2)
				);
			}

			return Math.Log(runningTotal, 2);
		}

		public BlockHeaderSummary(
			string channel, byte version,
			SHA256Hash parentBlockId, uint index,
			SHA256Hash contentSummary, ulong nonce
		)
		{
			Channel = channel;
			Version = version;
			ParentBlockId = parentBlockId;
			Index = index;
			ContentSummary = contentSummary;
			Nonce = nonce;
		}

		protected BlockHeaderSummary(
			string channel, byte version,
			SHA256Hash parentBlockId, uint index, ulong nonce
		)
			: this(channel, version, parentBlockId, index, Hash.SHA256.MaxValue, nonce) { }

		public SHA256Hash Id
		{
			get
			{
				var bytes = new List<byte>();

				bytes.Add(Version);

				var channelBytes = Encoding.UTF8.GetBytes(Channel);

				bytes.Add((byte) channelBytes.Length);
				bytes.AddRange(channelBytes);

				bytes.AddRange(ParentBlockId.ToByteArray());
				bytes.AddRange(BitConverter.GetBytes(Index));

				bytes.AddRange(ContentSummary.ToByteArray());
				bytes.AddRange(BitConverter.GetBytes(Nonce));

				return SHA256Hash.Factory.Of(bytes.ToArray());
			}
		}

		public string Channel { get; private set; }
		public byte Version { get; private set; }

		public SHA256Hash ParentBlockId { get; private set; }
		public uint Index { get; private set; }
	
		public virtual SHA256Hash ContentSummary { get; private set; }
		
		public ulong Nonce { get; protected set; }

		public static BlockHeaderSummary ReadFrom(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			switch (s.ReadByte())
			{
				case 0xf2:
					// todo read the full block -- internal
					throw new NotImplementedException();

				case 0xf1:
					// todo read the header -- internal
					throw new NotImplementedException();

				case 0xf0:
					break;

				default:
					throw new FormatException("The stream does not contain a valid BlockHeaderSummary");
			}

			var channelBytes = new byte[s.ReadByte()];
			s.Read(channelBytes, 0, channelBytes.Length);
			var channel = Encoding.UTF8.GetString(channelBytes);

			var version = (byte) s.ReadByte();

			var previousId = SHA256Hash.Factory.ReadFrom(s);

			var indexBytes = new byte[4];
			s.Read(indexBytes, 0, indexBytes.Length);
			var index = BitConverter.ToUInt32(indexBytes, 0);

			var contentSummary = SHA256Hash.Factory.ReadFrom(s);

			var nonceBytes = new byte[8];
			s.Read(nonceBytes, 0, nonceBytes.Length);
			var nonce = BitConverter.ToUInt64(nonceBytes, 0);

			return new BlockHeaderSummary(channel, version, previousId, index, contentSummary, nonce);
		}

		public byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		public virtual void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			s.WriteByte(0xf0);

			var channelBytes = Encoding.UTF8.GetBytes(Channel);

			s.WriteByte((byte) channelBytes.Length);
			s.Write(channelBytes, 0, channelBytes.Length);

			s.WriteByte(Version);

			ParentBlockId.WriteTo(s);

			var indexBytes = BitConverter.GetBytes(Index);

			s.Write(indexBytes, 0, indexBytes.Length);
		
			ContentSummary.WriteTo(s);
	
			var nonceBytes = BitConverter.GetBytes(Nonce);

			s.Write(nonceBytes, 0, nonceBytes.Length);	
		}

		public BlockHeaderSummary Summarize()
		{
			return new BlockHeaderSummary(
				Channel, Version, ParentBlockId, Index, ContentSummary, Nonce
			);
		}
	}
}
