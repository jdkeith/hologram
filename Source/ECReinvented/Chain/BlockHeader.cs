﻿using System;
using System.Collections.Generic;
using JLabs.Structures;
using JLabs.Structures.Cryptography;

namespace CAWG.Hologram.Core.Chain
{
	public class BlockHeader : BlockHeaderSummary
	{
		public static DateTime Epoch { get; private set; }

		public BlockHeader(
			string channel, byte version,
			SHA256Hash previousId, uint index, ulong nonce,
			byte[] chainedToCurrencyBlockId,
			DateTime timeUTC, double difficulty, double difficultyDiscount
		) : base(
			channel, version, previousId, index, nonce
		)
		{
			ChainedToCurrencyBlockId = chainedToCurrencyBlockId;
			TimeUTC = timeUTC;
			Difficulty = difficulty;
			DifficultyDiscount = difficultyDiscount;
		}

		public SHA256Hash ContentSummary
		{
			get
			{
				if (DifficultyDiscount < 0 || Difficulty < 0)
					return SHA256Hash.Factory.MaxValue;

				var bytes = new List<byte>();

				bytes.AddRange(
					BitConverter.GetBytes((ulong)TimeUTC.Subtract(Epoch).TotalSeconds)
				);

				bytes.AddRange(BitConverter.GetBytes(Difficulty));
				bytes.AddRange(BitConverter.GetBytes(DifficultyDiscount));

		//		bytes.AddRange(OperationsMerkleRoot.ToByteArray());
			//	bytes.AddRange(BalanceAdjustmentsMerkleRoot.ToByteArray());
				bytes.Add((byte)ChainedToCurrencyBlockId.Length);
				bytes.AddRange(ChainedToCurrencyBlockId);

				return SHA256Hash.Factory.Of(bytes.ToArray());
			}
		}

		public DateTime TimeUTC { get; private set; }
		public double Difficulty { get; private set; }
		public double DifficultyDiscount { get; private set; }

	//	public ContentMerkleTree<Operation> Operations { get; protected set; }
	//	public ContentMerkleTree<Balance> BalanceAdjustments { get; protected set; }
		public byte[] ChainedToCurrencyBlockId { get; protected set; }
	}
}
