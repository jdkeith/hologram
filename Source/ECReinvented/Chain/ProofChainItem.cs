﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;
using CAWG.Hologram.Core;
using CAWG.Hologram.Core.Chain;
using JLabs.Structures;

namespace CAWG.Hologram.Chain
{
	public class ProofChainItem : ISerializable
	{
		public static double ComputeWorth(double startingWorth, params BlockHeaderSummary[] blocks)
		{
			if (startingWorth < 0)
				throw new ArgumentOutOfRangeException("startingWorth");

			double runningTotal;

			if (startingWorth == 0)
				runningTotal = 0;
			else
				runningTotal = Math.Pow(2, startingWorth);

			foreach (var block in blocks)
			{
				runningTotal += Math.Pow(
					2, Constants.MaxBlockLogDifficulty - BigInteger.Log(block.Id.ToByteArray().ToNonNegativeBigInteger(), 2)
				);
			}

			return Math.Log(runningTotal, 2);
		}

		public ProofChainItem(ProofChainItem parent, SHA256Hash id)
		{
			ParentBlockId = parent == null ? SHA256Hash.MinValue : parent.Id;
			Id = id;
		}

		public ProofChainItem(SHA256Hash parentBlockId, SHA256Hash id)
		{
			ParentBlockId = parentBlockId;
			Id = id;
		}

		protected ProofChainItem(ProofChainItem parent)
			: this(parent, SHA256Hash.MinValue)
		{
		}

		protected ProofChainItem(SHA256Hash parentBlockId)
			: this(parentBlockId, SHA256Hash.MinValue)
		{
		}

		public virtual SHA256Hash Id { get; private set; }
		public SHA256Hash ParentBlockId { get; private set; }
		
		public static BlockHeaderSummary ReadFrom(Stream s)
		{
			throw new System.NotImplementedException();

			if (s == null)
				throw new ArgumentNullException("s");

			//return new ProofChainItem(

			//);

			switch (s.ReadByte())
			{
				case 0xf2:
					// todo read the full block -- internal
					throw new NotImplementedException();

				case 0xf1:
					// todo read the header -- internal
					throw new NotImplementedException();

				case 0xf0:
					break;

				default:
					throw new FormatException("The stream does not contain a valid BlockHeaderSummary");
			}
		}

		public virtual byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		public virtual void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			ParentBlockId.WriteTo(s);
			Id.WriteTo(s);
		}
	}
}
