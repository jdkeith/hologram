﻿using System;

using CAWG.Hologram.Core.User;

using JLabs.Structures;
using JLabs.Structures.Cryptography;
using JLabs.Structures.Units;
using JLabs.Structures.Utilities;

namespace CAWG.Hologram.Core.Chain
{
	public class Block : BlockHeader
	{
		public Block()
			: base(
				"TEMP", 0, SHA256Hash.Factory.MinValue, 0, 0,
				"3497823fb974023b148723b0069423f04723f23b74783029b8f3432f8479bf".FromHexString(),
				DateTime.Now, 0, 0
			)
		{

		}

		public Account MinersAddress { get; private set; }
	}
}
