﻿using System;
using System.Text.RegularExpressions;

namespace CAWG.Hologram.Core
{
	/// <summary>
	/// Contains constant values for the Hologram ecosystem
	/// </summary>
	public static class Constants
	{
		/// <summary>
		/// The maximum length that a channel name can have
		/// </summary>
		public const int MaxChannelNameLength = 8;

		/// <summary>
		/// A channel name validator regular expression. All channel names must meet the regex
		/// </summary>
		public static readonly Regex ChannelValidator = new Regex(@"^[A-Z0-9]{1," + MaxChannelNameLength + "}$");

		/// <summary>
		/// The total duration of the miniblock chain. Traders have a maximum of this
		/// amount of time to settle transactions.
		/// </summary>
		/// <remarks>
		/// Blocks older than this period are effectively cemented into place
		/// </remarks>
		public static readonly TimeSpan ActivePeriod = new TimeSpan(30, 0, 0, 0);

		/// <summary>
		/// The maximum amount of time that a new block can deviate from a node's clock
		/// if it is to be accepted and relayed by the node
		/// </summary>
		/// <remarks>
		/// Note that blocks which have been built upon and have a date in the past will
		/// always be accepted and relayed if they are otherwise valid
		/// </remarks>
		public static readonly TimeSpan MaxBlockTimeBias = new TimeSpan(0, 5, 0);

		/// <summary>
		/// The maximum difficulty that a block's id may have
		/// </summary>
		public static double MaxBlockLogDifficulty = 256;

		/// <summary>
		/// The base time (time zero) from which all time values in the system are offset
		/// </summary>
		public static DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
	}
}