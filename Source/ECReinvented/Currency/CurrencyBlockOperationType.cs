﻿using System;

namespace CAWG.Hologram.Core.Currency
{
	public enum CurrencyBlockOperationType : byte
	{
		Unknown,
		Release,
		Escrow,
		Data
	}
}
