﻿using System;

using CAWG.Hologram.Core.User;

namespace CAWG.Hologram.Core.Currency
{
	public class CurrencyBlockOperation
	{
		public byte[] Id;
		public Account From;
		public Account To;
		public ulong Amount;
		public byte[] Data;
		public CurrencyBlockOperationType Type;
	}
}
