﻿using System;
using System.Collections.Generic;

namespace CAWG.Hologram.Core.Currency
{
	public class CurrencyBlock
	{
		public string Channel;
		public uint Index;
		public byte[] Id;
		public byte[] ParentId;
		public IEnumerable<CurrencyBlockOperation> Operations;
	}
}
