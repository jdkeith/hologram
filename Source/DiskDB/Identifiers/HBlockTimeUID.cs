﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using CAWG.Hologram.Core;
using CAWG.Hologram.Core.Chain;

namespace Hologram.Storage.Identifiers
{
	public class HBlockTimeUID : UID
	{
		public class Comparer : IComparer<HBlockTimeUID>
		{
			public int Compare(HBlockTimeUID x, HBlockTimeUID y)
			{
				var compare = x.Channel.CompareTo(y.Channel);

				if (compare != 0)
					return compare;

				return x.BlockTime.CompareTo(y.BlockTime);
			}
		}

		public static HBlockTimeUID ReadFrom(Stream s)
		{
			// the type is already assumed to have been read
			var channel = ReadFixedSizeChannel(s);

			var totalSecondsBytes = new byte[sizeof(ulong)];
			s.Read(totalSecondsBytes, 0, totalSecondsBytes.Length);
			Array.Reverse(totalSecondsBytes);

			var blockTime = Constants.Epoch.AddSeconds(BitConverter.ToUInt64(totalSecondsBytes, 0));

			return new HBlockTimeUID(channel, blockTime);
		}

		public HBlockTimeUID(BlockHeader block)
			: base(block.Channel)
		{
			BlockTime = block.TimeUTC;
		}

		public HBlockTimeUID(string channel, DateTime blockTime)
			: base(channel)
		{
			BlockTime = blockTime;

			var totalSecondsSinceEpoch = (ulong)
				DateTime.SpecifyKind(blockTime, DateTimeKind.Utc)
				.Subtract(Constants.Epoch)
				.TotalSeconds;

			var totalSecondsBytes = BitConverter.GetBytes(totalSecondsSinceEpoch);
			Array.Reverse(totalSecondsBytes);

			Lookup = new byte[] { (byte)UID.UIDType.HBlockByDate }
				.Concat(totalSecondsBytes)
				.ToArray();
		}

		public DateTime BlockTime { get; private set; }

		public override string ToString()
		{
			return string.Format("{0,-8} {1:yyyy-MM-dd HH:mm:ss} UTC", Channel, BlockTime);
		}
	}
}