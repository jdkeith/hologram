﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

using JLabs.Structures;
using JLabs.Structures.Cryptography;

namespace Hologram.Storage.Identifiers
{
	public class HBlockIdUID : UID
	{
		public class Comparer : IComparer<HBlockIdUID>
		{
			public int Compare(HBlockIdUID x, HBlockIdUID y)
			{
				var compare = x.Channel.CompareTo(y.Channel);

				if (compare != 0)
					return compare;

				return x.BlockId.CompareTo(y.BlockId);
			}
		}

		public static HBlockIdUID ReadFrom(Stream s)
		{
			// the type is already assumed to have been read
			var channel = ReadFixedSizeChannel(s);
			var blockId = SHA256Hash.Factory.ReadFrom(s);

			return new HBlockIdUID(channel, blockId);
		}

		public HBlockIdUID(string channel, SHA256Hash blockId)
			: base(channel)
		{
			BlockId = blockId;

			var channelBytes = GetFixedSizeChannelBytes(channel);

			Lookup = new byte[] { (byte)UID.UIDType.HBlockById }
				.Concat(channelBytes)
				.Concat(blockId.ToByteArray())
				.ToArray();
		}

		public SHA256Hash BlockId { get; private set; }

		public override string ToString()
		{
			return string.Format("{0,-8} {1}", Channel, BlockId);
		}
	}
}