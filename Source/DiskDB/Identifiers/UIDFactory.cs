﻿using System;

using CAWG.Hologram.Core.User;
using JLabs.Cryptography.EC;
using JLabs.Structures;
using JLabs.Structures.Cryptography;

namespace Hologram.Storage.Identifiers
{
	/// <summary>
	/// Computes B+ tree keys and key ranges for various lookup scenarios
	/// </summary>
	public static class UIDFactory
	{
		#region Block

		public static UIDRange<HBlockIdUID> TailsByChannel(string channel)
		{
			var min = new HBlockIdUID(channel, SHA256Hash.Factory.MinValue);
			var max = new HBlockIdUID(channel, SHA256Hash.Factory.MaxValue);

			return new UIDRange<HBlockIdUID>(min, max);
		}

		/// <summary>
		/// Returns a key range which will match all blocks in a channel
		/// </summary>
		/// <param name="channel">The channel matched items will appear within</param>
		/// <returns>A key range which will match all blocks in a channel</returns>
		public static UIDRange<HBlockIndexUID> BlocksByChannel(string channel)
		{
			var min = new HBlockIndexUID(channel, uint.MinValue);
			var max = new HBlockIndexUID(channel, uint.MaxValue);

			return new UIDRange<HBlockIndexUID>(min, max);
		}

		public static UIDRange<HBlockIndexUID> BlocksByIndexRange(string channel, uint minIndex, uint maxIndex)
		{
			var min = new HBlockIndexUID(channel, minIndex);
			var max = new HBlockIndexUID(channel, maxIndex);

			return new UIDRange<HBlockIndexUID>(min, max);
		}


		public static UIDRange<HBlockTimeUID> BlocksByBlockTimeRange(string channel, DateTime minBlockTime, DateTime maxBlockTime)
		{
			var min = new HBlockTimeUID(channel, minBlockTime);
			var max = new HBlockTimeUID(channel, maxBlockTime);

			return new UIDRange<HBlockTimeUID>(min, max);
		}

		#endregion Block

		#region Account

		//public static SHA256Hash ByAccount(string channel, Account account)
		//{
		//    if (account == null)
		//        throw new ArgumentNullException("account");

		//    return ByAccount(channel, account.AccountType, account.PublicKey);
		//}

		//public static SHA256Hash ByAccount(string channel, WellKnownDomainParametersType accountType, ECPoint publicKey)
		//{
		//    var channelBytes = GetFixedSizeChannelBytes(channel);
		//    var typeByte = (byte)accountType;
		//    var publicKeyBytes = publicKey.ToByteArray();

		//    return SHA256Hash.FromByteArray(
		//        channelBytes
		//            .Concat(new[] {typeByte})
		//            .Concat(RIPEMD160Hash.Of(publicKeyBytes).ToByteArray())
		//            .Concat(new byte[SHA256Hash.Size])
		//            .Take(SHA256Hash.Size)
		//            .ToArray()
		//    );
		//}

		#endregion Account

		#region Balances

		public static UIDRange<BalanceUID> ByAccountBalance(string channel, Account account)
		{
			if (account == null)
				throw new ArgumentNullException("account");

			return new UIDRange<BalanceUID>(
				ByAccountBalance(channel, account.AccountType, account.PublicKey, uint.MinValue),
				ByAccountBalance(channel, account.AccountType, account.PublicKey, uint.MaxValue)
			);
		}

		public static UIDRange<BalanceUID> ByAccountBalance(string channel, ECCurveType accountType, ECPoint publicKey)
		{
			return new UIDRange<BalanceUID>(
				ByAccountBalance(channel, accountType, publicKey, uint.MinValue),
				ByAccountBalance(channel, accountType, publicKey, uint.MaxValue)
			);
		}

		public static BalanceUID ByAccountBalance(string channel, ECCurveType accountType, ECPoint publicKey, uint balanceBlockIndex)
		{
			var account = new Account(accountType, publicKey);

			return ByAccountBalance(channel, account, balanceBlockIndex);
		}

		public static BalanceUID ByAccountBalance(string channel, Account account, uint balanceBlockIndex)
		{
			if (account == null)
				throw new ArgumentNullException("account");

			return new BalanceUID(channel, SHA256Hash.Factory.Of(account), balanceBlockIndex);
		}

		public static BalanceUID GetPreviousReferencedBalanceId(string channel, Balance balance)
		{
			if (balance == null)
				throw new ArgumentNullException("balance");

			return ByAccountBalance(channel, balance.Account, balance.LastUpdatedInBlockIndex);
		}

		#endregion Balances
	}
}