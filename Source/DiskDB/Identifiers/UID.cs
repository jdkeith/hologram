﻿using System;
using System.IO;
using System.Linq;
using System.Text;

using CAWG.Hologram.Core;

namespace Hologram.Storage.Identifiers
{
	public abstract class UID
	{
		protected enum UIDType : byte
		{
			HBlockById,
			HBlockByIndex,
			HBlockByDate,
			Balance
		}

		public static UID ReadFrom(Stream s)
		{
			var t = (byte) s.ReadByte();

			switch ((UIDType)t)
			{
				case UIDType.HBlockById:
					return HBlockIdUID.ReadFrom(s);

				case UIDType.HBlockByIndex:
					return HBlockIndexUID.ReadFrom(s);

				case UIDType.HBlockByDate:
					return HBlockTimeUID.ReadFrom(s);

				case UIDType.Balance:
					return BalanceUID.ReadFrom(s);

				default:
					throw new FormatException("Unrecognized UID type");
			}
		}

		protected UID(string channel)
		{
			Channel = channel;
		}

		public string Channel { get; private set; }
		public byte[] Lookup { get; protected set; }

		#region Utilities

		protected static byte[] MaxBytes(int length)
		{
			return Enumerable.Repeat<byte>(byte.MaxValue, length).ToArray();
		}

		protected static byte[] GetFixedSizeChannelBytes(string channel)
		{
			if (channel == null)
				throw new ArgumentNullException("channel");

			if (!Constants.ChannelValidator.IsMatch(channel))
				throw new ArgumentException("Channel name is not valid", "channel");

			var channelBytes = Encoding.UTF8.GetBytes(channel);

			var result = new byte[Constants.MaxChannelNameLength];

			Array.Copy(channelBytes, result, channelBytes.Length);

			return result;
		}

		protected static string ReadFixedSizeChannel(Stream s)
		{
			var channelBytes = new byte[Constants.MaxChannelNameLength];

			s.Read(channelBytes, 0, channelBytes.Length);

			return Encoding.UTF8.GetString(
				channelBytes.TakeWhile(cb => cb != 0x00).ToArray()
			);
		}

		#endregion Utilities
	}
}