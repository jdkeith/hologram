﻿namespace Hologram.Storage.Identifiers
{
	/// <summary>
	/// Contains two B+ tree key values for inclusive range searches
	/// </summary>
	public class UIDRange<TUID> where TUID : UID
	{
		/// <summary>
		/// Creates a new range
		/// </summary>
		/// <param name="min">The minimum key which a matching object will have</param>
		/// <param name="max">The maximum key which a matching object will have</param>
		public UIDRange(TUID min, TUID max)
		{
			Min = min;
			Max = max;
		}

		/// <summary>
		/// The minimum key which a matching object will have
		/// </summary>
		public TUID Min { get; private set; }

		/// <summary>
		/// The maximum key which a matching object will have
		/// </summary>
		public TUID Max { get; private set; }
	}
}
