﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using CAWG.Hologram.Core;
using CAWG.Hologram.Core.Chain;

namespace Hologram.Storage.Identifiers
{
	public class HBlockIndexUID : UID
	{
		public class Comparer : IComparer<HBlockIndexUID>
		{
			public int Compare(HBlockIndexUID x, HBlockIndexUID y)
			{
				var compare = x.Channel.CompareTo(y.Channel);

				if (compare != 0)
					return compare;

				return x.Index.CompareTo(y.Index);
			}
		}

		public static HBlockIndexUID ReadFrom(Stream s)
		{
			// the type is already assumed to have been read
			var channel = ReadFixedSizeChannel(s);

			var indexBytes = new byte[sizeof(uint)];
			s.Read(indexBytes, 0, indexBytes.Length);
			Array.Reverse(indexBytes);

			var index = BitConverter.ToUInt32(indexBytes, 0);

			return new HBlockIndexUID(channel, index);
		}

		public HBlockIndexUID(BlockHeaderSummary block)
			: base(block.Channel)
		{
			Index = block.Index;
		}

		public HBlockIndexUID(string channel, uint index)
			: base(channel)
		{
			Index = index;

			var channelBytes = GetFixedSizeChannelBytes(channel);

			var indexBytes = BitConverter.GetBytes(index);
			Array.Reverse(indexBytes);

			Lookup = new byte[] { (byte)UID.UIDType.HBlockByIndex }
				.Concat(channelBytes)
				.Concat(indexBytes)
				.ToArray();
		}

		public uint Index { get; private set; }

		public override string ToString()
		{
			return string.Format("{0,-8} {1,10}", Channel, Index);
		}
	}
}