﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using CAWG.Hologram.Core;
using JLabs.Structures;
using JLabs.Structures.Cryptography;

namespace Hologram.Storage.Identifiers
{
	public class BalanceUID : UID
	{
		public class Comparer : IComparer<BalanceUID>
		{
			public int Compare(BalanceUID x, BalanceUID y)
			{
				var compare = x.Channel.CompareTo(y.Channel);

				if (compare != 0)
					return compare;

				compare = x.AccountId.CompareTo(y.AccountId);

				if (compare != 0)
					return compare;

				return x.Index.CompareTo(y.Index);
			}
		}

		public static BalanceUID ReadFrom(Stream s)
		{
			// the type is already assumed to have been read
			var channel = ReadFixedSizeChannel(s);
			var accountId = SHA256Hash.Factory.ReadFrom(s);

			var indexBytes = new byte[sizeof(uint)];
			s.Read(indexBytes, 0, indexBytes.Length);
			Array.Reverse(indexBytes);

			var index = BitConverter.ToUInt32(indexBytes, 0);

			return new BalanceUID(channel, accountId, index);
		}

		public BalanceUID(string channel, SHA256Hash accountId, uint index)
			: base(channel)
		{
			AccountId = accountId;
			Index = index;

			var channelBytes = GetFixedSizeChannelBytes(channel);

			var indexBytes = BitConverter.GetBytes(index);
			Array.Reverse(indexBytes);

			Lookup = new byte[] { (byte)UID.UIDType.Balance }
				.Concat(channelBytes)
				.Concat(indexBytes)
				.Concat(accountId.ToByteArray())
				.ToArray();
		}

		public SHA256Hash AccountId { get; private set; }
		public uint Index { get; private set; }

		public override string ToString()
		{
			return string.Format("{0,-8} {1} {2,10}", Channel, AccountId, Index);
		}
	}
}