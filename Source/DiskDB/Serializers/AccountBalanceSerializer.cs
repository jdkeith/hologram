﻿using System;
using System.IO;

using CAWG.Hologram.Core.User;

using CSharpTest.Net.Serialization;

namespace Hologram.Storage.Serializers
{
	public class AccountBalanceSerializer : ISerializer<Balance>
	{
		public Balance ReadFrom(Stream stream)
		{
			return Balance.ReadFrom(stream);
		}

		public void WriteTo(Balance value, Stream stream)
		{
			value.WriteTo(stream);
		}
	}
}