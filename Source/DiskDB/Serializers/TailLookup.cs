﻿using System;
using System.IO;
using CSharpTest.Net.Serialization;
using JLabs.Structures;
using JLabs.Structures.Cryptography;

namespace Hologram.Storage.Serializers
{
	public class TailLookup
	{
		public class Serializer : ISerializer<TailLookup>
		{
			public TailLookup ReadFrom(Stream stream)
			{
				var blockId = SHA256Hash.Factory.ReadFrom(stream);
				var nearestAncestorNonTentativeId = SHA256Hash.Factory.ReadFrom(stream);
			
				var apowBytes = new byte[sizeof(double)];
				stream.Read(apowBytes, 0, apowBytes.Length);
				var apow = BitConverter.ToDouble(apowBytes, 0);

				return new TailLookup(blockId, nearestAncestorNonTentativeId, apow);
			}

			public void WriteTo(TailLookup value, Stream stream)
			{
				value.BlockId.WriteTo(stream);
				value.NearestAncestorNonTentativeId.WriteTo(stream);

				stream.Write(BitConverter.GetBytes(value.AccumulatedProofOfWork), 0, sizeof(double));
			}
		}

		public TailLookup(SHA256Hash blockId, SHA256Hash nearestAncestorNonTentativeId, double accumulatedProofOfWork)
		{
			BlockId = blockId;
			NearestAncestorNonTentativeId = nearestAncestorNonTentativeId;
			AccumulatedProofOfWork = accumulatedProofOfWork;
		}

		public SHA256Hash BlockId { get; private set; }
		public SHA256Hash NearestAncestorNonTentativeId { get; private set; }
		public double AccumulatedProofOfWork { get; private set; }

		public bool IsTentative
		{
			get { return BlockId == NearestAncestorNonTentativeId; }
		}

		public void ClearTentative()
		{
			NearestAncestorNonTentativeId = BlockId;
		}
	}
}