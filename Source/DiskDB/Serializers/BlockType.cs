﻿namespace Hologram.Storage.Serializers
{
	public enum HBlockType : byte
	{
		BlockHeaderSummary,
		BlockHeader,
		Block
	}
}
