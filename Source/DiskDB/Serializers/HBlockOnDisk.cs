﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using CAWG.Hologram.Core;
using CAWG.Hologram.Core.Chain;
using CSharpTest.Net.Serialization;
using JLabs.Structures;
using JLabs.Structures.Cryptography;
using JLabs.Structures.Units;

namespace Hologram.Storage.Serializers
{
	public class HBlockOnDisk
	{
		public class Serializer : ISerializer<HBlockOnDisk>
		{
			public HBlockOnDisk ReadFrom(Stream stream)
			{
				var typeByte = (byte)stream.ReadByte();

				if (!Enum.IsDefined(typeof(HBlockType), typeByte))
					throw new FormatException("Invalid block type");

				var type = (HBlockType)typeByte;

				// parent block id
				var parentBlockId = SHA256Hash.Factory.ReadFrom(stream);
				
				// accumulated pow
				var apowBytes = new byte[sizeof(double)];
				stream.Read(apowBytes, 0, apowBytes.Length);
				var apow = BitConverter.ToDouble(apowBytes, 0);

				// cblockref
				SHA256Hash cblockRefId;

				if (type == HBlockType.BlockHeaderSummary)
					cblockRefId = SHA256Hash.Factory.MinValue;
				else
					cblockRefId = SHA256Hash.Factory.ReadFrom(stream);

				var referencedAltChains = new List<string>();

				var vuif = VarUInt.Factory;

				if (type == HBlockType.Block)
				{
                    // lame ----------v
					var refLength = (uint) (ulong) vuif.ReadFrom(stream);

					for( var i=0; i< refLength; i++ )
					{
						var channelNameBuffer = new byte[Constants.MaxChannelNameLength];

						stream.Read(channelNameBuffer, 0, channelNameBuffer.Length);

						referencedAltChains.Add(
							Encoding.UTF8.GetString(channelNameBuffer.TakeWhile( b => b != 0x00 ).ToArray() )
						);
					}
				}

				var blockLength = vuif.ReadFrom(stream);

				var blockData = new byte[blockLength];

				stream.Read(blockData, 0, blockData.Length);

				return new HBlockOnDisk(blockData)
				{
					AccumulatedProofOfWork = apow,
					ParentBlockId = parentBlockId,
					CBlockRefId = cblockRefId,
					ReferencedAltChains = referencedAltChains,
					Type = type
				};
			}

			public void WriteTo(HBlockOnDisk value, Stream stream)
			{
				// type
				stream.WriteByte((byte)value.Type);

				// parent block id
				value.ParentBlockId.WriteTo(stream);

				// accumulated pow
				stream.Write(BitConverter.GetBytes(value.AccumulatedProofOfWork), 0, sizeof(double));

				// cblock ref
				if (value.Type == HBlockType.BlockHeaderSummary)
					value.CBlockRefId.WriteTo(stream);

				// referenced alt chains
				if (value.Type == HBlockType.Block)
				{
					((VarUInt)(uint)value.ReferencedAltChains.Count()).WriteTo(stream);

					foreach( var referencedAltChain in value.ReferencedAltChains)
					{
						var channelNameBuffer = Encoding.UTF8.GetBytes(referencedAltChain);

						stream.Write(
							channelNameBuffer
								.Concat(Enumerable.Repeat(byte.MinValue, Constants.MaxChannelNameLength - channelNameBuffer.Length))
								.ToArray(),
							0,
							Constants.MaxChannelNameLength
						);
					}
				}

				((VarUInt) (uint) value._blockData.Length).WriteTo(stream);

				stream.Write(value._blockData, 0, value._blockData.Length);
			}
		}

		private HBlockOnDisk(byte[] blockData)
		{
			_blockData = blockData;
		}

		private readonly byte[] _blockData;
		private BlockHeaderSummary _cachedDeserializedBlock;

		public HBlockOnDisk(BlockHeaderSummary block, double previousAccumulatedProofOfWork)
		{
			AccumulatedProofOfWork = BlockHeaderSummary.ComputeWorth(previousAccumulatedProofOfWork, block);
			ParentBlockId = block.ParentBlockId;
			Type = HBlockType.BlockHeaderSummary;
			_blockData = block.ToByteArray();
			CBlockRefId = SHA256Hash.Factory.MinValue;
			ReferencedAltChains = Enumerable.Empty<string>();
		}

		public HBlockOnDisk(BlockHeader block, double previousAccumulatedProofOfWork)
			: this((BlockHeaderSummary) block, previousAccumulatedProofOfWork)
		{
			Type = HBlockType.BlockHeader;
			CBlockRefId = SHA256Hash.Factory.Of(block.ChainedToCurrencyBlockId);
		}

		public HBlockOnDisk(Block block, double previousAccumulatedProofOfWork)
			: this((BlockHeader) block, previousAccumulatedProofOfWork)
		{
			Type = HBlockType.Block;
			// todo operations into ReferencedAltChains
		}

		public SHA256Hash ParentBlockId { get; private set; }
		public double AccumulatedProofOfWork { get; private set; }
		public HBlockType Type { get; private set; }
		public SHA256Hash CBlockRefId { get; private set; }
		public IEnumerable<string> ReferencedAltChains { get; private set; }

		public HBlockOnDisk Trim()
		{
			if (Type == HBlockType.BlockHeaderSummary)
				return this;

			var block = ExtractBlock();

			var summary = new BlockHeaderSummary(
				block.Channel, block.Version, block.ParentBlockId, block.Index, block.ContentSummary, block.Nonce
			);

			return new HBlockOnDisk(summary, AccumulatedProofOfWork);
		}

		public BlockHeaderSummary ExtractBlock()
		{
			if (_cachedDeserializedBlock != null)
				return _cachedDeserializedBlock;

			// todo -- blocks should be able to read from bytes?
			using (var ms = new MemoryStream())
			{
				ms.Write(_blockData, 0, _blockData.Length);
				ms.Seek(0, SeekOrigin.Begin);

				switch (Type)
				{
					case HBlockType.BlockHeaderSummary:
						_cachedDeserializedBlock = BlockHeaderSummary.ReadFrom(ms);
						break;

					case HBlockType.BlockHeader:
						_cachedDeserializedBlock = BlockHeader.ReadFrom(ms);
						break;

					case HBlockType.Block:
						_cachedDeserializedBlock = Block.ReadFrom(ms);
						break;

					default:
						throw new FormatException("Could not parse block from byte representation");
				}
			}

			return _cachedDeserializedBlock;
		}
	}
}