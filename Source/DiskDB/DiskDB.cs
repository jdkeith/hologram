﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CAWG.Hologram.Core.Chain;
using CAWG.Hologram.Core.Currency;
using CAWG.Hologram.Core.DB;
using CAWG.Hologram.Core.User;

using CSharpTest.Net.Collections;
using CSharpTest.Net.Synchronization;
using Hologram.Storage.Identifiers;
using Hologram.Storage.Serializers;
using JLabs.Structures;
using JLabs.Structures.Cryptography;

namespace Hologram.Storage
{
	/// <summary>
	/// Stores client data to disk using B+ trees
	/// </summary>
	public class DiskDB : IDB
	{
		#region Fields

		private readonly string _dataDirectory;

		// lookups
		private readonly BPlusTree<HBlockIndexUID, HBlockType> _blocksByIndex;
		private readonly BPlusTree<SHA256Hash, HBlockOnDisk> _blocks;
		private readonly BPlusTree<SHA256Hash, SHA256Hash> _children;
		private readonly BPlusTree<HBlockIdUID, TailLookup> _tails;

		#endregion Fields

		#region Constructor

		public DiskDB(string dataDirectory)
		{
			if (dataDirectory == null)
				throw new ArgumentNullException("dataDirectory");

			if (!Directory.Exists(dataDirectory))
				throw new DirectoryNotFoundException();

			_dataDirectory = dataDirectory.TrimEnd('\\');

			Init();
		}

		#endregion Constructor

		#region Utilities

		private static void SetOptions(dynamic opts, string fileName, int kSize, int vSize)
		{
			opts.CalcBTreeOrder(kSize, vSize);
			opts.FileName = fileName;
			opts.CreateFile = CreatePolicy.IfNeeded;
			opts.StoragePerformance = StoragePerformance.Default;
			opts.FileBlockSize = 512;
			opts.StorageType = StorageType.Disk;

			opts.CacheKeepAliveTimeout = 10000;
			opts.CacheKeepAliveMinimumHistory = 0;
			opts.CacheKeepAliveMaximumHistory = 256;
			opts.CachePolicy = CachePolicy.Recent;

			opts.CallLevelLock = new ReaderWriterLocking();
			opts.LockingFactory = new LockFactory<SimpleReadWriteLocking>();
			opts.LockTimeout = 10000;
		}

		private void Init()
		{
			//var blockLookupOptions = new BPlusTree<SHA256Hash, HBlockOnDisk>.OptionsV2(
			//	new SHA256HashSerializer(), new HBlockOnDisk.Serializer(), SHA256Hash.Comparer
			//);

			//var offChainBlockOptions = new BPlusTree<SHA256Hash, BlockHeaderSummary>.OptionsV2(
				//new SHA256HashSerializer(), new HBlockSerializer(), new SHA256Hash.Comparer()
			//);

		//	SetOptions(offChainBlockOptions, _dataDirectory + "\\offchain.dat", 32, 256);

			//var inactiveBlockOptions = new BPlusTree<SHA256Hash, BlockHeader>.OptionsV2(
			//    new SHA256HashSerializer(), new HBlockHeaderSerializer(), new SHA256Hash.Comparer()
			//);

			//SetOptions(inactiveBlockOptions, _dataDirectory + "\\inactive.dat", 32, 4096);

			//var activeBlockOptions = new BPlusTree<SHA256Hash, Block>.OptionsV2(
			//    new SHA256HashSerializer(), new BlockSerializer(), new SHA256Hash.Comparer()
			//);

			//SetOptions(activeBlockOptions, _dataDirectory + "\\inactive.dat", 32, 1024 * 1024);

			//_offChainBlocks = new BPlusTree<SHA256Hash, BlockHeaderSummary>(offChainBlockOptions);
			//_inactiveBlocks = new BPlusTree<SHA256Hash,BlockHeader>(inactiveBlockOptions);
			//_activeBlocks = new BPlusTree<SHA256Hash, Block>(activeBlockOptions); 
			//_accountBalances = new BPlusTree<SHA256Hash,Balance>(accountBalanceOptions);

		//	_activeHeads = new List<SHA256Hash>();
		}

		#endregion Utilities

		#region Subscriptions

		/// <summary>
		/// Returns a set of all channels the client is subscribed to
		/// </summary>
		public IEnumerable<string> SubscribedToChannels { get; private set; }

		/// <summary>
		/// Unsubscribes from the given channel
		/// </summary>
		/// <param name="channel">The channel to unsubscribe from</param>
		/// <remarks>
		/// <para>Has no effect if the channel is not already subscribed to</para>
		/// <para>Implementations are free to immediately delete unnecessary data after this call</para>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel parameter is null
		/// </exception>
		public void UnsubscribeFrom(string channel)
		{
			if( channel == null )
				throw new ArgumentNullException("channel");

			throw new NotImplementedException();
		}

		/// <summary>
		/// Subscribes to the given channel
		/// </summary>
		/// <param name="channel">The channel to subscribe to</param>
		/// <remarks>Has no effect if the channel is already subscribed to</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel parameter is null
		/// </exception>
		public void SubscribeTo(string channel)
		{
			if( channel == null )
				throw new ArgumentNullException("channel");

			throw new NotImplementedException();
		}

		#endregion Subscriptions

		/// <summary>
		/// Gets the block with the given id
		/// </summary>
		/// <param name="id">The id of the block to retrieve</param>
		/// <param name="includeTentative">Whether tentative blocks are included in the search</param>
		/// <returns>The block with the given id</returns>
		/// <remarks>
		/// The fullest type of block stored is always returned. This method may return
		/// Blocks, BlockHeaders, or BlockHeaderSummaries
		/// </remarks>
		public BlockHeaderSummary GetBlock(SHA256Hash id) //, bool includeTentative)
		{
			var block = _blocks[id];

			if (block == null)
				return null;

			return block.ExtractBlock();
		}

		/// <summary>
		/// Gets the block with the given id and all ancestors back to the channel's genesis block
		/// </summary>
		/// <param name="id">The id of the block to retrieve</param>
		/// <param name="includeTentative">Whether tentative blocks are included in the search</param>
		/// <returns>The block with the given id and all its ancestors</returns>
		/// <remarks>
		/// The fullest type of block stored is always returned. This method may return
		/// Blocks, BlockHeaders, or BlockHeaderSummaries
		/// </remarks>
		public IEnumerable<BlockHeaderSummary> GetBlocksFrom(SHA256Hash id) //, bool includeTentative)
		{
			var block = _blocks[id];

			while (block != null)
			{
				yield return block.ExtractBlock();

				if (block.ParentBlockId == SHA256Hash.Factory.MinValue)
					yield break;

				block = _blocks[block.ParentBlockId];
			}
		}

		/// <summary>
		/// Gets the tail block of the highest proof-of-work branch on the given
		/// channel's chain
		/// </summary>
		/// <param name="channel">The channel of the chain</param>
		/// <param name="includeTentative">
		/// Whether tentative blocks are included in the proof-of-work calculation
		/// </param>
		/// <returns>
		/// The tail block of the highest proof-of-work branch on the given channel's chain
		/// </returns>
		/// <remarks>
		///		<para>
		///	The fullest type of block stored is always returned. This method may return
		/// Blocks, BlockHeaders, or BlockHeaderSummaries
		///		</para>
		///		<para>
		///	If includedTentative is set to false, the tail block will never be tentative
		///		</para>
		///		<para>
		///	Returns null if the channel is not subscribed to
		///		</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel argument is null
		/// </exception>
		public BlockHeader GetBestTail(string channel, bool includeTentative)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the tail block of the highest proof-of-work branch on the given
		/// channel's chain and all ancestors to the channel's genesis block
		/// </summary>
		/// <param name="channel">The channel of the chain</param>
		/// <param name="includeTentative">
		/// Whether tentative blocks are included in the proof-of-work calculation
		/// </param>
		/// <returns>
		/// The tail block of the highest proof-of-work branch on the given channel's chain
		/// and all its ancestors
		/// </returns>
		/// <remarks>
		///		<para>
		///	The fullest type of block stored is always returned. This method may return
		/// Blocks, BlockHeaders, or BlockHeaderSummaries
		///		</para>
		///		<para>
		///	If includedTentative is set to false, tentative blocks will never be returned
		///		</para>
		///		<para>
		///	Returns an empty enumeration if the channel is not subscribed to
		///		</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel argument is null
		/// </exception>
		public IEnumerable<BlockHeader> GetBlocksFromBestTail(string channel, bool includeTentative)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets all non-trimmed blocks, non-tentative blocks
		/// which have operations referencing any of the given channels,
		/// pulled from the longest respective branches
		/// </summary>
		/// <param name="channels">
		/// The channels which operations must reference
		/// </param>
		/// <returns>
		/// An lookup of matching blocks with duplicates removed grouped by block channel
		/// (not search channel)
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channels parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any channels are null
		/// </exception>
		public ILookup<string, BlockHeader> GetBlocksByChannelReferences(
			IEnumerable<string> channels
		)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the current balance of an account on the given channel
		/// </summary>
		/// <param name="channel">The channel to search for the account on</param>
		/// <param name="accountId">The account to search for</param>
		/// <returns>The most recent balance for the account</returns>
		/// <remarks>
		/// <para>Will return null if the channel is not subscribed to</para>
		/// <para>
		/// If the account doesn't exist on the channel,
		/// an empty balance (zero bias) is returned for it
		/// </para>
		/// <para>
		/// Implementations are not expected to reject accounts due to
		/// domain param/channel mismatches.
		/// </para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public Balance GetCurrentBalanceFor(string channel, SHA256Hash accountId)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Stores a block header summary
		/// </summary>
		/// <param name="block">The summary to store</param>
		/// <returns>
		/// True if the block summary is stored, false otherwise
		/// </returns>
		/// <remarks>
		/// Blocks header summaries will be rejected for the following reasons
		/// <para>A block with the given id already exists</para>
		/// <para>The block does not have a parent block which exists (if it's not the genesis block)</para>
		/// <para>The block summary builds off of a non-summary block</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the block parameter is null
		/// </exception>
		public bool StoreBlock(BlockHeaderSummary block)
		{
			throw new NotImplementedException();

			if (_blocks.ContainsKey(block.Id))
				return true;

			var buildFrom = _blocks[block.ParentBlockId];

			if (buildFrom == null)
				return false;

			if (buildFrom.Type != HBlockType.BlockHeaderSummary)
				return false;

			//if( 

			if (_blocks.ContainsKey(block.ParentBlockId))
				return false;


		}

		/// <summary>
		/// Stores a block header
		/// </summary>
		/// <param name="block">The header to store</param>
		/// <returns>
		/// True if the block header is stored, false otherwise
		/// </returns>
		/// <remarks>
		/// Block headers will be rejected for the following reasons
		/// <para>A block with the given id already exists</para>
		/// <para>The block does not have a parent block which exists (if it's not the genesis block)</para>
		/// <para>The block header builds off of a full block</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the block parameter is null
		/// </exception>
		public bool StoreBlock(BlockHeader block)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Stores a full block
		/// </summary>
		/// <param name="block">The full block to store</param>
		/// <param name="isTentative">
		/// Whether or not the block is tentative (not completely verified)
		/// </param>
		/// <returns>
		/// True if the block is stored, false otherwise
		/// </returns>
		/// <remarks>
		/// Blocks will be rejected for the following reasons
		/// <para>A block with the given id already exists</para>
		/// <para>The block does not have a parent block which exists (if it's not the genesis block)</para>
		/// <para>The block builds off of a non-full block</para>
		/// <para>
		/// The block is marked as not tentative but builds on a tentative
		/// block (use ClearTentativesFrom before adding)
		/// </para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the block parameter is null
		/// </exception>
		public bool StoreBlock(Block block, bool isTentative)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Marks the block with the given id and all of its ancestors as
		/// non-tentative (fully confirmed)
		/// </summary>
		/// <param name="id">The id of the starting (tail) block to make non-tentative</param>
		/// <returns>
		/// The number of blocks converted. Will be zero if the tail block is not tentative
		/// or the tail block is not found.
		/// </returns>
		public int ClearTentativesFrom(SHA256Hash id)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Trims (converts to a BlockHeaderSummary) the block with the given id and all of its ancestors
		/// </summary>
		/// <param name="id">The id of the starting (tail) block to trim</param>
		/// <returns>
		/// The number of blocks trimmed. Will be zero if the tail block is already a
		/// summary or the tail block is not found.
		/// </returns>
		public int TrimFrom(SHA256Hash id)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Trims (converts to a BlockHeaderSummary) the first block
		/// with a date before the given date and all of its ancestors
		/// </summary>
		/// <param name="channel">The channel to trim blocks on</param>
		/// <param name="maxDate">The date to trim blocks from</param>
		/// <returns>
		/// The number of blocks trimmed. Will be zero if the tail block is already a
		/// summary or no untrimmed blocks before that date are found.
		/// </returns>
		/// <remarks>
		/// All branches stemming from the trimmed block(s)
		/// (except for the one with the longest proof-of-work)
		/// are detatched and may be deleted. Tentative blocks are not
		/// counted in the proof-of-work.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the channel argument is null
		/// </exception>
		public int TrimFrom(string channel, DateTime maxDate) { throw new NotImplementedException(); }

		/// <summary>
		/// Gets the wallet associated with the given key
		/// </summary>
		/// <param name="key">The key of the wallet to return</param>
		/// <returns>The wallet associated under the given key, null otherwise</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key parameter is null
		/// </exception>
		public IWallet GetWallet(string key)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Adds or overwrites a wallet under the given key
		/// </summary>
		/// <param name="key">The key of store the wallet under</param>
		/// <param name="wallet">The wallet to store under the key</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public void SetWallet(string key, IWallet wallet)
		{
			throw new NotImplementedException();
		}

		public void Dispose()
		{
			throw new NotImplementedException();
		}


		IEnumerable<BlockHeader> IDB.GetBlocksFrom(SHA256Hash id)
		{
			throw new NotImplementedException();
		}
	}
}