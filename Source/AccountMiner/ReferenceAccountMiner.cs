﻿using System;
using System.Numerics;
using System.Security.Cryptography;
using System.Threading;

using CAWG.Hologram.Core.Mining;
using CAWG.Hologram.Core.User;
using JLabs.Cryptography.EC;

namespace AccountMiner
{
	public class ReferenceAccountMiner : IAccountMiner
	{
		#region Private Class

		private class MiningProperties
		{
			public BigInteger TargetWorth;
			public ECCurve Curve;
			public ECPoint StartPoint;
			public BigInteger? BasePrivateKey;
			public bool IsPaidAccount;
		}

		#endregion Private Class

		#region Fields

		private readonly static RNGCryptoServiceProvider _rnd;

		private Thread _miningThread;
		private bool _stopSignal;
		private MiningProperties _miningProperties;

		#endregion Fields

		#region Constructor

		static ReferenceAccountMiner()
		{
			_rnd = new RNGCryptoServiceProvider();
		}

		#endregion Constructor

		#region Start / Stop

		public bool Start(double targetWorth, ECCurveType accountType, bool isPaidAccount = false, RandomNumberGenerator rnd = null)
		{
			var biTarget = new BigInteger(targetWorth);

			return Start(biTarget, accountType, isPaidAccount, rnd);
		}

		public bool Start(BigInteger targetWorth, ECCurveType accountType, bool isPaidAccount = false, RandomNumberGenerator rnd = null)
		{
			if (targetWorth > Account.MaxWorth || targetWorth <= 0)
				throw new ArgumentOutOfRangeException("targetWorth", "Target worth must be in the 0 < x < Account.MaxWorth");

			if (IsRunning)
				return false;

			var curve = ECCurve.GetByCurveType(accountType);

			rnd = rnd ?? _rnd;

			var basePrivateKey = rnd.NextBigInteger(BigInteger.One, curve.N);

			_miningProperties = new MiningProperties
			{
				Curve = curve,
				StartPoint = curve.G * basePrivateKey,
				BasePrivateKey = basePrivateKey,
				TargetWorth = targetWorth,
				IsPaidAccount = isPaidAccount
			};

			Start();

			return true;
		}

		public bool StartFrom(double targetWorth, Account buildFrom, bool isPaidAccount = false)
		{
			var biTarget = new BigInteger(targetWorth);

			return StartFrom(biTarget, buildFrom, isPaidAccount);
		}

		public bool StartFrom(BigInteger targetWorth, Account buildFrom, bool isPaidAccount = false)
		{
			if (targetWorth > Account.MaxWorth || targetWorth <= 0)
				throw new ArgumentOutOfRangeException("targetWorth", "Target worth must be in the 0 < x < Account.MaxWorth");

			if (buildFrom == null)
				throw new ArgumentNullException("buildFrom");

			if (IsRunning)
				return false;

			var curve = ECCurve.GetByCurveType(buildFrom.AccountType);

			_miningProperties = new MiningProperties
			{
				Curve = curve,
				StartPoint = buildFrom.PublicKey + curve.G,
				TargetWorth = targetWorth,
				IsPaidAccount = isPaidAccount
			};

			Start();

			return true;
		}

		private void Start()
		{
			_miningThread = new Thread(Mine);

			_miningThread.Start();

			IsRunning = true;

			if (OnStart != null)
				OnStart(this, new EventArgs());
		}

		public void Stop()
		{
			if (!IsRunning || _stopSignal)
				return;

			_stopSignal = true;

			var stopThread = new Thread(WaitForStop);

			stopThread.Start();
		}

		private void WaitForStop()
		{
			while (_miningThread.ThreadState == ThreadState.Running)
			{
				Thread.Sleep(100);
			}

			// clear parameters
			_miningProperties = null;
			_miningThread = null;

			_stopSignal = false;
			IsRunning = false;
			TotalOperationsSinceStart = 0;
			OperationsPerSecond = 0;

			if (OnStop != null)
				OnStop(this, new EventArgs());
		}

		#endregion Start / Stop

		#region Mining

		private void Mine(object startParams)
		{
			var currentPoint = _miningProperties.StartPoint;

			var startedAt = DateTime.Now;

			while (! _stopSignal)
			{
				if (Account.ComputeWorth(currentPoint) >= _miningProperties.TargetWorth)
				{
					if (OnAccountFound != null)
					{
						AccountFoundEventArgs ea;

						if (_miningProperties.BasePrivateKey.HasValue)
						{
							ea = new AccountFoundEventArgs(
								_miningProperties.StartPoint,
								_miningProperties.BasePrivateKey,
								TotalOperationsSinceStart,
								new Account(_miningProperties.Curve.Type, _miningProperties.BasePrivateKey.Value + TotalOperationsSinceStart)
							);
						}
						else
						{
							ea = new AccountFoundEventArgs(
								_miningProperties.StartPoint,
								null,
								TotalOperationsSinceStart,
								new Account(_miningProperties.Curve.Type, currentPoint)
							);
						}

						OnAccountFound(this, ea);
					}
				}

				currentPoint += _miningProperties.Curve.G;
				TotalOperationsSinceStart++;

				if (TotalOperationsSinceStart % 1000 == 0)
				{
					var totalSeconds = DateTime.Now.Subtract(startedAt).TotalSeconds;

					OperationsPerSecond = TotalOperationsSinceStart / totalSeconds;
				}
			}
		}

		#endregion Mining

		#region Properties

		public bool IsRunning { get; private set; }
		public double OperationsPerSecond { get; private set; }
		public ulong TotalOperationsSinceStart { get; private set; }

		#endregion Properties

		#region Events

		public event EventHandler OnStart;
		public event EventHandler OnStop;
		public event EventHandler<AccountFoundEventArgs> OnAccountFound;

		#endregion Events
	}
}