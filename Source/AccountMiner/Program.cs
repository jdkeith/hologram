﻿using System;
using System.IO;
using System.Numerics;
using System.Threading;
using CAWG.Hologram.Core.Mining;
using JLabs.Cryptography.EC;
using JLabs.Structures;
using JLabs.Structures.Cryptography;

namespace AccountMiner
{
	class Program
	{
		static IAccountMiner _miner;
		static BigInteger _totalDifficulty;
		static uint _totalAccounts;
		static string _outputPath;

		static void Main()
		{
			Console.WriteLine("HOLOGRAM REFERENCE CPU ACCOUNT MINER\n\n");

			while (true)
			{
				Console.Write("Write holograms to path> ");
				_outputPath = Console.ReadLine();

				if (!Directory.Exists(_outputPath))
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.Write("Path {0} doesn't exist", _outputPath);
					Console.ForegroundColor = ConsoleColor.Gray;
					Console.WriteLine();
					continue;
				}

				break;
			}

			double targetDifficulty;

			while (true)
			{
				Console.Write("Target difficulty 2**x (0<=x<256)> ");

				if (
					!double.TryParse(Console.ReadLine(), out targetDifficulty) ||
					targetDifficulty < 0 || targetDifficulty >= 256
				)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.Write("Invalid number");
					Console.ForegroundColor = ConsoleColor.Gray;
					Console.WriteLine();
					continue;
				}

				targetDifficulty = Math.Pow(2, targetDifficulty);

				break;
			}

			ECCurveType accountType;

			while (true)
			{
				var types = Enum.GetValues(typeof(ECCurveType));

				var i = 1;

				foreach (var type in types)
				{
					Console.WriteLine("For {0} enter {1}", type, (byte) type);
					i++;
				}

				Console.Write("Account Type> ");

				byte at;

				if (
					!byte.TryParse(Console.ReadLine(), out at) ||
					!Enum.IsDefined(typeof(ECCurveType), at)
				)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.Write("Invalid account type");
					Console.ForegroundColor = ConsoleColor.Gray;
					Console.WriteLine();
					continue;
				}

				accountType = (ECCurveType)at;

				break;
			}

			_miner = new ReferenceAccountMiner();

			_miner.OnStart += OnMinerStart;
			_miner.OnStop += OnMinerStop;
			_miner.OnAccountFound += OnAccountFound;

			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Press enter twice to stop the miner");
			Console.ForegroundColor = ConsoleColor.Gray;

			_miner.Start(targetDifficulty, accountType);

			Console.ReadLine();
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Press enter one more time to quit");
			Console.ForegroundColor = ConsoleColor.Gray;

			Console.ReadLine();

			_miner.Stop();

			while (_miner.IsRunning)
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				Console.WriteLine("The miner is shutting down, please wait...");
				Thread.Sleep(2000);
			}
		}

		static void OnMinerStart(object sender, EventArgs e)
		{
			Console.WriteLine("Miner started.");
		}

		static void OnMinerStop(object sender, EventArgs e)
		{
			Console.WriteLine("Miner stopped.");
		}

		static void OnAccountFound(object sender, AccountFoundEventArgs e)
		{
			_totalAccounts++;
			_totalDifficulty += e.FoundAccount.Worth;

			Console.WriteLine("Account found");
			Console.WriteLine("\tDifficulty: {0}", e.FoundAccount.Worth);
			Console.WriteLine("\tTotal accounts found: {0}", _totalAccounts);
			Console.WriteLine("\tTotal difficulty: {0}", _totalDifficulty);
			Console.WriteLine("\tOperations per second: {0}", _miner.OperationsPerSecond);
			Console.WriteLine();

			var fi = new FileInfo(_outputPath + "\\" + SHA256Hash.Factory.Of(e.FoundAccount) + ".holo");

			using( var fs = fi.OpenWrite() )
			{
				e.FoundAccount.WriteTo(fs, true);
			}
		}
	}
}
