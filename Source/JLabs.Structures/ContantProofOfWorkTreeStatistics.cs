﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace JLabs.Structures
{
	public class ConstantProofOfWorkTreeStatistics
	{
		#region Fields

		private readonly int _itemsCount;
		private readonly int _depth;
		private readonly int _constrainedItemsCount;
		private readonly int[] _indicies;
		private readonly BigInteger[] _difficulties;

		private Tuple<BigInteger, BigInteger> _cachedBestFitLineSlopeIntercept;
		private BigInteger? _cachedRoughFitDifficulty;
		private BigInteger? _cachedHashesToFake;
		private double? _cachedConfidence;

		#endregion Fields

		#region Constructor

		public ConstantProofOfWorkTreeStatistics(
			int itemsCount, int depth,
			IEnumerable<int> indicies, IEnumerable<BigInteger> difficulties
		)
		{
			if (indicies == null)
				throw new ArgumentNullException("indicies");

			if (difficulties == null)
				throw new ArgumentNullException("difficulties");

			_itemsCount = itemsCount;
			_depth = depth;
			_indicies = indicies.ToArray();
			_difficulties = difficulties.ToArray();

			if (_indicies.Length != _difficulties.Length)
				throw new ArgumentException("Indicies and difficulties parameters must be the same length", "indicies");

			if (_indicies.Any(i => i < 0))
				throw new ArgumentOutOfRangeException("indicies", "Indicies cannot contain negative numbers");

			if(_indicies.Any( i => i > itemsCount ) )
				throw new ArgumentOutOfRangeException("indicies", "Indicies cannot contain items greater than the items count");

			if (_indicies.Distinct().Count() != _indicies.Length)
				throw new ArgumentException("Indicies cannot contain duplicate items", "indicies");

			for (var i = 0; i < _difficulties.Length - 1; i++)
			{
				if (_difficulties[i].CompareTo(_difficulties[i + 1]) < 0)
					throw new ArgumentException("Difficulties must be sorted in descending order", "difficulties");
			}

			_constrainedItemsCount = Math.Min(_itemsCount, (int) Math.Pow(2, _depth));
		}

		#endregion Properties

		#region Computation

		protected virtual Tuple<BigInteger, BigInteger> ComputeBestFitLineSlopeIntercept()
		{
			throw new NotImplementedException();
		}

		protected virtual BigInteger ComputeRoughSlopeDifficulty()
		{
			if (_difficulties.Length == 1)
				return _difficulties.First() * _constrainedItemsCount;

			var runningTotal = BigInteger.Zero;

			for (var i = 0; i < _indicies.Length - 1; i++)
			{
				var pointA = new {y = _difficulties[i], x = _indicies[i]};
				var pointB = new {y = _difficulties[i + 1], x = _indicies[i + 1]};

				// rise over run
				var slope = (pointA.y - pointB.y) / (pointA.x - pointB.x);

				// b (in y = mx+b)
				var cxc = pointB.y - slope * pointB.x;

				// integral slope
				var dxc = (slope / 2);

				// integral of point a
				var start = dxc * BigInteger.Pow(pointA.x, 2) + cxc * pointA.x;

				// integral of point b;
				var end = dxc * BigInteger.Pow(pointB.x, 2) + cxc * pointB.x;

				// add area under the line
				runningTotal += end - start;
			}

			// add in the last piece
			runningTotal += (_constrainedItemsCount - _indicies.Last())*_difficulties.Last();

			return runningTotal;
		}

		protected virtual double ComputeConfidence()
		{
			throw new NotImplementedException();
		}

		protected virtual BigInteger ComputeHashesToFake()
		{
			return BigInteger.Pow(_constrainedItemsCount, _difficulties.Length);
		}

		#endregion Computation

		#region Properties

		public Tuple<BigInteger, BigInteger> BestFitLineSlopeIntercept
		{
			get
			{
				if (_cachedBestFitLineSlopeIntercept == null)
				{
					_cachedBestFitLineSlopeIntercept = ComputeBestFitLineSlopeIntercept();
				}

				return _cachedBestFitLineSlopeIntercept;
			}
		}

		public BigInteger BestFitDifficulty
		{
			get
			{
				var constraintedItemsCount = Math.Max(_itemsCount, (int) Math.Pow(2, _depth));

				var end = (BestFitLineSlopeIntercept.Item1/2)*(int) Math.Pow(constraintedItemsCount, 2) +
				          BestFitLineSlopeIntercept.Item2 * constraintedItemsCount;

				// start is always 0

				return end;
			}
		}

		public BigInteger RoughSlopeDifficulty
		{
			get
			{
				if (! _cachedRoughFitDifficulty.HasValue)
				{
					_cachedRoughFitDifficulty = ComputeRoughSlopeDifficulty();
				}

				return _cachedRoughFitDifficulty.Value;
			}
		}

		public virtual double Confidence
		{
			get
			{
				if (! _cachedConfidence.HasValue)
				{
					_cachedConfidence = ComputeConfidence();
				}

				return _cachedConfidence.Value;
			}
		}

		public virtual BigInteger MinimumHashesToFake
		{
			get
			{
				if (!_cachedHashesToFake.HasValue)
				{
					_cachedHashesToFake = ComputeHashesToFake();
				}

				return _cachedHashesToFake.Value;
			}
		}

		#endregion Properties
	}
}