﻿/*
 * 
 * JDK note: I'm not sure this can work
 * 
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using Native=System.Runtime.Serialization;
using System.Text;
using System.Runtime.Serialization;

namespace JLabs.Structures
{
	public class ISHA256HashedProperties
	{
		public IEnumerable<object> Values { get; private set; }
		public SHA256Hash Remainder { get; private set; }
		public SHA256Hash Result { get; private set; }
	}

	public class SHA256HashedPropertyBag
	{
		// todo - move this to core type
		public static byte[] SerializeObject(object value)
		{
			if (value == null)
				throw new ArgumentNullException("value");

			var serializable = value as ISerializable;

			if (serializable != null)
				return serializable.ToByteArray();

			var t = value.GetType();

			if (! t.IsPrimitive || ! typeof (Native.ISerializable).IsAssignableFrom(t))
				throw new ArgumentException("Value must be primitive or serializable", "value")
				
			var formatter = new Native.Formatters.Binary.BinaryFormatter();

			using (var ms = new MemoryStream())
			{
				formatter.Serialize(ms, value);
				return ms.ToArray();
			}
		}

		public SHA256HashedPropertyBag(SHA256Hash basis)
		{
			Basis = basis;
			Bias = SHA256Hash.MinValue;
		}

		public void Add(object value)
		{
			var bytes = SHA256Hash.Of(SerializeObject(value)).ToByteArray();
			var biasBaseBytes = Bias.ToByteArray();

			biasBaseBytes = biasBaseBytes
				.Zip(bytes, (a, b) => (byte) (a ^ b))
				.ToArray();

			Bias = SHA256Hash.FromByteArray(biasBaseBytes);
		}

		public ISHA256HashedProperties ExtractValues(params object[] values)
		{
			
		}

		public SHA256Hash Basis { get; private set; }
		public SHA256Hash Bias { get; private set; }

		public SHA256Hash Result
		{
			get
			{
				return Basis + Bias;
			}
		}
	}
}
*/