﻿using System.Collections;
using System.Linq;

using JLabs.Structures.Cryptography;

// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming
namespace System.Numerics
{
	/// <summary>
	/// Provides convenience and utility methods for or related to big integers
	/// </summary>
	public static class BigIntegerExtensions
	{
		/// <summary>
		/// Converts a BigInteger to a fixed-sized byte array
		/// </summary>
		/// <param name="value">The value to convert</param>
		/// <param name="arrayLength">The length of the result array</param>
		/// <param name="reverse">Whether or not the array should be reversed</param>
		/// <param name="trimLeadingZeros">Whether or not to trim leading zeroes before storing</param>
		/// <returns>An array representation of the value</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the arrayLength parameter is not a positive number
		/// </exception>
		/// <exception cref="System.ArithmeticException">
		/// Thrown if the arrayLength parameter is too small to contain the given value
		/// </exception>
		public static byte[] ToFixedSizeByteArray(this BigInteger value, int arrayLength, bool reverse = false, bool trimLeadingZeros = false)
		{
			if (arrayLength <= 0)
				throw new ArgumentOutOfRangeException("arrayLength", "Array length must be a positive number");

			var unexpandedArray = value.ToByteArray();

			if (trimLeadingZeros)
			{
				Array.Reverse(unexpandedArray);
				
				unexpandedArray = unexpandedArray.SkipWhile( v => v == 0 )
					.Reverse()
					.ToArray();
			}

			if (reverse)
				Array.Reverse(unexpandedArray);

			var result = new byte[arrayLength];

			if( unexpandedArray.Length > arrayLength )
				throw new ArithmeticException("Cannot store value in the specified array size");

			Array.Copy(unexpandedArray, 0, result, arrayLength - unexpandedArray.Length, unexpandedArray.Length);

			return result;
		}

		/// <summary>
		/// Converts a byte array to a non-negative big integer
		/// </summary>
		/// <param name="buffer">The byte array to convert</param>
		/// <param name="reverse">Whether or not the buffer will be reversed</param>
		/// <returns>A non-negative big integer derived from the given byte array</returns>
		/// <remarks>
		/// Big integers are negative if their highest order bit is set. There is no
		/// constructor which allows the specification of a sign while passing an
		/// unsigned byte array. This method works around that limitation.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the buffer is null
		/// </exception>
		public static BigInteger ToNonNegativeBigInteger(this byte[] buffer, bool reverse = false)
		{
			if (buffer == null)
				throw new ArgumentNullException("buffer");

			var copyBuffer = (byte[])buffer.Clone();

			if (reverse)
				Array.Reverse(copyBuffer);

			var positiveBuffer = new byte[copyBuffer.Length + 1];
			Array.Copy(copyBuffer, positiveBuffer, copyBuffer.Length);

			return new BigInteger(positiveBuffer);
		}

		/// <summary>
		/// Converts a SHA256Hash to a non-negative big integer
		/// </summary>
		/// <param name="hash">The hash to convert</param>
		/// <returns>A non-negative BigInteger representing the hash</returns>
		public static BigInteger ToNonNegativeBigInteger(this SHA256Hash hash)
		{
			return hash.ToByteArray().ToNonNegativeBigInteger(true);
		}

		/// <summary>
		/// Converts a RIPEMD160Hash to a non-negative big integer
		/// </summary>
		/// <param name="hash">The hash to convert</param>
		/// <returns>A non-negative BigInteger representing the hash</returns>
		public static BigInteger ToNonNegativeBigInteger(this RIPEMD160Hash hash)
		{
			return hash.ToByteArray().ToNonNegativeBigInteger(true);
		}

		public static BitArray ToBitArray(this BigInteger value)
		{
			return new BitArray(value.ToByteArray());
		}

		/// <summary>
		/// Gets the number of bits required to represent the number
		/// </summary>
		/// <param name="value">The value to determine the bit length of</param>
		/// <returns>The number of bits required to represent the number</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the number is null
		/// </exception>
		/// <remarks>Negative values may currently return erroneous values</remarks>
		public static int GetBitLength(this BigInteger value)
		{
			if (value == null)
				throw new ArgumentNullException("value");

			var bitArray = value.ToBitArray();

			var result = 0;

			for (var i = 0; i < bitArray.Length; i++)
			{
				if (bitArray[i])
					result = i + 1;
			}

			return result;
		}

		/// <summary>
		/// Determines whether or not the bit at a certain index within a big integer is set
		/// </summary>
		/// <param name="value">The big integer to test</param>
		/// <param name="bitIndex">The index of the bit to test</param>
		/// <returns>
		/// True if the bit at that index is set, false if it is not
		/// or if the given index is negative or greater than or equal
		/// to the bit length of the number
		/// </returns>
		/// <remarks>
		/// Bits are in little-endian format, that is index 0 represents
		/// the bit for value 2**0
		/// </remarks>
		public static bool TestBit(this BigInteger value, int bitIndex)
		{
			var bitArray = value.ToBitArray();

			try
			{
				return bitArray[bitIndex];
			}
			catch(ArgumentOutOfRangeException)
			{
				return false;
			}
		}

		/// <summary>
		/// Calculates the modular multiplicative inverse of a big integer
		/// </summary>
		/// <param name="value">The value to compute the inverse of</param>
		/// <param name="modulus">The domain to find the inverse within</param>
		/// <returns>The modular multiplicative inverse of the given value</returns>
		/// <see cref="http://en.wikipedia.org/wiki/Modular_multiplicative_inverse"/>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the value is less than or equal to zero
		/// </exception>
		/// <exception cref="System.ArithmeticException">
		/// Thrown if the value and modulus are not coprime
		/// </exception>
		public static BigInteger ModInverse(this BigInteger value, BigInteger modulus)
		{
			if (modulus <= BigInteger.Zero)
				throw new ArgumentOutOfRangeException("modulus", "Modulus must be positive");

			var dividend = value % modulus;
			var divisor = modulus;

			var lastX = BigInteger.One;
			var currentX = BigInteger.Zero;

			while (divisor.Sign > 0)
			{
				var quotient = dividend / divisor;
				var remainder = dividend % divisor;

				if (remainder.Sign <= 0)
					break;

				// In the algorithm ax + by = gcd(a, b) we only keep track of the
				// value currentX and the lastX from last iteration,
				// the y value is ignored anyway. After the remainder
				// runs to zero, we get our inverse from currentX
				var nextX = lastX - currentX * quotient;
				lastX = currentX;
				currentX = nextX;

				dividend = divisor;
				divisor = remainder;
			}

			if (divisor != BigInteger.One)
				throw new ArithmeticException("Value and modulus are not coprime");

			return currentX.Sign < 0 ? currentX + modulus : currentX;
		}
	}
}