﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JLabs.Structures.Utilities
{
    public interface IContinuation<T>
    {
        IEnumerable<T> CurrentSet { get; }
        IEnumerable<T> Remainder { get; }
        bool HasRemainder { get; }
        int SetSize { get; }
    }

    public static class LinqExtensions
    {
        private class Continuation<T> : IContinuation<T>
        {
            private readonly IEnumerator<T> _enumerator;
            private T _toggleItem;
            private bool _hasToggleItem;

            public Continuation(IEnumerable<T> items, int setSize)
            {
                _enumerator = items.GetEnumerator();
                SetSize = setSize;
            }

            public bool IsRemainderReady { get; private set; }

            public IEnumerable<T> CurrentSet
            {
                get
                {
                    if (IsRemainderReady)
                        throw new InvalidOperationException("CurrentSet may only be read once");

                    for (var i = 0; i < SetSize; i++)
                    {
                        if (! _enumerator.MoveNext())
                        {
                            _hasToggleItem = false;
                            IsRemainderReady = true;
                            yield break;
                        }

                        yield return _enumerator.Current;
                    }

                    if (_enumerator.MoveNext())
                    {
                        _toggleItem = _enumerator.Current;
                        _hasToggleItem = true;
                    }

                    IsRemainderReady = true;
                }
            }

            public IEnumerable<T> Remainder
            {
                get
                {
                    if (!IsRemainderReady)
                        throw new InvalidOperationException("CurrentSet must be completely read before remainder is read");

                    if (! _hasToggleItem)
                        yield break;

                    yield return _toggleItem;

                    while (_enumerator.MoveNext())
                        yield return _enumerator.Current;
                }
            }

            public bool HasRemainder
            {
                get
                {
                    if( ! IsRemainderReady )
                        throw new InvalidOperationException("CurrentSet must be completely read before remainder is read");

                    return _hasToggleItem;
                }
            }

            public int SetSize { get; private set; }
        }

        public static IContinuation<T> Page<T>(this IEnumerable<T> source, int setSize)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (setSize < 1)
                throw new ArgumentOutOfRangeException("setSize", "Set size must be positive");

            return new Continuation<T>(source, setSize);
        }

        public static IEnumerable<T> ShuffleCombine<T>(
            this IEnumerable<T> source, Func<T, T, bool, T> combiner, bool truncateIfOdd = true
        )
        {
            if( source == null )
                throw new ArgumentNullException("source");

            var enumerator = source.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var item1 = enumerator.Current;
                T item2;
                var isOdd = false;

                if (enumerator.MoveNext())
                {
                    item2 = enumerator.Current;
                }
                else if (truncateIfOdd)
                {
                    yield break;
                }
                else
                {
                    item2 = default(T);
                    isOdd = true;
                }

                yield return combiner(item1, item2, isOdd);
            }
        }

		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
	    {
			ForEach(source, (v, i, f, l) => { action(v); return false; });
	    }

		public static void ForEach<T>(this IEnumerable<T> source, Func<T, bool> action)
		{
			ForEach(source, (v, i, f, l) => action(v));
		}

		public static void ForEach<T>(this IEnumerable<T> source, Action<T, int> action)
		{
			ForEach(source, (v, i, f, l) => { action(v, i); return false; });
		}

		public static void ForEach<T>(this IEnumerable<T> source, Func<T, int, bool> action)
		{
			ForEach(source, (v, i, f, l) => action(v, i));
		}

		public static void ForEach<T>(this IEnumerable<T> source, Action<T, int, bool, bool> action)
		{
			ForEach(source, (v,i,f,l) => { action(v, i, f, l); return false; });
		}

		public static void ForEach<T>(this IEnumerable<T> source, Func<T, int, bool, bool, bool> action)
		{
			if( source == null )
				throw new ArgumentNullException("source");

			if( action == null )
				throw new ArgumentNullException("action");

			var isFirst = true;

			var enumerator = source.GetEnumerator();

			if (!enumerator.MoveNext())
				return;

			var current = enumerator.Current;

			var index = 0;

			while (true)
			{
				var isLast = !enumerator.MoveNext();

				var @break = action(current, index, isFirst, isLast);

				if (@break)
					return;

				isFirst = false;

				if (isLast)
					return;

				current = enumerator.Current;

				index++;
			}
		}
    }
}