﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable CheckNamespace
namespace System.Numerics
{
	public static class BigIntegerLinqExtensions
	{
		public static BigInteger Sum(this IEnumerable<BigInteger> values)
		{
			if( values == null )
				throw new ArgumentNullException("values");

			return values.Aggregate(BigInteger.Zero, (current, value) => current + value);
		}

		public static BigInteger Min(this IEnumerable<BigInteger> values)
		{
			var valueSeen = false;
			var result = BigInteger.Zero;

			foreach (var value in values)
			{
				if (! valueSeen || value < result)
					result = value;

				valueSeen = true;
			}

			if( ! valueSeen )
				throw new InvalidOperationException("Sequence contains no elements");

			return result;
		}

		public static BigInteger Max(this IEnumerable<BigInteger> values)
		{
			var valueSeen = false;
			var result = BigInteger.Zero;

			foreach (var value in values)
			{
				if (!valueSeen || value > result)
					result = value;

				valueSeen = true;
			}

			if (!valueSeen)
				throw new InvalidOperationException("Sequence contains no elements");

			return result;
		}

		public static BigInteger Average(this IEnumerable<BigInteger> values)
		{
			var itemsCount = 0;
			var result = BigInteger.Zero;

			foreach (var value in values)
			{
				result += value;
				itemsCount++;
			}

			if (itemsCount == 0)
				throw new InvalidOperationException("Sequence contains no elements");

			return result/itemsCount;
		}
	}
}