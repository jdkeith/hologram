﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JLabs.Structures.Utilities
{
	public static class ByteArrayExtensions
	{
		public static string ToHexString(this IEnumerable<byte> data)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			var sb = new StringBuilder();

			foreach (var b in data)
			{
				sb.Append(b.ToString("x2"));
			}

			return sb.ToString();
		}

		public static byte[] FromHexString(this string hex)
		{
			if (hex == null)
				throw new ArgumentNullException("hex");

			if (hex.Length%2 != 0)
				throw new ArgumentException("Hex string must contain an even number of characters", "hex");

			hex = hex.ToLower();

			var result = new byte[hex.Length / 2];

			for (int i = 0; i < result.Length; i++)
			{
				result[i] = (byte)((GetHexVal(hex[i * 2]) << 4) + GetHexVal(hex[i * 2 + 1]));
			}

			return result;
		}

		public static int GetHexVal(char hex)
		{
			var val = (int)hex;
			val = val - (val < 58 ? 48 : 87);

			if (val < 0 || val > 15)
				throw new FormatException("Hex string may only contain the values 0-9, A-F");

			return val;
		}

		public static int CompareByteArrays(this byte[] a, byte[] b)
		{
			if (ReferenceEquals(a, null))
				return ReferenceEquals(b, null) ? 0 : 1;

			if (ReferenceEquals(b, null))
				return -1;

			var length = Math.Min(a.Length, b.Length);

			for (var i = 0; i < length; i++)
			{
				var cmp = a[i].CompareTo(b[i]);

				if (cmp != 0) return cmp;
			}

			// the shorter array is first
			return a.Length.CompareTo(b.Length);
		}
	}
}