﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using JLabs.Structures.Cryptography;
using JLabs.Structures.Units;

namespace JLabs.Structures
{
	/// <summary>
	/// A space-efficient probabilistic data structure used
	/// to test whether an element is a member of a set.
	/// </summary>
	public class BloomFilter : ISerializable
	{
		#region Factory

		private class BloomFilterFactory : IDeserializer<BloomFilter>
		{
			private readonly IHasher _hasher;

			public BloomFilterFactory(IHasher hasher)
			{
				_hasher = hasher;
			}

			public BloomFilter Deserialize(byte[] serialized)
			{
				if (serialized == null)
					throw new ArgumentNullException("serialized");

				using (var ms = new MemoryStream())
				{
					ms.Write(serialized, 0, serialized.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public BloomFilter ReadFrom(Stream s)
			{
				if (s == null)
					throw new ArgumentNullException("s");

				var slotsCount = (int) VarUInt.Factory.ReadFrom(s);
				var slotsPerKey = (int) VarUInt.Factory.ReadFrom(s);

				var bytes = new byte[(int) Math.Ceiling((double) slotsCount/8)];

				s.Read(bytes, 0, bytes.Length);

				var bits = new BitArray(bytes);

				return new BloomFilter(slotsCount, slotsPerKey, _hasher, bits);
			}
		}

		public static IDeserializer<BloomFilter> Factory(IHasher hasher = null)
		{
			return new BloomFilterFactory(hasher);
		}

		#endregion Factory

		#region Fields

		private readonly IHasher _hasher;
		private readonly BitArray _bits;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates a new bloom filter with the given parameters
		/// </summary>
		/// <param name="slotsCount">
		/// The number of bits used to represent the bloom filter. The higher this number,
		/// the less likely items are to conflict.
		/// </param>
		/// <param name="slotsPerItem">The number of slots each item takes up</param>
		/// <param name="hasher">
		/// The hash implementation to use to map items to slots. If not specified,
		/// a SHA256Hash implementation is used.
		/// </param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if
		/// <para>slotsCount is not a positive number</para>
		/// <para>slotsPerItem is not a positive number</para>
		/// <para>slotsPerKey is greater than slotsCount</para>
		/// </exception>
		public BloomFilter(int slotsCount, int slotsPerItem, IHasher hasher = null)
		{
			_hasher = hasher ?? SHA256Hash.Factory;

			if (slotsCount <= 0)
				throw new ArgumentOutOfRangeException("slotsCount", "SlotsCount must be a positive number");

			if (slotsPerItem <= 0)
				throw new ArgumentOutOfRangeException("slotsPerItem", "SlotsPerKey per key must be a positive number");

			if (slotsPerItem > slotsCount)
				throw new ArgumentOutOfRangeException("slotsPerItem", "SlotsPerKey per key must be less than or equal to SlotsCount");

			SlotsCount = slotsCount;
			SlotsPerItem = slotsPerItem;

			_bits = new BitArray(slotsCount);
		}

		private BloomFilter(int slotsCount, int slotsPerItem, IHasher hasher, BitArray bits)
			: this(slotsCount, slotsPerItem, hasher)
		{
			_bits = bits;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// The number of bits used to represent the bloom filter. The higher this number,
		/// the less likely items are to conflict.
		/// </summary>
		public int SlotsCount { get; private set; }

		/// <summary>
		/// The number of slots each item takes up
		/// </summary>
		public int SlotsPerItem { get; private set; }

		#endregion Properties

		#region Public Methods

		/// <summary>
		/// Adds a summary of the given data to the bloom filter
		/// </summary>
		/// <param name="data">The data to summarize and add to the bloom filter</param>
		public void Add(byte[] data)
		{
			if( data == null )
				throw new ArgumentNullException("data");

			Add(_hasher.Of(data));
		}

		/// <summary>
		/// Adds the summary of an item to the bloom filter
		/// </summary>
		/// <param name="item">The item to add to the bloom filter</param>
		public void Add(ISerializable item)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			var hashBytes = item.GetType() == _hasher.HashType
				? item.ToByteArray()
				: _hasher.Of(item).ToByteArray();

			foreach (var slot in GetUniqueSlots(hashBytes))
			{
				_bits.Set(slot, true);
			}
		}

		/// <summary>
		/// Tests whether the given data is represented in the bloom filter
		/// </summary>
		/// <param name="data">The data to check for</param>
		/// <returns>
		/// True if the data *might* be in the bloom filter, false if
		/// it is *definitely not* in the bloom filter.
		/// </returns>
		/// <remarks>
		/// The likelihood of a false positive is dependent on how may items
		/// are in the bloom filter, the slotsCount parameter, the slotsPerItem parameter,
		/// and the hash implementation used
		/// </remarks>
		public bool Test(byte[] data)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			return Test(_hasher.Of(data));
		}

		/// <summary>
		/// Tests whether the given item is represented in the bloom filter
		/// </summary>
		/// <param name="item">The item to check for</param>
		/// <returns>
		/// True if the item *might* be in the bloom filter, false if
		/// it is *definitely not* in the bloom filter.
		/// </returns>
		/// <remarks>
		/// The likelihood of a false positive is dependent on how may items
		/// are in the bloom filter, the slotsCount parameter, the slotsPerItem parameter,
		/// and the hash implementation used
		/// </remarks>
		public bool Test(ISerializable item)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			var hashBytes = item.GetType() == _hasher.HashType
				? item.ToByteArray()
				: _hasher.Of(item).ToByteArray();

			return GetUniqueSlots(hashBytes)
				.All(i => _bits.Get(i));
		}

		#endregion Public Methods

		#region Utilities

		private IEnumerable<int> GetUniqueSlots(byte[] hashBytes)
		{
			var result = new List<int>();

			var current = SlotsCount + 1;
			var bias = 0;

			while (true)
			{
				current = GetSlot(hashBytes, current + bias);

				if (!result.Contains(current))
				{
					result.Add(current);
					bias = 0;
				}
				else
				{
					bias++;
					continue;
				}

				if (result.Count >= SlotsPerItem)
					break;
			}

			return result;
		}

		/// <summary>
		/// Converts a set of bytes and a slot index to a new slot index constrained by the slotsCount parameter
		/// </summary>
		/// <param name="hashBytes">The bytes to get a slot index for</param>
		/// <param name="previousSlotIndex">
		/// The previously returned slot index, used as a nonce to prevent a halting state where
		/// the output index is always the last index
		/// </param>
		/// <returns>An deterministic index in the range of 0 to slotsCount - 1 inclusive</returns>
		protected virtual int GetSlot(IEnumerable<byte> hashBytes, int previousSlotIndex)
		{
			if( hashBytes == null )
				throw new ArgumentNullException("hashBytes");

			if( previousSlotIndex < 0 )
				throw new ArgumentOutOfRangeException("previousSlotIndex", "PreviousSlotIndex cannot be a negative number");

			var newHash = _hasher.Of(hashBytes.Concat(BitConverter.GetBytes(previousSlotIndex)).ToArray());

			var bytes = newHash.ToByteArray();

			var result = BitConverter.ToInt32(bytes, 0);

			return Math.Abs(result) % SlotsCount;
		}

		#endregion Utilities

		#region Serialization

		/// <summary>
		/// Serializes the given item to a byte array
		/// </summary>
		/// <returns>The serialized representation of the item</returns>
		public byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write the item to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			((VarUInt)(ulong)SlotsCount).WriteTo(s);
			((VarUInt)(ulong)SlotsPerItem).WriteTo(s);

			var bytes = new byte[(int)Math.Ceiling((double)SlotsCount / 8)];

			_bits.CopyTo(bytes, 0);

			s.Write(bytes, 0, bytes.Length);
		}

		#endregion Serialization
	}
}