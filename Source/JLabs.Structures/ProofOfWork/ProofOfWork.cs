﻿using System;
using System.IO;
using System.Numerics;

using JLabs.Structures.Cryptography;

namespace JLabs.Structures.ProofOfWork
{
	public class HashProofOfWorkFactory<TH> : IDeserializer<IHashProofOfWork<TH>> where TH : IHash
	{
		#region Inner Class

		private class HashProofOfWork<TH> : IHashProofOfWork<TH> where TH : IHash
		{
			private const int BitsPerByte = 8;

			public HashProofOfWork(IHasher<TH> hasher, TH basis, TH nonce)
			{
				Basis = basis;
				Nonce = nonce;
				Result = hasher.Combine(basis, nonce);
				MaxDifficulty = BigInteger.Pow(2, hasher.HashSize * BitsPerByte);
				Difficulty = MaxDifficulty / Result.ToByteArray().ToNonNegativeBigInteger(true);
			}

			public byte[] ToByteArray()
			{
				using (var ms = new MemoryStream())
				{
					WriteTo(ms);
					return ms.ToArray();
				}
			}

			public void WriteTo(Stream s)
			{
				if (s == null)
					throw new ArgumentNullException("s");

				Basis.WriteTo(s);
				Nonce.WriteTo(s);
			}

			public int CompareTo(IProofOfWork other)
			{
				if (ReferenceEquals(other, null))
					return -1;

				return Difficulty.CompareTo(other.Difficulty);
			}

			public BigInteger MaxDifficulty { get; private set; }
			public BigInteger Difficulty { get; private set; }

			public TH Basis { get; private set; }
			public TH Nonce { get; private set; }
			public TH Result { get; private set; }

			IHash IHashProofOfWork.Basis
			{
				get { return Basis; }
			}

			IHash IHashProofOfWork.Nonce
			{
				get { return Nonce; }
			}

			IHash IHashProofOfWork.Result
			{
				get { return Result; }
			}
		}

		#endregion Inner Class

		private readonly IHasher<TH> _hasher;

		public HashProofOfWorkFactory(IHasher<TH> hasher)
		{
			_hasher = hasher;
		}

		public IHashProofOfWork<TH> Create(TH basis, TH nonce)
		{
			return new HashProofOfWork<TH>(_hasher, basis, nonce);
		}

		public IHashProofOfWork<TH> Deserialize(byte[] serialized)
		{
			using (var ms = new MemoryStream())
			{
				ms.Write(serialized, 0, serialized.Length);
				ms.Seek(0, SeekOrigin.Begin);
				return ReadFrom(ms);
			}
		}

		public IHashProofOfWork<TH> ReadFrom(Stream s)
		{
			var basis = _hasher.ReadFrom(s);
			var nonce = _hasher.ReadFrom(s);

			return new HashProofOfWork<TH>(_hasher, basis, nonce);
		}
	}
}