﻿using System;
using System.Numerics;
using JLabs.Structures.Cryptography;

namespace JLabs.Structures.ProofOfWork
{
	public interface IProofOfWork : ISerializable, IComparable<IProofOfWork>
	{
		BigInteger MaxDifficulty { get; }
		BigInteger Difficulty { get; }
	}

	public interface IHashProofOfWork : IProofOfWork
	{
		IHash Basis { get; }
		IHash Nonce { get; }
		IHash Result { get; }
	}

	public interface IHashProofOfWork<TH> : IHashProofOfWork where TH : IHash
	{
		TH Basis { get; }
		TH Nonce { get; }
		TH Result { get; }
	}
}