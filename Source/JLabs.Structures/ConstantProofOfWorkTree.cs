﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Numerics;
//using JLabs.Structures.Utilities;

//namespace JLabs.Structures
//{
//    public class ConstantProofOfWorkTree<T> : ISerializable where T : IProofOfWork
//    {
//        #region Fields

//        private readonly MerkleTree.Node _tree;
//        private readonly byte _retainedProofsCount;

//        protected int[] _cachedRetainedProofIndicies;
//        private T[] _cachedRetainedProofs;

//        private ConstantProofOfWorkTreeStatistics _cachedStatistics;

//        #endregion Fields

//        #region Functions

//        public static double CompressionDifficultyRatio(BigInteger totalDifficulty, int itemSize, int itemsCount, int selectionCount)
//        {
//            var depth = (int) Math.Log(itemsCount, 2);

//            var compDifficulty = new ConstantProofOfWorkTreeStatistics(
//                itemsCount, depth,
//                Enumerable.Range(0, selectionCount).ToArray(),
//                Enumerable.Repeat(totalDifficulty, selectionCount).ToArray()
//            ).MinimumHashesToFake;

//            compDifficulty = BigInteger.Min(totalDifficulty, compDifficulty);

//            var totalSize = itemsCount * itemSize;
//            var compressedSize = selectionCount*(itemSize + SHA256Hash.Size*depth);
//            // add constants
//            compressedSize += sizeof (int);

//            var result = (double) compressedSize/totalSize;

//            result *= ((double) compDifficulty/(double) totalDifficulty);

//            return result;
//        }

//        #endregion Functions

//        #region Constructor

//        public ConstantProofOfWorkTree(IEnumerable<T> proofs, byte retainedProofsCount)
//        {
//            if (proofs == null)
//                throw new ArgumentNullException("proofs");

//            if (retainedProofsCount == 0)
//                throw new ArgumentOutOfRangeException("retainedProofsCount", "Retained proofs count must be a positive number");

//            _retainedProofsCount = retainedProofsCount;

//            var orderedProofs = proofs.OrderByDescending(p => p);
//          //  ItemsCount = orderedProofs.Length;
//            //Depth = (int)Math.Ceiling(Math.Log(ItemsCount, 2));



//            _tree = MerkleTree.BuildTree(
//                proofsArr,
//                pow => new MerkleTree.Node(pow.ToByteArray()),
//                false
//            );

//            // necessary so the number is initially correct before calling GetRetainedProofIndicies()
//            RetainedProofs = Enumerable.Repeat(default(T), retainedProofsCount);

//            _retainedProofIndicies = GetRetainedProofIndicies();

//            RetainedProofs = _retainedProofIndicies
//                .Select(rpi => proofsArr[rpi])
//                .ToList();

//            CascadeTrim(_tree, _retainedProofIndicies);
//        }

//        private ConstantProofOfWorkTree(
//            MerkleTree.Node trimmedRoot,
//            IEnumerable<T> retainedProofs,
//            int itemsCount, int depth
//        )
//        {
//            _tree = trimmedRoot;
//            RetainedProofs = retainedProofs.OrderByDescending(p => p).ToArray();
//            ItemsCount = itemsCount;
//            Depth = depth;
//        }

//        #endregion Constructor

//        #region Properties

//        public int ItemsCount { get; private set; }

//        public int Depth { get; private set; }

//        public SHA256Hash Root
//        {
//            get
//            {
//                return _tree.Value;
//            }
//        }

//        public IEnumerable<T> RetainedProofsx { get; private set; }

//        public ConstantProofOfWorkTreeStatistics Statistics
//        {
//            get
//            {
//                if (_cachedStatistics == null)
//                {
//                    _cachedStatistics = new ConstantProofOfWorkTreeStatistics(
//                        ItemsCount, Depth, GetRetainedProofIndicies(), RetainedProofs.Select(rp => rp.Difficulty)
//                    );
//                }

//                return _cachedStatistics;
//            }

//        }

//        #endregion Properties

//        #region Retained Proofs

//        public virtual int[] GetRetainedProofIndicies()
//        {
//            if (_cachedRetainedProofIndicies == null)
//            {
//                Func<SHA256Hash, int> hashToInt = h =>
//                {
//                    var arr = h.ToByteArray();

//                    var merged = Enumerable
//                        .Range(0, 4)
//                        .Select(r => arr.Nth(4, r))
//                        .Xor();

//                    return (int) Math.Abs(BitConverter.ToUInt32(merged, 0));
//                };

//                var currentHash = SHA256Hash.Of(
//                    Root.ToByteArray().Concat(BitConverter.GetBytes(_retainedProofsCount)).ToArray()
//                );
	            
//                var indicies = new List<int>();

//                while (indicies.Count < _retainedProofsCount)
//                {
//                    var index = hashToInt(currentHash);

//                    if (! indicies.Contains(index))
//                        indicies.Add(index);

//                    currentHash = SHA256Hash.Of(currentHash);
//                }

//                _cachedRetainedProofIndicies = indicies.OrderBy(i => i).ToArray();
//            }

//            return (int[]) _cachedRetainedProofIndicies.Clone();
//        }

//        public T[] GetRetainedProofs()
//        {
//            if (_cachedRetainedProofs == null)
//            {
//                var indicies = GetRetainedProofIndicies();

//                var result = new T[indicies.Length];

//                FillLeaves(_tree, result);

//                _cachedRetainedProofs = result;
//            }

//            return (T[]) _cachedRetainedProofs.Clone();
//        }

//        #endregion Retained Proofs

//        #region Serialization

//        private static int WalkGetRetainedProofs(List<T> retainedProofs, Func<byte[], T> deserializer, MerkleTree.Node root, int parentDepth)
//        {
//            if (root.Type == MerkleTree.NodeType.ContentLeaf)
//            {
//                retainedProofs.Add(deserializer(root.Content));
//                return parentDepth + 1;
//            }

//            if (root.Type != MerkleTree.NodeType.Branch)
//                return parentDepth;

//            var leftDepth = WalkGetRetainedProofs(retainedProofs, deserializer, root.Left, parentDepth);
//            var rightDepth = WalkGetRetainedProofs(retainedProofs, deserializer, root.Right, parentDepth);

//            return Math.Max(leftDepth, rightDepth);
//        }

//        public static ConstantProofOfWorkTree<T> Deserialize(
//            byte[] serialized, IDeserializer<T> deserializer
//        )
//        {
//            if (serialized == null)
//                throw new ArgumentNullException("serialized");

//            if (deserializer == null)
//                throw new ArgumentNullException("deserializer");

//            return Deserialize(serialized, deserializer.Deserialize);
//        }

//        public static ConstantProofOfWorkTree<T> Deserialize(
//            byte[] serialized, Func<byte[], T> deserializer
//        )
//        {
//            if (serialized == null)
//                throw new ArgumentNullException("serialized");

//            using (var ms = new MemoryStream())
//            {
//                ms.Write(serialized, 0, serialized.Length);
//                ms.Seek(0, SeekOrigin.Begin);
//                return ReadFrom(ms, deserializer);
//            }
//        }

//        public static ConstantProofOfWorkTree<T> ReadFrom(
//            Stream s, IDeserializer<T> deserializer
//        )
//        {
//            if (s == null)
//                throw new ArgumentNullException("s");

//            if (deserializer == null)
//                throw new ArgumentNullException("deserializer");

//            return ReadFrom(s, deserializer.Deserialize);
//        }

//        public static ConstantProofOfWorkTree<T> ReadFrom(
//            Stream s, Func<byte[], T> deserializer
//        )
//        {
//            if( s == null )
//                throw new ArgumentNullException("s");

//            if (deserializer == null)
//                throw new ArgumentNullException("deserializer");

//            try
//            {
//                var itemsCount = new BinaryReader(s).ReadInt32();
//                var tree = MerkleTree.Node.ReadFrom(s);
//                var retainedProofs = new List<T>();

//                var depth = WalkGetRetainedProofs(retainedProofs, deserializer, tree, 0);

//                return new ConstantProofOfWorkTree<T>(tree, retainedProofs, itemsCount, depth);
//            }
//            catch (Exception ex)
//            {
//                throw new FormatException("Invalid format", ex);
//            }
//        }

//        public byte[] ToByteArray()
//        {
//            using (var ms = new MemoryStream())
//            {
//                WriteTo(ms);
//                return ms.ToArray();
//            }
//        }

//        public void WriteTo(Stream s)
//        {
//            new BinaryWriter(s).Write(ItemsCount);
//            _tree.WriteTo(s);
//        }

//        #endregion Serialization

//        #region Utilities

//        // ReSharper disable once InconsistentNaming
//        private static int SHA256HashToInt(SHA256Hash hash)
//        {
//            var result = new byte[4];
//            var bytes = hash.ToByteArray();

//            for (var i = 0; i < 4; i++)
//            {
//                for (var j = 0; j < SHA256Hash.Size/4; j++)
//                {
//                    result[i] ^= bytes[j*SHA256Hash.Size/4 + i];
//                }
//            }

//            return BitConverter.ToInt32(bytes, 0);
//        }

//        private static bool IsInRange(int start, int end, int test)
//        {
//            return start <= test && end >= test;
//        }

//        private void CascadeTrim(MerkleTree.Node root, int[] retainedProofIndicies, int currentDepth=0, int currentBias=0)
//        {
//            if (root.Type != MerkleTree.NodeType.Branch)
//                return;

//            var domainSize = (int) Math.Pow(2, Depth - currentDepth - 1);

//            var leftRange = new
//            {
//                start = currentBias,
//                end = currentBias + domainSize-1
//            };

//            var rightRange = new
//            {
//                start = currentBias + domainSize,
//                end = currentBias + domainSize*2-1
//            };

//            if (retainedProofIndicies.Any(rpi => IsInRange(leftRange.start, leftRange.end, rpi)))
//                CascadeTrim(root.Left, retainedProofIndicies, currentDepth + 1, leftRange.start);
//            else
//                root.Left.Trim();

//            if (retainedProofIndicies.Any(rpi => IsInRange(rightRange.start, rightRange.end, rpi)))
//                CascadeTrim(root.Right, retainedProofIndicies, currentDepth + 1, rightRange.start);
//            else
//                root.Right.Trim();
//        }

//        #endregion Utilities
//    }
//}