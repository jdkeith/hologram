﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JLabs.Structures.Cryptography;
using JLabs.Structures.Units;

namespace JLabs.Structures
{
	public class MerkleTree : ISerializable
	{
		#region Nodes

		public enum NodeType : byte
		{
			Branch,
			Leaf,
			ContentLeaf,
			TrimmedLeaf
		}

		public class ReadOnlyNode : Node
		{
			private static byte[] SafeCloneByteArray(byte[] array)
			{
				if( array == null )
					return null;

				return (byte[]) array.Clone();
			}

			public static ReadOnlyNode CopyFrom(Node node)
			{
				if (node == null)
					return null;

				if( node is ReadOnlyNode )
					return (ReadOnlyNode) node;

				return new ReadOnlyNode
				{
					Left = CopyFrom(node.Left),
					Right = CopyFrom(node.Right),
					Value = node.Value,
					Content = SafeCloneByteArray(node.Content),
					Type = node.Type
				};
			}

			private ReadOnlyNode()
			{
			}

			public override bool Trim()
			{
				throw new NotSupportedException("Node is readonly.");
			}

			public override void Update()
			{
				throw new NotSupportedException("Node is readonly.");
			}
		}

		public class Node : ISerializable
		{
			#region Constructors

			protected Node()
			{
			}

			public Node(SHA256Hash hash, NodeType type = NodeType.Leaf)
			{
				if (type != NodeType.Leaf && type != NodeType.TrimmedLeaf)
					throw new NotSupportedException();

				Type = type;
				Value = hash;
				Content = new byte[0];
			}

			public Node(byte[] content)
			{
				if (content == null)
					throw new ArgumentNullException("content");

				Type = NodeType.ContentLeaf;
				Content = content;
				Value = Hash.SHA256.Of(Content);
			}

			public Node(Node a)
			{
				if (a == null)
					throw new ArgumentNullException("a");

				Left = a;
				Right = new Node(a.Value, NodeType.TrimmedLeaf);
				Value = a.Value + Right.Value;
				Type = NodeType.Branch;
				Content = new byte[0];
			}

			public Node(Node a, Node b, bool order = true, bool childrenMustBeDistinct = false)
			{
				if (a == null)
					throw new ArgumentNullException("a");

				if (b == null)
					throw new ArgumentNullException("b");

				Type = NodeType.Branch;

				if (! order || a.Value < b.Value)
				{
					Left = a;
					Right = b;
				}
				else
				{
					Left = b;
					Right = a;
				}

				if (childrenMustBeDistinct && Left.Value == Right.Value)
					throw new ArgumentException("Distinct nodes required", "b");

				Content = new byte[0];
				Value = Left.Value + Right.Value;
			}

			#endregion Constructors

			#region Properties

			public NodeType Type { get; protected set; }
			public SHA256Hash Value { get; protected set; }
			public Node Left { get; protected set; }
			public Node Right { get; protected set; }
			public byte[] Content { get; protected set; }

			#endregion Properties

			#region Manipulation and Walking

			public virtual Node UncleOf(SHA256Hash value)
			{
				if (Type != NodeType.Branch)
					throw new NotSupportedException();

				if (Left.Value == value)
					return Right;

				if (Right.Value == value)
					return Left;

				throw new NotSupportedException();
			}

			public virtual bool Trim()
			{
				if( Type == NodeType.TrimmedLeaf)
					return false;

				Left = null;
				Right = null;
				Type = NodeType.TrimmedLeaf;
				Content = new byte[0];

				return true;
			}

			public virtual void Update()
			{
				if (Type != NodeType.Branch)
					return;

				if (Left.Type == NodeType.TrimmedLeaf && Right.Type == NodeType.TrimmedLeaf)
				{
					Left = null;
					Right = null;
					Type = NodeType.TrimmedLeaf;
				}
			}

			#endregion Manipulation and Walking

			#region Serialization

			public static Node FromByteArray(byte[] bytes)
			{
				using (var ms = new MemoryStream())
				{
					ms.Write(bytes, 0, bytes.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public static Node ReadFrom(Stream s)
			{
				var type = (NodeType)s.ReadByte();

				switch (type)
				{
					case NodeType.Leaf:
					case NodeType.TrimmedLeaf:
						return new Node(
							Hash.SHA256.ReadFrom(s), type
						);

					case NodeType.ContentLeaf:
						{
							var contentLength = VarUInt.ReadFrom(s);
							var content = new byte[(int) contentLength];
							s.Read(content, 0, content.Length);

							return new Node(content);
						}

					case NodeType.Branch:
						break;

					default:
						throw new FormatException();
				}

				var left = ReadFrom(s);
				var right = ReadFrom(s);

				return new Node(left, right, false);
			}

			public byte[] ToByteArray()
			{
				using (var ms = new MemoryStream())
				{
					WriteTo(ms);
					return ms.ToArray();
				}
			}

			public virtual void WriteTo(Stream s)
			{
				s.WriteByte((byte) Type);

				if (Type == NodeType.ContentLeaf)
				{
					VarUInt contentLength = (uint) Content.Length;
					contentLength.WriteTo(s);
					s.Write(Content, 0, Content.Length);
					return;
				}

				if (Type != NodeType.Branch)
				{
					Value.WriteTo(s);
					return;
				}

				Left.WriteTo(s);
				Right.WriteTo(s);
			}

			#endregion Serialization

			#region Conversion

			public override string ToString()
			{
				var indicator = ' ';

				switch (Type)
				{
					case NodeType.Branch:
						indicator = '+';
						break;

					case NodeType.Leaf:
						indicator = '*';
						break;

					case NodeType.ContentLeaf:
						indicator = 'c';
						break;

					case NodeType.TrimmedLeaf:
						indicator = 'x';
						break;
				}

				return string.Format("{0} {1}", indicator, Value);
			}

			#endregion Conversion
		}

		#endregion Nodes

		#region Fields

		protected readonly Node _root;

		#endregion Fields

		#region Constructors

		protected MerkleTree(Node root)
		{
			_root = root;
		}

		public MerkleTree(IEnumerable<ISerializable> items)
		{
			_root = BuildTree(items, CreateNodeFromSerializable, true);
		}

		#endregion Constructors

		#region Properties

		public SHA256Hash Root
		{
			get { return _root.Value; }
		}

		#endregion Properties

		#region Manipulation

		public virtual bool Trim(ISerializable item)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			return Trim(Hash.SHA256.Of(item));
		}
		
		public virtual bool Trim(SHA256Hash hash)
		{
			var path = new List<Node>();

			var foundNode = WalkFind(path, _root, hash);

			if (! foundNode)
				return false;

			path.Insert(0, _root);

			if( ! path.Last().Trim() )
				return false;

			// walk the ancestors in reverse
			foreach (var node in Enumerable.Reverse(path).Skip(1))
			{
				node.Update();
			}

			return true;
		}

		#endregion Manipulation

		#region Proof and Verification

		public virtual IEnumerable<SHA256Hash> GenerateProof(ISerializable item)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			var value = Hash.SHA256.Of(item);

			var path = new List<Node>();

			var nodeFound = WalkFind(path, _root, value);

			if (!nodeFound)
				return Enumerable.Empty<SHA256Hash>();

			path.Insert(0, _root);

			var uncles = new List<SHA256Hash>();
			
			foreach (var node in Enumerable.Reverse(path).Skip(1))
			{
				var uncleNode = node.UncleOf(value);
				uncles.Add(uncleNode.Value);
				value = node.Value;
			}

			return uncles;
		}

		public virtual bool Verify(ISerializable item, IEnumerable<SHA256Hash> proof, bool failIfTrimmed = true)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			if (proof == null)
				throw new ArgumentNullException("proof");

			var value = Hash.SHA256.Of(item);

			if (failIfTrimmed)
			{
				var path = new List<Node>();

				var foundNode = WalkFind(path, _root, value);

				if (!foundNode)
					return false;
			}

			foreach (var hash in proof)
			{
				if (value < hash)
					value += hash;
				else
					value = hash + value;
			}

			return value == Root;
		}

		#endregion Proof and Verification

		#region Serialization

		public static MerkleTree Deserialize(byte[] serialized)
		{
			if (serialized == null)
				throw new ArgumentNullException("serialized");

			using (var ms = new MemoryStream())
			{
				ms.Write(serialized, 0, serialized.Length);
				ms.Seek(0, SeekOrigin.Begin);
				return ReadFrom(ms);
			}
		}

		public static MerkleTree ReadFrom(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			var root = Node.ReadFrom(s);

			return new MerkleTree(root);
		}

		public virtual byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		public virtual void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			_root.WriteTo(s);
		}

		#endregion Serialization

		#region Utilities

		protected virtual Node CreateNodeFromSerializable(ISerializable item)
		{
			return new Node(Hash.SHA256.Of(item));
		}

		private bool WalkFind(List<Node> path, Node current, SHA256Hash value)
		{
			if (current.Value == value && current.Type != NodeType.TrimmedLeaf)
				return true;

			if (current.Type != NodeType.Branch)
				return false;

			var foundLeft = WalkFind(path, current.Left, value);

			if (foundLeft)
			{
				path.Insert(0, current.Left);
				return true;
			}

			var foundRight = WalkFind(path, current.Right, value);

			if (foundRight)
			{
				path.Insert(0, current.Right);
				return true;
			}

			return false;
		}

		public static Node BuildTree<T>(IEnumerable<T> items, Func<T,Node> nodeCreator, bool ordered)
		{
			if (items == null)
				throw new ArgumentNullException("items");

			items = items.ToArray();

			if (items.Any(i => ReferenceEquals(i,null)))
				throw new ArgumentException("Items cannot contain null entries", "items");

			if (!items.Any())
			{
				return new Node(Hash.SHA256.MinValue, NodeType.TrimmedLeaf);
			}

			var nodes = items
				.Select(nodeCreator)
				.ToList();

			// continuously renormalize the list
			while (nodes.Count > 1)
			{
				var newNodes = new List<Node>();

				for (var i = 0; i < nodes.Count; i += 2)
				{
					if (i + 1 >= nodes.Count)
					{
						newNodes.Add(new Node(nodes[i]));
					}
					else
					{
						newNodes.Add(new Node(nodes[i], nodes[i + 1], ordered));
					}
				}

				nodes = newNodes;
			}

			return nodes[0];
		}

		#endregion Utilities
	}
}