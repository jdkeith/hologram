﻿using System;
using System.IO;

namespace JLabs.Structures.Tree
{
	/// <summary>
	/// A generic tree node
	/// </summary>
	public class TreeNode<T> : ISerializable where T : ISerializable
	{
		#region Factory

		/// <summary>
		/// Creates a new TreeNode factory with the specified deserializer
		/// </summary>
		/// <param name="deserializer">A deserializer which deserializes a node's value from a stream</param>
		/// <returns>A TreeNode factory</returns>
		public static IDeserializer<TreeNode<T>> Factory(IDeserializer<T> deserializer)
		{
			if (deserializer == null)
				throw new ArgumentNullException("deserializer");

			return new TreeNodeDeserializer<T>(deserializer.ReadFrom);
		}

		/// <summary>
		/// Creates a new TreeNode factory with the specified deserializer
		/// </summary>
		/// <param name="deserializer">A function which deserializes a node's value from a stream</param>
		/// <returns>A TreeNode factory</returns>
		public static IDeserializer<TreeNode<T>> Factory(Func<Stream, T> deserializer)
		{
			if (deserializer == null)
				throw new ArgumentNullException("deserializer");

			return new TreeNodeDeserializer<T>(deserializer);
		}

		private class TreeNodeDeserializer<TT> : IDeserializer<TreeNode<TT>> where TT : ISerializable
		{
			private readonly Func<Stream, TT> _deserializer;

			public TreeNodeDeserializer(Func<Stream, TT> deserializer)
			{
				_deserializer = deserializer;
			}

			public TreeNode<TT> Deserialize(byte[] serialized)
			{
				if( serialized == null )
					throw new ArgumentNullException("serialized");

				using (var ms = new MemoryStream())
				{
					ms.Write(serialized, 0, serialized.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public TreeNode<TT> ReadFrom(Stream s)
			{
				if (s == null)
					throw new ArgumentNullException("s");

				if (s == null)
					throw new ArgumentNullException("s");

				var type = (TreeNodeType) s.ReadByte();
				TreeNode<TT> left = null;
				TreeNode<TT> right = null;
				TT value;

				if (type == TreeNodeType.Branch)
				{
					var children = s.ReadByte();

					if ((children & 0x01) > 0)
						left = ReadFrom(s);

					if ((children & 0x02) > 0)
						right = ReadFrom(s);

					value = default(TT);
				}
				else
				{
					value = _deserializer(s);
				}

				return new TreeNode<TT>
				{
					Type = type,
					Left = left,
					Right = right,
					Value = value
				};
			}
		}

		#endregion Factory

		#region Constructors

		/// <summary>
		/// Creates an empty tree node
		/// </summary>
		protected TreeNode()
		{
		
		}

		/// <summary>
		/// Creates a branch node
		/// </summary>
		/// <param name="left">The left child</param>
		/// <param name="right">The right child</param>
		/// <remarks>Null children are allowed</remarks>
		public TreeNode(TreeNode<T> left, TreeNode<T> right)
		{
			Left = left;
			Right = right;
			Value = default(T);
			Type = TreeNodeType.Branch;
		}

		/// <summary>
		/// Creates a new leaf node
		/// </summary>
		/// <param name="value">The content to store in the node</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the content parameter is null
		/// </exception>
		public TreeNode(T value)
		{
			if( ReferenceEquals(value, null))
				throw new ArgumentNullException("value");

			Left = null;
			Right = null;
			Value = value;
			Type = TreeNodeType.Leaf;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// The child on the left branch of the node. Null if the node is not a branch node.
		/// </summary>
		public TreeNode<T> Left { get; protected set; }

		/// <summary>
		/// The child on the right branch of the node. Null if the node is not a branch node.
		/// </summary>
		public TreeNode<T> Right { get; protected set; }

		/// <summary>
		/// The value of the node. An empty array if the node is not a content node.
		/// </summary>
		public T Value { get; protected set; }

		/// <summary>
		/// The node type (branch, leaf, etc.)
		/// </summary>
		public TreeNodeType Type { get; protected set; }

		#endregion Properties

		#region Serialization

		/// <summary>
		/// Serializes the tree node to a byte array
		/// </summary>
		/// <returns>The serialized tree node</returns>
		public virtual byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the tree node to a stream
		/// </summary>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public virtual void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			s.WriteByte((byte)Type);

			if (Type == TreeNodeType.Branch)
			{
				byte children = 0;
				children |= (byte) (Left != null ? 0x01 : 0x00);
				children |= (byte) (Right != null ? 0x02 : 0x00);

				s.WriteByte(children);

				if (Left != null) Left.WriteTo(s);
				if (Right != null) Right.WriteTo(s);
			}
			else
			{
				Value.WriteTo(s);
			}
		}

		#endregion Serialization

		#region Conversion

		/// <summary>
		/// Provides a string representation of the Merkle tree node
		/// </summary>
		/// <returns>A string representation of the Merkle tree node</returns>
		public override string ToString()
		{
			var indicator = ' ';

			switch (Type)
			{
				case TreeNodeType.Branch:
					indicator = '+';
					break;

				case TreeNodeType.Leaf:
					indicator = '*';
					break;

				case TreeNodeType.ContentLeaf:
					indicator = 'c';
					break;

				case TreeNodeType.TrimmedLeaf:
					indicator = 'x';
					break;
			}

			return string.Format("{0} {1}", indicator, Value);
		}

		#endregion Conversion
	}
}