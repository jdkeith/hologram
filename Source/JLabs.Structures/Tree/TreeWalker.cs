﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JLabs.Structures.Tree
{
	/// <summary>
	/// Indicates which branch in a tree to take
	/// </summary>
	public enum PathBranch
	{
		/// <summary>
		/// The left branch
		/// </summary>
		Left,

		/// <summary>
		/// The right branch
		/// </summary>
		Right
	}

	/// <summary>
	/// The result of a search for a specific node in a tree
	/// </summary>
	/// <typeparam name="T">The type of the node to search for</typeparam>
	public class PathResult<T> where T : ISerializable
	{
		/// <summary>
		/// Creates a new PathResult object
		/// </summary>
		/// <param name="path">An enumeration of left or right steps to reach the node from the root</param>
		/// <param name="node">The node which was searched for</param>
		public PathResult(PathBranch[] path, TreeNode<T> node)
		{
			Path = path;
			Node = node;
		}

		/// <summary>
		/// An enumeration of left or right steps to reach the node from the root
		/// </summary>
		public PathBranch[] Path { get; private set; }

		/// <summary>
		/// The node which was searched for
		/// </summary>
		public TreeNode<T> Node { get; private set; }
	}

	/// <summary>
	/// A utility class for walking and searching trees
	/// </summary>
	public static class TreeWalker
	{
		#region Flatten

		/// <summary>
		/// Flattens a tree and returns nodes in node order
		/// </summary>
		/// <typeparam name="T">The type of node to return</typeparam>
		/// <param name="tree">The root of the tree to flatten</param>
		/// <param name="leafNodesOnly">Whether or not only leaf nodes are returned</param>
		/// <returns>An enumeration of nodes in node order</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree parameter is null
		/// </exception>
		public static IEnumerable<TreeNode<T>> Flatten<T>(TreeNode<T> tree, bool leafNodesOnly = true) where T : ISerializable
		{
			while (true)
			{
				if (tree == null)
					throw new ArgumentNullException("tree");

				if (tree.Type == TreeNodeType.Leaf || tree.Type == TreeNodeType.ContentLeaf)
					yield return tree;

				if (tree.Type != TreeNodeType.Branch)
					yield break;

				if (! leafNodesOnly)
					yield return tree;

				if (tree.Left != null)
				{
					foreach (var leftNode in Flatten(tree.Left, leafNodesOnly))
						yield return leftNode;
				}

				if (tree.Right == null)
					yield break;

				// recursive tail call replaced with loop
				tree = tree.Right;
			}
		}

		#endregion Flatten

		#region Path To First

		/// <summary>
		/// Gets the path to the first node whose value is considered equal to the test value
		/// </summary>
		/// <typeparam name="T">The type of node to search for</typeparam>
		/// <param name="tree">The tree to search within</param>
		/// <param name="item">The node to search for</param>
		/// <returns>
		/// A PathResult object indicating how to navigate from the tree root to the given node
		/// if the node is found, null otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree or item parameters are null
		/// </exception>
		public static PathResult<T> PathToFirst<T>(TreeNode<T> tree, T item) where T : ISerializable
		{
			if( tree == null )
				throw new ArgumentNullException("tree");

			if( ReferenceEquals(item, null) )
				throw new ArgumentNullException("item");

			return PathToFirstInternal(
				new PathBranch[0], tree, item,
				(a,b) => ReferenceEquals(a, null) ? ReferenceEquals(b, null) : a.Equals(b)
			);
		}

		/// <summary>
		/// Gets the path to the first node whose value is considered equal to the test node
		/// value using a specified equality comparer
		/// </summary>
		/// <typeparam name="T">The type of node to search for</typeparam>
		/// <param name="tree">The tree to search within</param>
		/// <param name="item">The node to search for</param>
		/// <param name="comparer">The equality comparer to use to determine value equality</param>
		/// <returns>
		/// A PathResult object indicating how to navigate from the tree root to the given node
		/// if the node is found, null otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree, item, or comparer parameters are null
		/// </exception>
		public static PathResult<T> PathToFirst<T>(TreeNode<T> tree, T item, IEqualityComparer<T> comparer)
			where T : ISerializable
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			if (ReferenceEquals(item, null))
				throw new ArgumentNullException("item");

			if (comparer == null)
				throw new ArgumentNullException("comparer");

			return PathToFirstInternal(
				new PathBranch[0], tree, item,
				comparer.Equals
			);
		}

		/// <summary>
		/// Gets the path to the first node whose value is considered equal to the test node
		/// value using a specified comparer
		/// </summary>
		/// <typeparam name="T">The type of node to search for</typeparam>
		/// <param name="tree">The tree to search within</param>
		/// <param name="item">The node to search for</param>
		/// <param name="comparer">The equality comparer to use to determine value equality</param>
		/// <returns>
		/// A PathResult object indicating how to navigate from the tree root to the given node
		/// if the node is found, null otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree, item, or comparer parameters are null
		/// </exception>
		public static PathResult<T> PathToFirst<T>(TreeNode<T> tree, T item, IComparer<T> comparer)
			where T : ISerializable
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			if (ReferenceEquals(item, null))
				throw new ArgumentNullException("item");

			if (comparer == null)
				throw new ArgumentNullException("comparer");

			return PathToFirstInternal(
				new PathBranch[0], tree, item,
				(a,b) => comparer.Compare(a,b) == 0
			);
		}

		/// <summary>
		/// Gets the path to the first node whose value is considered equal to the test node
		/// value using a specified equality comparer
		/// </summary>
		/// <typeparam name="T">The type of node to search for</typeparam>
		/// <param name="tree">The tree to search within</param>
		/// <param name="item">The node to search for</param>
		/// <param name="comparer">The equality comparer to use to determine value equality</param>
		/// <returns>
		/// A PathResult object indicating how to navigate from the tree root to the given node
		/// if the node is found, null otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree, item, or comparer parameters are null
		/// </exception>
		public static PathResult<T> PathToFirst<T>(TreeNode<T> tree, T item, Func<T,T,bool> comparer)
			where T : ISerializable
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			if (ReferenceEquals(item, null))
				throw new ArgumentNullException("item");

			if (comparer == null)
				throw new ArgumentNullException("comparer");

			return PathToFirstInternal(
				new PathBranch[0], tree, item,
				comparer
			);
		}

		private static PathResult<T> PathToFirstInternal<T>(
			IEnumerable<PathBranch> path, TreeNode<T> tree, T item, Func<T,T, bool> equivalenceTest 
		) where T : ISerializable
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			if (ReferenceEquals(item, null))
				throw new ArgumentNullException("item");

			if (tree.Type == TreeNodeType.Leaf || tree.Type == TreeNodeType.ContentLeaf)
			{
				if( tree.Value.GetHashCode() == item.GetHashCode() && equivalenceTest(tree.Value, item) )
					return new PathResult<T>(path.ToArray(), tree);

				return null;
			}

			path = path.ToArray();

			if (tree.Type != TreeNodeType.Branch)
				return null;

			if (tree.Left != null)
			{
				var result = PathToFirstInternal(path.Concat(new[] {PathBranch.Left}), tree.Left, item, equivalenceTest);

				if (result != null)
					return result;
			}

			if (tree.Right == null)
				return null;

			return PathToFirstInternal(path.Concat(new[] { PathBranch.Right }), tree.Right, item, equivalenceTest);
		}

		#endregion Path To First

		#region Binary Find

		/// <summary>
		/// Gets the path to the first node whose value is considered equal to the test value
		/// using a binary search
		/// </summary>
		/// <typeparam name="T">The type of node to search for</typeparam>
		/// <param name="tree">The tree to search within</param>
		/// <param name="item">The node to search for</param>
		/// <param name="comparer">The comparer to to determine value order</param>
		/// <returns>
		/// A PathResult object indicating how to navigate from the tree root to the given node
		/// if the node is found, null otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree or item parameters are null
		/// </exception>
		public static PathResult<T> SortedBinaryPathToFirst<T>(TreeNode<T> tree, T item, IComparer<T> comparer)
			where T : ISerializable
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			if (ReferenceEquals(item, null))
				throw new ArgumentNullException("item");

			if (comparer == null)
				throw new ArgumentNullException("comparer");

			return SortedBinaryPathToFirstInternal(
				new PathBranch[0], tree, item, comparer.Compare
			);
		}

		/// <summary>
		/// Gets the path to the first node whose value is considered equal to the test value
		/// using a binary search
		/// </summary>
		/// <typeparam name="T">The type of node to search for</typeparam>
		/// <param name="tree">The tree to search within</param>
		/// <param name="item">The node to search for</param>
		/// <param name="comparer">The comparer to to determine value order</param>
		/// <returns>
		/// A PathResult object indicating how to navigate from the tree root to the given node
		/// if the node is found, null otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree or item parameters are null
		/// </exception>
		public static PathResult<T> SortedBinaryPathToFirst<T>(TreeNode<T> tree, T item, Func<T, T, int> comparer)
			where T : ISerializable
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			if (ReferenceEquals(item, null))
				throw new ArgumentNullException("item");

			if (comparer == null)
				throw new ArgumentNullException("comparer");

			return SortedBinaryPathToFirstInternal(
				new PathBranch[0], tree, item, comparer
			);
		}

		private static PathResult<T> SortedBinaryPathToFirstInternal<T>(IEnumerable<PathBranch> path, TreeNode<T> tree, T item, Func<T, T, int> comparatorTest) where T : ISerializable
		{
			while (true)
			{
				if (tree == null)
					throw new ArgumentNullException("tree");

				if (ReferenceEquals(item, null))
					throw new ArgumentNullException("item");

				if (tree.Type == TreeNodeType.Leaf || tree.Type == TreeNodeType.ContentLeaf)
				{
					if (tree.Value.GetHashCode() == item.GetHashCode() && comparatorTest(tree.Value, item) == 0)
						return new PathResult<T>(path.ToArray(), tree);

					return null;
				}

				path = path.ToArray();

				if (tree.Type != TreeNodeType.Branch)
					return null;

				PathBranch branch;

				if (tree.Left != null && comparatorTest(item, tree.Value) < 0)
					branch = PathBranch.Left;
				else if (tree.Right != null && comparatorTest(item, tree.Value) > 0)
					branch = PathBranch.Right;
				else
					return null;

				// tail recursive call replaced by loop
				path = path.Concat(new[] {branch});
				tree = tree.Right;
			}
		}

		#endregion Binary Find

		#region Grab

		/// <summary>
		/// Grabs the given child of a specified tree node
		/// </summary>
		/// <typeparam name="T">The type of node to get</typeparam>
		/// <param name="node">A node to get a child from</param>
		/// <param name="branch">Whether to take the left or right child of a branch node</param>
		/// <returns>The left or right child of the tree node</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the node parameter is null
		/// </exception>
		/// <exception cref="System.NotSupportedException">
		/// Thrown if the node type is not Branch
		/// </exception>
		public static TreeNode<T> Grab<T>(TreeNode<T> node, PathBranch branch) where T : ISerializable
		{
			if (node == null)
				throw new ArgumentNullException("node");

			if (node.Type != TreeNodeType.Branch)
				throw new NotSupportedException("Only branch trees may be grabbed");

			return branch == PathBranch.Left ? node.Left : node.Right;
		}

		/// <summary>
		/// Grabs the given child a series of steps from the specified tree
		/// </summary>
		/// <typeparam name="T">The type of node to get</typeparam>
		/// <param name="tree">The root of the tree to search</param>
		/// <param name="path">A series of left/right steps to take to get to the child</param>
		/// <returns>The node at the given path</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the node parameter is null
		/// </exception>
		/// <exception cref="System.NotSupportedException">
		/// Thrown if the path tries to go beyond a branch node
		/// </exception>
		public static TreeNode<T> Grab<T>(TreeNode<T> tree, IEnumerable<PathBranch> path) where T : ISerializable
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			return path.Aggregate(tree, Grab);
		}

		#endregion Grab
	}
}