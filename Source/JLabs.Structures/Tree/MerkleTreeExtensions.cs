﻿using System;
using System.Collections.Generic;
using System.Linq;
using JLabs.Structures.Cryptography;

namespace JLabs.Structures.Tree
{
	/// <summary>
	/// Provides extension methods for creating and manipulating Merkle Trees
	/// </summary>
	public static class MerkleTreeExtensions
	{
		#region Create

		/// <summary>
		/// Creates a new Merkle Tree from the given items
		/// </summary>
		/// <param name="items">An enumeration of items to build a Merkle Tree from</param>
		/// <param name="summarized">
		/// Whether or not the nodes in the Merkle Tree are SHA256Hash values of
		/// their respective item or whether they contain a serialization of their respective item
		/// </param>
		/// <param name="childrenMustBeDistinct">
		/// When true, the value of all leaf nodes must be unique
		/// </param>
		/// <returns></returns>
        public static MerkleTreeNode Create(
			IEnumerable<ISerializable> items, bool summarized = false, bool childrenMustBeDistinct = false
		)
	    {
            if( items == null )
                throw new ArgumentNullException("items");

	        MerkleTreeNode[] nodes;

	        Action<ISerializable> checkNull = i =>
	        {
		        if (i == null) throw new ArgumentException("Items cannot contain null objects", "items");
	        };

	        if (summarized)
	        {
		        nodes = items.Select(i =>
		        {
			        checkNull(i);
			        return new MerkleTreeNode(SHA256Hash.Factory.Of(i));
		        }).ToArray();
	        }
	        else
	        {
				nodes = items.Select(i =>
				{
					checkNull(i);
					return new MerkleTreeNode(i.ToByteArray());
				}).ToArray();
	        }

	        if (nodes.Length == 0)
                throw new ArgumentException("Items cannot be empty", "items");

	        while (nodes.Length > 1)
	        {
		        var replace = new MerkleTreeNode[(int) Math.Ceiling((double)nodes.Length/2)];

		        for (var i = 0; i < nodes.Length; i += 2)
		        {
			        var left = nodes[i];

			        if (i + 1 < nodes.Length)
			        {
				        replace[i/2] = new MerkleTreeNode(left, nodes[i + 1], true, childrenMustBeDistinct);
			        }
			        else
			        {
				        replace[i/2] = new MerkleTreeNode(left);
			        }
		        }

		        nodes = replace;
	        }

	        return nodes.First();
	    }

		#endregion Create

		#region Manipulation

		/// <summary>
		/// Trims a leaf node from the given tree
		/// </summary>
		/// <param name="tree">The tree to trim within</param>
		/// <param name="item">The value to trim</param>
		/// <returns>True if the item was trimmed, false if it was not found</returns>
		public static bool Trim(this MerkleTreeNode tree, ISerializable item)
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			if (item == null)
				throw new ArgumentNullException("item");

			return Trim(tree, SHA256Hash.Factory.Of(item));
		}

		/// <summary>
		/// Trims a leaf node from the given tree
		/// </summary>
		/// <param name="tree">The tree to trim within</param>
		/// <param name="hash">The value of the node to trim</param>
		/// <returns>True if the item was trimmed, false if it was not found</returns>
		public static bool Trim(this MerkleTreeNode tree, SHA256Hash hash)
		{
			var foundNode = TreeWalker.PathToFirst(tree, hash);

			return foundNode != null && ((MerkleTreeNode) foundNode.Node).Trim();
		}

		/// <summary>
		/// Trims leaf nodes which match a predicate from the given tree
		/// </summary>
		/// <param name="tree">The tree to trim within</param>
		/// <param name="predicate">The predicate which determines which nodes to trim</param>
		/// <returns>The number of nodes which were trimmed</returns>
		public static int TrimIf(this MerkleTreeNode tree, Func<MerkleTreeNode, bool> predicate)
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			if (predicate == null)
				throw new ArgumentNullException("predicate");

			var flattened = TreeWalker.Flatten(tree);

			var count = 0;

			foreach (var mNode in flattened.Cast<MerkleTreeNode>().Where(predicate))
			{
				mNode.Trim();
				count++;
			}

			return count;
		}

		/// <summary>
		/// Normalizes the tree by trimming all branches with trimmed children
		/// </summary>
		/// <param name="tree">The tree to normalize</param>
		public static void Normalize(this MerkleTreeNode tree)
		{
			bool anyChange;

			do
			{
				anyChange = false;

				var branchesToTrim = TreeWalker
					.Flatten(tree, false)
					.Where( n =>
						n.Type == TreeNodeType.Branch && n.Left.Type == TreeNodeType.TrimmedLeaf &&
						n.Right.Type == TreeNodeType.TrimmedLeaf
					);
				
				foreach (var branchToTrim in branchesToTrim)
				{
					((MerkleTreeNode) branchToTrim).Trim();
					anyChange = true;
				}

			} while (anyChange);
		}

		#endregion Manipulation

		#region Proof and Verification

		/// <summary>
		/// Generates a proof-of-presence for an item within a tree
		/// </summary>
		/// <param name="tree">The tree to generate the proof within</param>
		/// <param name="item">The item to search for</param>
		/// <returns>
		/// An enumeration of ancestor siblings proving the presence of an item in the tree
		/// or an empty enumeration if the item was not found in the tree
		/// </returns>
		/// <remarks>The node for the item itself is NOT part of the proof</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree or item parameters are null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the tree node is not a branch
		/// </exception>
		public static IEnumerable<SHA256Hash> GenerateProof(this MerkleTreeNode tree, ISerializable item)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			return GenerateProof(tree, SHA256Hash.Factory.Of(item));
		}

		/// <summary>
		/// Generates a proof-of-presence for an node value within a tree
		/// </summary>
		/// <param name="tree">The tree to generate the proof within</param>
		/// <param name="hash">The node value to search for</param>
		/// <returns>
		/// An enumeration of ancestor siblings proving the presence of a node with
		/// the specified value within the tree or an empty enumeration
		/// if such a node was not found in the tree
		/// </returns>
		/// <remarks>The node for the item itself is NOT part of the proof</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree or item parameters are null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the tree node is not a branch
		/// </exception>
		public static IEnumerable<SHA256Hash> GenerateProof(this MerkleTreeNode tree, SHA256Hash hash)
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			if (tree.Type != TreeNodeType.Branch)
				throw new InvalidOperationException("Only branch tree nodes can generate proofs");

			var foundNode = TreeWalker.PathToFirst(tree, hash);

			if (foundNode == null)
				return Enumerable.Empty<SHA256Hash>();

			var siblings = new List<SHA256Hash>();

			var currentTree = tree;

			foreach (var path in foundNode.Path.Take(foundNode.Path.Length - 0))
			{
				if (currentTree.Type != TreeNodeType.Branch)
					return Enumerable.Empty<SHA256Hash>();

				MerkleTreeNode child;

				if (path == PathBranch.Left)
				{
					child = (MerkleTreeNode) TreeWalker.Grab(currentTree, PathBranch.Right);
					currentTree = (MerkleTreeNode) TreeWalker.Grab(currentTree, PathBranch.Left);
				}
				else
				{
					child = (MerkleTreeNode) TreeWalker.Grab(currentTree, PathBranch.Left);
					currentTree = (MerkleTreeNode) TreeWalker.Grab(currentTree, PathBranch.Right);
				}

				if( child == null || currentTree == null)
					return Enumerable.Empty<SHA256Hash>();

				siblings.Add(child.Value);
			}

			siblings.Reverse();

			return siblings;
		}

		/// <summary>
		/// Verifies the presence of an item within a tree
		/// </summary>
		/// <param name="tree">The tree to search within</param>
		/// <param name="item">The item to search for</param>
		/// <param name="proof">The proof-of-presence for the item</param>
		/// <returns>
		/// True if the item is in the tree, false otherwise
		/// </returns>
		/// <remarks>
		/// Proofs-of-presence continue to work on trimmed trees
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree, item, or proof parameters are null
		/// </exception>
		public static bool Verify(
			this MerkleTreeNode tree, ISerializable item, IEnumerable<SHA256Hash> proof
		)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			return Verify(tree, SHA256Hash.Factory.Of(item), proof);
		}

		/// <summary>
		/// Verifies the presence of a node with the given value within a tree
		/// </summary>
		/// <param name="tree">The tree to search within</param>
		/// <param name="hash">The node value to sesarch for</param>
		/// <param name="proof">The proof-of-presence for the item</param>
		/// <returns>
		/// True if the item is in the tree, false otherwise
		/// </returns>
		/// <remarks>
		/// Proofs-of-presence continue to work on trimmed trees
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the tree or proof parameters are null
		/// </exception>
		public static bool Verify(
			this MerkleTreeNode tree, SHA256Hash hash, IEnumerable<SHA256Hash> proof
		)
		{
			if (tree == null)
				throw new ArgumentNullException("tree");

			return Verify(tree.Value, hash, proof);
		}

		/// <summary>
		/// Verifies the presence of an item within a tree
		/// whose root value is the value specified
		/// </summary>
		/// <param name="rootValue">The value of the tree's root</param>
		/// <param name="item">The item to search for</param>
		/// <param name="proof">The proof-of-presence for the item</param>
		/// <returns>
		/// True if the item is in the tree, false otherwise
		/// </returns>
		/// <remarks>
		/// Proofs-of-presence continue to work on trimmed trees
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the item or proof parameters are null
		/// </exception>
		public static bool Verify(
			SHA256Hash rootValue, ISerializable item, IEnumerable<SHA256Hash> proof
			)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			return Verify(rootValue, SHA256Hash.Factory.Of(item), proof);
		}

		/// <summary>
		/// Verifies the presence of a node with the given value within a tree
		/// </summary>
		/// <param name="rootValue">The tree to search within</param>
		/// <param name="hash">The node value to sesarch for</param>
		/// <param name="proof">The proof-of-presence for the item</param>
		/// <returns>
		/// True if the item is in the tree, false otherwise
		/// </returns>
		/// <remarks>
		/// Proofs-of-presence continue to work on trimmed trees
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the proof parameter is null
		/// </exception>
		public static bool Verify(
			SHA256Hash rootValue, SHA256Hash hash, IEnumerable<SHA256Hash> proof
		)
		{
			if (proof == null)
				throw new ArgumentNullException("proof");

			foreach (var p in proof)
			{
				var left = hash;
				var right = p;

				if (left.CompareTo(right) > 0)
				{
					left = p;
					right = hash;
				}

				hash = SHA256Hash.Factory.Combine(left, right);
			}

			return rootValue.Equals(hash);
		}

		#endregion Proof and Verification
	}
}