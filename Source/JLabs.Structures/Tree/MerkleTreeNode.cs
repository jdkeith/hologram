﻿using System;
using System.IO;
using JLabs.Structures.Cryptography;
using JLabs.Structures.Units;

namespace JLabs.Structures.Tree
{
	/// <summary>
	/// A binary tree
	/// </summary>
    public class MerkleTreeNode : TreeNode<SHA256Hash>
    {
		#region Factory

		/// <summary>
		/// Gets a MerkleTreeNode factory
		/// </summary>
		/// <returns>A MerkleTreeNode factory</returns>
		public static IDeserializer<MerkleTreeNode> Factory()
		{
			return new MerkleTreeFactory();
		}

		private new static IDeserializer<TreeNode<SHA256Hash>> Factory(IDeserializer<SHA256Hash> deserializer)
		{
			throw new NotImplementedException();
		}

		private new static IDeserializer<TreeNode<SHA256Hash>> Factory(Func<Stream, SHA256Hash> deserializer)
		{
			throw new NotImplementedException();
		}

		private class MerkleTreeFactory : IDeserializer<MerkleTreeNode>
		{
			public MerkleTreeNode Deserialize(byte[] serialized)
			{
				if( serialized == null )
					throw new ArgumentNullException("serialized");

				using (var ms = new MemoryStream())
				{
					ms.Write(serialized, 0, serialized.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public MerkleTreeNode ReadFrom(Stream s)
			{
				if( s == null )
					throw new ArgumentNullException("s");

				var type = (TreeNodeType) s.ReadByte();

				switch (type)
				{
					case TreeNodeType.Leaf:
					case TreeNodeType.TrimmedLeaf:
						return new MerkleTreeNode(
							SHA256Hash.Factory.ReadFrom(s), type
						);

					case TreeNodeType.ContentLeaf:
					{
						var contentLength = VarUInt.Factory.ReadFrom(s);
						var content = new byte[(int)contentLength];
						s.Read(content, 0, content.Length);

						return new MerkleTreeNode(content);
					}

					case TreeNodeType.Branch:
						break;

					default:
						throw new FormatException();
				}

				var left = ReadFrom(s);
				var right = ReadFrom(s);

				return new MerkleTreeNode(left, right, false);
			}
		}

		#endregion Factory

        #region Constructors

		/// <summary>
		/// Creates a new Merkle tree
		/// </summary>
        protected MerkleTreeNode()
        {
        }

		/// <summary>
		/// Creates a new Merkle tree with the given hash and type
		/// </summary>
		/// <param name="hash">The id of the node</param>
		/// <param name="type">The type of node</param>
		/// <exception cref="System.NotSupportedException">
		/// Thrown if the type parameter is neither Leaf nor TrimmedLeaf
		/// </exception>
        public MerkleTreeNode(SHA256Hash hash, TreeNodeType type = TreeNodeType.Leaf)
        {
            if (type != TreeNodeType.Leaf && type != TreeNodeType.TrimmedLeaf)
                throw new NotSupportedException();

            Type = type;
            Value = hash;
            Content = new byte[0];
        }

		/// <summary>
		/// Creates a new content Merkle tree node with the given content
		/// </summary>
		/// <param name="content">The content to store in the node</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the content parameter is null
		/// </exception>
        public MerkleTreeNode(byte[] content)
        {
            if (content == null)
                throw new ArgumentNullException("content");

            Type = TreeNodeType.ContentLeaf;
            Content = content;
            Value = SHA256Hash.Factory.Of(Content);
        }

		/// <summary>
		/// Creates a branch Merkle tree node with duplicated children
		/// </summary>
		/// <param name="a">The child of the branch</param>
		/// <remarks>The right child will be trimmed automatically</remarks>
        public MerkleTreeNode(MerkleTreeNode a)
        {
            if (a == null)
                throw new ArgumentNullException("a");

            Left = a;
            Right = new MerkleTreeNode(a.Value, TreeNodeType.TrimmedLeaf);
			Value = SHA256Hash.Factory.Combine(a.Value, Right.Value);
            Type = TreeNodeType.Branch;
            Content = new byte[0];
        }

		/// <summary>
		/// Creates a branch Merkle tree with two children which may or may not be distinct
		/// </summary>
		/// <param name="a">The first child of the branch</param>
		/// <param name="b">The second child of the branch</param>
		/// <param name="order">
		/// Whether or not the children should be ordered. If true, the child with the lower
		/// hash will always appear on the left and the higher on the right. If false,
		/// the "a" child will appear on the left and the "b" child will appear on the right.
		/// </param>
		/// <param name="childrenMustBeDistinct">
		/// When true, the parameters "a" and "b" must not have equivalent values
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either child node is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the childrenMustBeDistinct property is true and both child parameters have equivalent values
		/// </exception>
        public MerkleTreeNode(MerkleTreeNode a, MerkleTreeNode b, bool order = true, bool childrenMustBeDistinct = false)
        {
            if (a == null)
                throw new ArgumentNullException("a");

            if (b == null)
                throw new ArgumentNullException("b");

            Type = TreeNodeType.Branch;

            if (!order || a.Value < b.Value)
            {
                Left = a;
                Right = b;
            }
            else
            {
                Left = b;
                Right = a;
            }

            if (childrenMustBeDistinct && Left.Value == Right.Value)
                throw new ArgumentException("Distinct nodes required", "b");

            Content = new byte[0];
			Value = SHA256Hash.Factory.Combine(Left.Value, Right.Value);
        }

        #endregion Constructors

        #region Properties

		/// <summary>
		/// The child on the left branch of the node. Null if the node is not a branch node.
		/// </summary>
		public new MerkleTreeNode Left
		{
			get { return (MerkleTreeNode)base.Left; }
			protected set { base.Left = value; }
		}

		/// <summary>
		/// The child on the right branch of the node. Null if the node is not a branch node.
		/// </summary>
		public new MerkleTreeNode Right
		{
			get { return (MerkleTreeNode) base.Right; }
			protected set { base.Right = value; }
		}

		/// <summary>
		/// Content stored in the node. An empty array if the node is not a content node.
		/// </summary>
        public byte[] Content { get; protected set; }

        #endregion Properties

        #region Manipulation and Walking

		///// <summary>
		///// Gets the sibling of the given child
		///// </summary>
		///// <param name="child">The child to get the sibling of</param>
		///// <returns>
		///// The sibling node of the given child. That is, the left node if the given
		///// node is the right child, the right node if the given node
		///// is the left child.
		///// </returns>
		///// <exception cref="System.ArgumentNullException">
		///// Thrown if the child parameter is null
		///// </exception>
		///// <exception cref="System.NotSupportedException">
		///// Thrown if the invocant node is not a branch
		///// </exception>
		///// <exception cref="NodeNotFoundException">
		///// Thrown if the given node value is not one of a child on the branch
		///// </exception>
		//public virtual MerkleTreeNode SiblingOf(MerkleTreeNode child)
		//{
		//	if (child == null)
		//		throw new ArgumentNullException("child");

		//	return SiblingOf(child.Value);
		//}

		///// <summary>
		///// Gets the sibling of the given child
		///// </summary>
		///// <param name="childValue">The value of the child to get the sibling of</param>
		///// <returns>
		///// The sibling node of the given child. That is, the left node if the given
		///// node value is that of the right child, the right node if the given node value
		///// is that of the left child.
		///// </returns>
		///// <exception cref="System.NotSupportedException">
		///// Thrown if the invocant node is not a branch
		///// </exception>
		///// <exception cref="NodeNotFoundException">
		///// Thrown if the given node value is not one of a child on the branch
		///// </exception>
		//public virtual MerkleTreeNode SiblingOf(SHA256Hash childValue)
		//{
		//	if (Type != TreeNodeType.Branch)
		//		throw new NotSupportedException();

		//	if (Left.Value == childValue)
		//		return Right;

		//	if (Right.Value == childValue)
		//		return Left;

		//	throw new NodeNotFoundException(childValue);
		//}

		/// <summary>
		/// Trims the current node, removing its children and content.
		/// </summary>
		/// <returns>
		/// True if the node was trimmed, false if it was already trimmed
		/// </returns>
        public virtual bool Trim()
        {
            if (Type == TreeNodeType.TrimmedLeaf)
                return false;

            Left = null;
            Right = null;
            Type = TreeNodeType.TrimmedLeaf;
            Content = new byte[0];

            return true;
        }

		///// <summary>
		///// 
		///// </summary>
		//public virtual void Update()
		//{
		//	if (Type != MerkleTreeNodeType.Branch)
		//		return;

		//	if (Left.Type == TreeNodeType.TrimmedLeaf && Right.Type == TreeNodeType.TrimmedLeaf)
		//	{
		//		Left = null;
		//		Right = null;
		//		Type = TreeNodeType.TrimmedLeaf;
		//	}
		//}

        #endregion Manipulation and Walking

        #region Serialization

		/// <summary>
		/// Serializes the Merkle tree node to a byte array
		/// </summary>
		/// <returns>The serialized Merkle tree node</returns>
        public override byte[] ToByteArray()
        {
            using (var ms = new MemoryStream())
            {
                WriteTo(ms);
                return ms.ToArray();
            }
        }

		/// <summary>
		/// Writes the Merkle tree node to a stream
		/// </summary>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
        public override void WriteTo(Stream s)
        {
			if( s == null )
				throw new ArgumentNullException("s");

            s.WriteByte((byte)Type);

            if (Type == TreeNodeType.ContentLeaf)
            {
                VarUInt contentLength = (uint)Content.Length;
                contentLength.WriteTo(s);
                s.Write(Content, 0, Content.Length);
                return;
            }

            if (Type != TreeNodeType.Branch)
            {
                Value.WriteTo(s);
                return;
            }

            Left.WriteTo(s);
            Right.WriteTo(s);
        }

        #endregion Serialization
    }
}