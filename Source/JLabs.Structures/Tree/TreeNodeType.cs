﻿namespace JLabs.Structures.Tree
{
	/// <summary>
	/// The type of a MerkleTreeNode
	/// </summary>
	public enum TreeNodeType : byte
	{
		/// <summary>
		/// The node is a branch, with two children
		/// </summary>
		Branch,

		/// <summary>
		/// The node is an untrimmed leaf with no content
		/// </summary>
		Leaf,

		/// <summary>
		/// The node has been trimmed and has no children
		/// </summary>
		TrimmedLeaf,

		/// <summary>
		/// The node is a leaf and has content
		/// </summary>
		ContentLeaf
	}
}