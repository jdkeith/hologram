﻿using System;
using JLabs.Structures.Cryptography;

namespace JLabs.Structures.Tree
{
	/// <summary>
	/// The exception that is thrown when the given node cannot be found
	/// </summary>
	public class NodeNotFoundException : Exception
	{
		/// <summary>
		/// Creates a new NodeNotFoundException
		/// </summary>
		/// <param name="nodeValue">The value of the node which was not found</param>
		/// <param name="message">An optional message to store with the exception</param>
		public NodeNotFoundException(SHA256Hash nodeValue, string message = null)
			: base(message)
		{
			NodeValue = nodeValue;
		}

		/// <summary>
		/// The value of the node which was not found
		/// </summary>
		public SHA256Hash NodeValue { get; private set; }
	}
}