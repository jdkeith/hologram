﻿using System;
using System.Reflection;

namespace JLabs.Structures
{
	public static class ParserBuilder
	{
		private class BuiltParser
		{
			private readonly Type _t;
			private readonly MethodInfo _parse;
			private readonly MethodInfo _tryParse;

			public BuiltParser(Type t)
			{
				_t = t;

				const BindingFlags flags = BindingFlags.Static | BindingFlags.Public;

				if (_t.IsEnum)
				{
					_parse = typeof (Enum).GetMethod(
						"Parse", flags,
						null, CallingConventions.Standard,
						new[] { typeof(Type), typeof(string), typeof(bool) },
						new ParameterModifier[0]
					);
				}
				else
				{
					_parse = t.GetMethod(
						"Parse", flags,
						null, CallingConventions.Standard,
						new[] {typeof (string)},
						new ParameterModifier[0]
					);

					_tryParse = t.GetMethod(
						"TryParse", flags,
						null, CallingConventions.Standard,
						new[] {typeof (string), t.MakeByRefType()},
						new ParameterModifier[0]
					);
				}
			}

			public bool IsValid
			{
				get { return _parse != null || _tryParse != null; }
			}

			public Type Type
			{
				get { return _t; }
			}

			public void Extract(string input, out bool success, out object result)
			{
				object[] @parms;

				if (_tryParse != null)
				{
					@parms = new object[] {input, null};

					success = (bool) _tryParse.Invoke(null, @parms);

					result = @parms[1];
				}
				else if (_t.IsEnum)
				{
					try
					{
						@parms = new object[] { _t, input, true };
						result = _parse.Invoke(null, @parms);
						success = true;
					}
					catch (Exception ex)
					{
						result = _t.IsValueType ? Activator.CreateInstance(_t) : null;
						success = false;
					}
				}
				else
				{
					try
					{
						@parms = new object[] { input };
						result = _parse.Invoke(null, @parms);
						success = true;
					}
					catch(Exception ex)
					{
						result = _t.IsValueType ? Activator.CreateInstance(_t) : null;
						success = false;
					}
				}
			}
		}

		private class BuiltParser<T> : IParser<T>
		{
			private readonly BuiltParser _parser;

			public BuiltParser(BuiltParser parser)
			{
				_parser = parser;
			}

			public T Parse(string value)
			{
				bool success;
				object result;

				_parser.Extract(value, out success, out result);

				if (success) return (T) result;

				throw new FormatException(string.Format("Value '{0}' could not be parsed to type {1}", value, _parser.Type.FullName));
			}

			public bool TryParse(string value, out T result)
			{
				object innerResult;
				bool success;

				_parser.Extract(value, out success, out innerResult);

				result = (T) innerResult;
				return success;
			}

			public override string ToString()
			{
				return "Built parser for " + _parser.Type.FullName;
			}
		}

		public static IParser<T> BuildFor<T>()
		{
			var builder = new BuiltParser(typeof (T));

			if (! builder.IsValid)
				return null;

			return new BuiltParser<T>(builder);
		}

		public static IParser<object> BuildFor(Type t)
		{
			if( t == null )
				throw new ArgumentNullException("t");

			var builder = new BuiltParser(t);

			if (!builder.IsValid)
				return null;

			return new BuiltParser<object>(builder);
		}
	}
}