﻿using System.IO;

namespace JLabs.Structures
{
	/// <summary>
	/// An interface for objects which can be serialized to byte arrays or streams
	/// </summary>
	public interface ISerializable
	{
		/// <summary>
		/// Serializes the given item to a byte array
		/// </summary>
		/// <returns>The serialized representation of the item</returns>
		byte[] ToByteArray();

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write the item to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		void WriteTo(Stream s);
	}
}
