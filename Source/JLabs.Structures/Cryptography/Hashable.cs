﻿using System;
using System.IO;

namespace JLabs.Structures.Cryptography
{
	public struct ByteSerializable : ISerializable
	{
		private byte[] _bytes;

		public static implicit operator ByteSerializable(byte[] bytes)
		{
			return new ByteSerializable {_bytes = bytes};
		}

		public static implicit operator byte[](ByteSerializable bs)
		{
			return bs._bytes ?? new byte[0];
		}

		public byte[] ToByteArray()
		{
			return _bytes ?? new byte[0];
		}

		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			var b = ToByteArray();

			s.Write(b, 0, b.Length);
		}
	}
}