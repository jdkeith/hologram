﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using JLabs.Structures.Utilities;

namespace JLabs.Structures.Cryptography
{
	/// <summary>
	/// A summary of data or a unique identifier
	/// </summary>
	public static class Hash
	{
		// ReSharper disable InconsistentNaming
		/// <summary>
		/// A SHA256 implementation
		/// </summary>
		public static IHasher<SHA256Hash> SHA256
		{
			get { return SHA256Hash.Factory; }
		}

		/// <summary>
		/// A RIPEMD160 implementation
		/// </summary>
		public static IHasher<RIPEMD160Hash> RIPEMD160
		{
			get { return RIPEMD160Hash.Factory; }
		}
		// ReSharper enable InconsistentNaming
	}

	/// <summary>
	/// A summary of data or a unique identifier
	/// </summary>
	public interface IHash : ISerializable
	{
	}

	/// <summary>
	/// A SHA256 hash implementation
	/// </summary>
	public struct SHA256Hash : IHash, IComparable<SHA256Hash>, IEquatable<SHA256Hash>
	{
		#region Singletons

		private static IComparer<SHA256Hash> DefaultComparerSingleton;
		private static IHasher<SHA256Hash> DefaultFactorySingleton;

		#endregion Singletons

		#region Inner Classes and Singleton Access

		private class SHA256HashComparer : IComparer<SHA256Hash>
		{
			public int Compare(SHA256Hash x, SHA256Hash y)
			{
				return x.CompareTo(y);
			}
		}

		/// <summary>
		/// Gets a SHA256Hash comparer singleton
		/// </summary>
		public static IComparer<SHA256Hash> Comparer
		{
			get { return DefaultComparerSingleton ?? (DefaultComparerSingleton = new SHA256HashComparer()); }
		}

		/// <summary>
		/// Gets a SHA256Hash factory singleton
		/// </summary>
		public static IHasher<SHA256Hash> Factory
		{
			get
			{
				if (DefaultFactorySingleton == null)
				{
					DefaultFactorySingleton = new Hasher<SHA256Hash>(
						Size,
						b => new SHA256Hash(b),
						b => new SHA256Hash(_sha.ComputeHash(b))
						);
				}

				return DefaultFactorySingleton;
			}
		}

		#endregion Inner Classes and Singleton Access

		#region Fields and Constants

		/// <summary>
		/// The number of bytes a SHA256Hash requires to represent
		/// </summary>
		public const int Size = 32;

		private static readonly SHA256Managed _sha;
		private byte[] _value;

		#endregion Fields and Constants

		#region Constructors

		private SHA256Hash(byte[] value)
		{
			_value = value;
		}

		static SHA256Hash()
		{
			_sha = new SHA256Managed();
		}

		#endregion Constructors

		#region Combine

		/// <summary>
		/// Computes the hash of the current hash combined with zero or more items
		/// </summary>
		/// <param name="items">A param array of items to compute a hash of</param>
		/// <returns>A hash of the hash plus all combined hashes</returns>
		/// <remarks>
		/// <para>Items are concatenated into a byte array in the order they're passed before being hashed</para>
		/// <para>The invocant hash is prepended to the list of items before execution</para>
		/// <para>If the items array is empty, the hash itself is returned</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hashes parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any hashes are null
		/// </exception>
		public SHA256Hash CombineWith(params ISerializable[] items)
		{
			if (! items.Any())
				return this;

			return (SHA256Hash) Factory.Combine(
				new ISerializable[] { this }.Concat(items).ToArray()
			);
		}

		#endregion Combine

		#region Serialization

		/// <summary>
		/// Serializes the SHA256Hash to a byte array
		/// </summary>
		/// <returns>The serialized representation of the SHA256Hash</returns>
		public byte[] ToByteArray()
		{
			if (_value == null)
				_value = new byte[Size];

			return (byte[]) _value.Clone();
		}

		/// <summary>
		/// Writes the SHA256Hash to a stream
		/// </summary>
		/// <param name="s">The stream to write the SHA256Hash to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			s.Write(ToByteArray(), 0, Size);
		}

		#endregion Serialization

		#region Comparison and Equality

		/// <summary>
		/// Compares two SHA256Hash instances
		/// </summary>
		/// <param name="a">The first SHA256Hash to compare</param>
		/// <param name="b">The second SHA256Hash to compare</param>
		/// <returns>True if the SHA256Hashs are considered equal, false otherwise</returns>
		public static bool operator ==(SHA256Hash a, SHA256Hash b)
		{
			return a.CompareTo(b) == 0;
		}

		/// <summary>
		/// Compares two SHA256Hash instances
		/// </summary>
		/// <param name="a">The first SHA256Hash to compare</param>
		/// <param name="b">The second SHA256Hash to compare</param>
		/// <returns>True if the SHA256Hashs are considered equal, false otherwise</returns>
		public static bool operator !=(SHA256Hash a, SHA256Hash b)
		{
			return a.CompareTo(b) != 0;
		}

		/// <summary>
		/// Compares two SHA256Hash instances
		/// </summary>
		/// <param name="a">The first SHA256Hash to compare</param>
		/// <param name="b">The second SHA256Hash to compare</param>
		/// <returns>True if the first SHA256Hash is considered greater than the second, false otherwise</returns>
		public static bool operator >(SHA256Hash a, SHA256Hash b)
		{
			return a.CompareTo(b) > 0;
		}

		/// <summary>
		/// Compares two SHA256Hash instances
		/// </summary>
		/// <param name="a">The first SHA256Hash to compare</param>
		/// <param name="b">The second SHA256Hash to compare</param>
		/// <returns>True if the first SHA256Hash is considered less than the second, false otherwise</returns>
		public static bool operator <(SHA256Hash a, SHA256Hash b)
		{
			return a.CompareTo(b) < 0;
		}

		/// <summary>
		/// Compares a SHA256Hash to another SHA256hash
		/// </summary>
		/// <param name="other">The SHA256Hash to compare to</param>
		/// <returns>An integer representing the relative ordering of the invocant to the parameter</returns>
		public int CompareTo(SHA256Hash other)
		{
			for (var i = 0; i < Size; i++)
			{
				var a = _value[i];
				var b = other._value[i];

				if (a == b)
					continue;

				return a.CompareTo(b);
			}

			return 0;
		}

		/// <summary>
		/// Compares a SHA256Hash to another SHA256hash
		/// </summary>
		/// <param name="other">The SHA256Hash to compare to</param>
		/// <returns>True if the invocant is considered equal to the parameter, false otherwise</returns>
		public bool Equals(SHA256Hash other)
		{
			return CompareTo(other) == 0;
		}

		/// <summary>
		/// Compares a SHA256Hash to another object
		/// </summary>
		/// <param name="obj">The object to compare to</param>
		/// <returns>True if the invocant is considered equal to the parameter, false otherwise</returns>
		public override bool Equals(object obj)
		{
			if (!(obj is SHA256Hash))
				return false;

			return Equals((SHA256Hash) obj);
		}

		/// <summary>
		/// Serves as a hash key for the given instance
		/// </summary>
		/// <returns>An integer derived from the data within the instance</returns>
		public override int GetHashCode()
		{
			return ToString().GetHashCode();
		}

		#endregion Comparison and Equality

		#region Conversion

		/// <summary>
		/// Provides a hex string representation of the SHA256Hash
		/// </summary>
		/// <returns>A hex string representation of the SHA256Hash</returns>
		public override string ToString()
		{
			return _value.ToHexString();
		}

		#endregion Conversion
	}

	/// <summary>
	/// A RIPEMD160 hash implementation
	/// </summary>
	public struct RIPEMD160Hash : IHash, IComparable<RIPEMD160Hash>, IEquatable<RIPEMD160Hash>
	{
		#region Singletons

		private static IComparer<RIPEMD160Hash> DefaultComparerSingleton;
		private static IHasher<RIPEMD160Hash> DefaultFactorySingleton;

		#endregion Singletons

		#region Inner Classes and Singleton Access

		private class RIPEMD160HashComparer : IComparer<RIPEMD160Hash>
		{
			public int Compare(RIPEMD160Hash x, RIPEMD160Hash y)
			{
				return x.CompareTo(y);
			}
		}

		/// <summary>
		/// Gets a RIPEMD160Hash comparer singleton
		/// </summary>
		public static IComparer<RIPEMD160Hash> Comparer
		{
			get { return DefaultComparerSingleton ?? (DefaultComparerSingleton = new RIPEMD160HashComparer()); }
		}

		/// <summary>
		/// Gets a RIPEMD160Hash factory singleton
		/// </summary>
		public static IHasher<RIPEMD160Hash> Factory
		{
			get
			{
				if (DefaultFactorySingleton == null)
				{
					DefaultFactorySingleton = new Hasher<RIPEMD160Hash>(
						Size,
						b => new RIPEMD160Hash(b),
						b => new RIPEMD160Hash(_rmd.ComputeHash(b))
						);
				}

				return DefaultFactorySingleton;
			}
		}

		#endregion Inner Classes and Singleton Access

		#region Fields and Constants

		/// <summary>
		/// The number of bytes a RIPEMD160Hash requires to represent
		/// </summary>
		public const int Size = 20;

		private static readonly RIPEMD160Managed _rmd;
		private byte[] _value;

		#endregion Fields and Constants

		#region Constructors

		private RIPEMD160Hash(byte[] value)
		{
			_value = value;
		}

		static RIPEMD160Hash()
		{
			_rmd = new RIPEMD160Managed();
		}

		#endregion Constructors

		#region Combine

		/// <summary>
		/// Computes the hash of the current hash combined with zero or more items
		/// </summary>
		/// <param name="items">A param array of items to compute a hash of</param>
		/// <returns>A hash of the hash plus all combined hashes</returns>
		/// <remarks>
		/// <para>Items are concatenated into a byte array in the order they're passed before being hashed</para>
		/// <para>The invocant hash is prepended to the list of items before execution</para>
		/// <para>If the items array is empty, the hash itself is returned</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hashes parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any hashes are null
		/// </exception>
		public RIPEMD160Hash CombineWith(params ISerializable[] items)
		{
			if (!items.Any())
				return this;

			return (RIPEMD160Hash)Factory.Combine(
				new ISerializable[] { this }.Concat(items).ToArray()
			);
		}

		#endregion Combine

		#region Serialization

		/// <summary>
		/// Serializes the RIPEMD160Hash to a byte array
		/// </summary>
		/// <returns>The serialized representation of the RIPEMD160Hash</returns>
		public byte[] ToByteArray()
		{
			if (_value == null)
				_value = new byte[Size];

			return (byte[])_value.Clone();
		}

		/// <summary>
		/// Writes the RIPEMD160Hash to a stream
		/// </summary>
		/// <param name="s">The stream to write the RIPEMD160Hash to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			s.Write(ToByteArray(), 0, Size);
		}

		#endregion Serialization

		#region Comparison and Equality

		/// <summary>
		/// Compares two RIPEMD160Hash instances
		/// </summary>
		/// <param name="a">The first RIPEMD160Hash to compare</param>
		/// <param name="b">The second RIPEMD160Hash to compare</param>
		/// <returns>True if the RIPEMD160Hash are considered equal, false otherwise</returns>
		public static bool operator ==(RIPEMD160Hash a, RIPEMD160Hash b)
		{
			return a.CompareTo(b) == 0;
		}

		/// <summary>
		/// Compares two SHA256Hash instances
		/// </summary>
		/// <param name="a">The first RIPEMD160Hash to compare</param>
		/// <param name="b">The second RIPEMD160Hash to compare</param>
		/// <returns>True if the RIPEMD160Hash are considered equal, false otherwise</returns>
		public static bool operator !=(RIPEMD160Hash a, RIPEMD160Hash b)
		{
			return a.CompareTo(b) != 0;
		}

		/// <summary>
		/// Compares two SHA256Hash instances
		/// </summary>
		/// <param name="a">The first RIPEMD160Hash to compare</param>
		/// <param name="b">The second RIPEMD160Hash to compare</param>
		/// <returns>True if the first RIPEMD160Hash is considered greater than the second, false otherwise</returns>
		public static bool operator >(RIPEMD160Hash a, RIPEMD160Hash b)
		{
			return a.CompareTo(b) > 0;
		}

		/// <summary>
		/// Compares two RIPEMD160Hash instances
		/// </summary>
		/// <param name="a">The first RIPEMD160Hash to compare</param>
		/// <param name="b">The second RIPEMD160Hash to compare</param>
		/// <returns>True if the first RIPEMD160Hash is considered less than the second, false otherwise</returns>
		public static bool operator <(RIPEMD160Hash a, RIPEMD160Hash b)
		{
			return a.CompareTo(b) < 0;
		}

		/// <summary>
		/// Compares a SHA256Hash to another RIPEMD160Hash
		/// </summary>
		/// <param name="other">The RIPEMD160Hash to compare to</param>
		/// <returns>An integer representing the relative ordering of the invocant to the parameter</returns>
		public int CompareTo(RIPEMD160Hash other)
		{
			for (var i = 0; i < Size; i++)
			{
				var a = _value[i];
				var b = other._value[i];

				if (a == b)
					continue;

				return a.CompareTo(b);
			}

			return 0;
		}

		/// <summary>
		/// Compares a RIPEMD160Hash to another RIPEMD160Hash
		/// </summary>
		/// <param name="other">The RIPEMD160Hash to compare to</param>
		/// <returns>True if the invocant is considered equal to the parameter, false otherwise</returns>
		public bool Equals(RIPEMD160Hash other)
		{
			return CompareTo(other) == 0;
		}

		/// <summary>
		/// Compares a RIPEMD160Hash to another object
		/// </summary>
		/// <param name="obj">The object to compare to</param>
		/// <returns>True if the invocant is considered equal to the parameter, false otherwise</returns>
		public override bool Equals(object obj)
		{
			if (!(obj is RIPEMD160Hash))
				return false;

			return Equals((RIPEMD160Hash)obj);
		}

		/// <summary>
		/// Serves as a hash key for the given instance
		/// </summary>
		/// <returns>An integer derived from the data within the instance</returns>
		public override int GetHashCode()
		{
			return ToString().GetHashCode();
		}

		#endregion Comparison and Equality

		#region Conversion

		/// <summary>
		/// Provides a hex string representation of the RIPEMD160Hash
		/// </summary>
		/// <returns>A hex string representation of the RIPEMD160Hash</returns>
		public override string ToString()
		{
			return _value.ToHexString();
		}

		#endregion Conversion
	}
}