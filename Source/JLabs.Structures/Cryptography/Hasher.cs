﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using JLabs.Structures.Utilities;

// ReSharper disable InconsistentNaming

namespace JLabs.Structures.Cryptography
{
	/// <summary>
	/// Provides methods to 
	/// </summary>
	public interface IHasher
	{
		/// <summary>
		/// Creates a random hash useful for unique identifiers
		/// </summary>
		IHash UID { get; }

		/// <summary>
		/// Computes the hash of a string using UTF-8 encoding
		/// </summary>
		/// <param name="message">The message to compute the hash of</param>
		/// <returns>The hash of the message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message parameter is null
		/// </exception>
		IHash Of(string message);

		/// <summary>
		/// Computes the hash of a byte array
		/// </summary>
		/// <param name="data">The data to compute the hash of</param>
		/// <returns>The hash of the data</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		IHash Of(byte[] data);

		/// <summary>
		/// Computes the hash of a serializable item
		/// </summary>
		/// <param name="data">The item to compute the hash of</param>
		/// <returns>The hash of the item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		IHash Of(ISerializable data);

		/// <summary>
		/// Computes the hash of the bytes on a stream starting from the current position
		/// </summary>
		/// <param name="s">The stream to read from</param>
		/// <returns>The hash of the bytes on the stream starting from the current position</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		IHash Of(Stream s);

		/// <summary>
		/// Computes the hash of one or more hashes
		/// </summary>
		/// <param name="hashes">A param array of hashes to compute a hash of</param>
		/// <returns>A hash of all combined hashes</returns>
		/// <remarks>
		/// Hashes are concatenated into a byte array in the order they're passed before being hashed
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hashes parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any hashes are null
		/// </exception>
		IHash Combine(params IHash[] hashes);

		/// <summary>
		/// Computes the hash of one or more items
		/// </summary>
		/// <param name="items">A param array of items to compute the total has of</param>
		/// <returns>A hash of all combined items</returns>
		/// <remarks>
		/// Items are concatenated into a byte array in the order they're passed before being hashed
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the items parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any items are null
		/// </exception>
		IHash Combine(params ISerializable[] items);

		/// <summary>
		/// Creates a hash message authentication code from the given key and message
		/// </summary>
		/// <param name="key">The key to create an HMAC from</param>
		/// <param name="message">The message to create an HMAC from</param>
		/// <returns>An HMAC for the given key and message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key or message arguments are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the key or message arguments are empty
		/// </exception>
		IHash HMAC(byte[] key, byte[] message);

		/// <summary>
		/// Creates a hash message authentication code from the given key and message
		/// </summary>
		/// <param name="key">The key to create an HMAC from</param>
		/// <param name="message">The message to create an HMAC from</param>
		/// <returns>An HMAC for the given key and message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key or message arguments are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the key is empty or the message produces no bytes when serialized
		/// </exception>
		IHash HMAC(byte[] key, ISerializable message);

		/// <summary>
		/// The minimum value for the given hash type
		/// </summary>
		IHash MinValue { get; }

		/// <summary>
		/// The maximum value for the given hash type
		/// </summary>
		IHash MaxValue { get; }

		/// <summary>
		/// The type of hash which the hasher produces
		/// </summary>
		Type HashType { get; }

		/// <summary>
		/// Gets the number of bytes in the given hash implementation
		/// </summary>
		int HashSize { get; }
	}

	/// <summary>
	/// Provides methods to create, parse, and deserialize hashes
	/// </summary>
	/// <typeparam name="H">The type of hash to create</typeparam>
	public interface IHasher<H> : IHasher, IDeserializerParser<H> where H : IHash
	{
		/// <summary>
		/// Creates a random hash useful for unique identifiers
		/// </summary>
		new H UID { get; }

		/// <summary>
		/// Computes the hash of a string using UTF-8 encoding
		/// </summary>
		/// <param name="message">The message to compute the hash of</param>
		/// <returns>The hash of the message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message parameter is null
		/// </exception>
		new H Of(string message);

		/// <summary>
		/// Computes the hash of a byte array
		/// </summary>
		/// <param name="data">The data to compute the hash of</param>
		/// <returns>The hash of the data</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		new H Of(byte[] data);

		/// <summary>
		/// Computes the hash of a serializable item
		/// </summary>
		/// <param name="data">The item to compute the hash of</param>
		/// <returns>The hash of the item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		new H Of(ISerializable data);

		/// <summary>
		/// Computes the hash of the bytes on a stream starting from the current position
		/// </summary>
		/// <param name="s">The stream to read from</param>
		/// <returns>The hash of the bytes on the stream starting from the current position</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		new H Of(Stream s);

		/// <summary>
		/// Computes the hash of one or more hashes
		/// </summary>
		/// <param name="hashes">A param array of hashes to compute a hash of</param>
		/// <returns>A hash of all combined hashes</returns>
		/// <remarks>
		/// Hashes are concatenated into a byte array in the order they're passed before being hashed
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hashes parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any hashes are null
		/// </exception>
		new H Combine(params IHash[] hashes);

		H HMAC(byte[] key, byte[] message);

		/// <summary>
		/// The minimum value for the given hash type
		/// </summary>
		new H MinValue { get; }

		/// <summary>
		/// The maximum value for the given hash type
		/// </summary>
		new H MaxValue { get; }
	}

	internal class Hasher
	{
		protected static readonly RandomNumberGenerator _rng = new RNGCryptoServiceProvider();
	}

	internal class Hasher<H> : Hasher, IHasher<H> where H : IHash
	{
		private readonly int _size;
		private readonly Func<byte[], H> Deserializer;
		private readonly Func<byte[], H> Computer;

		public Hasher(int size, Func<byte[], H> deserializer, Func<byte[], H> computer)
		{
			_size = size;
			Deserializer = deserializer;
			Computer = computer;
		}

		public H UID
		{
			get
			{
				var data = new byte[_size];
				_rng.GetBytes(data);

				return Deserialize(data);
			}
		}

		public H Parse(string hex)
		{
			if (hex == null)
				throw new ArgumentNullException("hex");

			if (hex.Length != _size * 2)
				throw new FormatException(
					string.Format("Hex string must be {0} characters long", _size * 2)
				);

			return Deserialize(hex.FromHexString());
		}

		public bool TryParse(string hex, out H hash)
		{
			try
			{
				hash = Parse(hex);
				return true;
			}
			catch
			{
				hash = MinValue;
				return false;
			}
		}

		public H Of(ISerializable data)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			return Computer(data.ToByteArray());
		}

		public H Of(Stream s)
		{
			if( s == null )
				throw new ArgumentNullException("s");

			const int bufferSize = 65536;
			var buffer = new byte[bufferSize];

			using (var ms = new MemoryStream())
			{
				int read;

				while ((read = s.Read(buffer, 0, bufferSize)) > 0)
				{
					ms.Write(buffer, 0, read);
				}

				return Of(ms.ToArray());
			}
		}

		public H Combine(params IHash[] hashes)
		{
			if (hashes == null)
				throw new ArgumentNullException("hashes");

			if (hashes.Length == 0)
				throw new ArgumentException("Hash must be of one or more items", "hashes");

			using (var ms = new MemoryStream())
			{
				foreach (var hash in hashes)
				{
					if (hash == null)
						throw new ArgumentException("No parameters may be null", "hashes");

					hash.WriteTo(ms);
				}

				return Of(ms.ToArray());
			}
		}

		public H Combine(params ISerializable[] items)
		{
			if (items == null)
				throw new ArgumentNullException("items");

			if (items.Length == 0)
				throw new ArgumentException("Hash must be of one or more items", "items");

			using (var ms = new MemoryStream())
			{
				foreach (var item in items)
				{
					if (item == null)
						throw new ArgumentException("No parameters may be null", "items");

					item.WriteTo(ms);
				}

				return Of(ms.ToArray());
			}
		}

		public H HMAC(byte[] key, byte[] message)
		{
			if (key == null)
				throw new ArgumentNullException("key");

			if (message == null)
				throw new ArgumentNullException("message");

			if (! key.Any())
				throw new ArgumentException("Key parameter is empty array", "key");

			if (!message.Any())
				throw new ArgumentException("Message parameter is empty array", "message");

			ByteSerializable kbs = key;
			ByteSerializable mbs = message;

			return Combine(kbs, Combine(kbs, mbs));
		}

		public H MinValue
		{
			get { return Deserialize(new byte[_size]); }
		}

		public H MaxValue
		{
			get { return Deserialize(Enumerable.Repeat(byte.MaxValue, _size).ToArray()); }
		}

		public H Deserialize(byte[] serialized)
		{
			if (serialized == null)
				throw new ArgumentNullException("serialized");

			return Deserializer(serialized);
		}

		public H ReadFrom(Stream s)
		{
			if( s == null )
				throw new ArgumentNullException("s");

			var br = new BinaryReader(s);

			return Deserialize(br.ReadBytes(_size));
		}

		#region IHasher Implementation

		IHash IHasher.UID
		{
			get { return UID; }
		}

		public H Of(string message)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return Of(Encoding.UTF8.GetBytes(message));
		}

		IHash IHasher.Of(string message)
		{
			return Of(message);
		}

		public H Of(byte[] data)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			return Computer(data);
		}

		IHash IHasher.Of(ISerializable data)
		{
			return Of(data);
		}

		IHash IHasher.Of(Stream s)
		{
			return Of(s);
		}

		IHash IHasher.Combine(params IHash[] hashes)
		{
			return Combine(hashes);
		}

		IHash IHasher.Combine(params ISerializable[] items)
		{
			return Combine(items);
		}

		IHash IHasher.HMAC(byte[] key, byte[] message)
		{
			return HMAC(key, message);
		}

		IHash IHasher.HMAC(byte[] key, ISerializable message)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return HMAC(key, message.ToByteArray());
		}

		IHash IHasher.MinValue
		{
			get { return MinValue; }
		}

		IHash IHasher.MaxValue
		{
			get { return MaxValue; }
		}

		IHash IHasher.Of(byte[] data)
		{
			return Of(data);
		}

		Type IHasher.HashType
		{
			get { return typeof (H); }
		}

		int IHasher.HashSize
		{
			get { return _size; }
		}

		#endregion IHasher Implementation
	}
}