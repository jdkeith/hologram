﻿using System.IO;

namespace JLabs.Structures
{
	/// <summary>
	/// An interface for deserializing objects from byte arrays or streams
	/// </summary>
	/// <typeparam name="T">The type of object to be deserialized</typeparam>
	public interface IDeserializer<out T> where T : ISerializable
	{
		/// <summary>
		/// Deserializes an object from a byte array
		/// </summary>
		/// <param name="serialized">The byte array to deserialize an object from</param>
		/// <returns>The object represented by the byte array</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the serialized parameter is null
		/// </exception>
		T Deserialize(byte[] serialized);

		/// <summary>
		/// Reads an object from a steam
		/// </summary>
		/// <param name="s">The stream to read the object from</param>
		/// <returns>The object within the stream</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		T ReadFrom(Stream s);
	}

	/// <summary>
	/// An interface for parsing string representations of objects
	/// </summary>
	/// <typeparam name="T">The type of object to be parsed</typeparam>
	public interface IParser<T>
	{
		/// <summary>
		/// Extracts an item from a string representation
		/// </summary>
		/// <param name="value">The string to parse</param>
		/// <returns>The item represented by the string</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the string parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if the string parameter does not represent an item of the return type
		/// </exception>
		T Parse(string value);

		/// <summary>
		/// Attempts to extract an item from a string representation
		/// </summary>
		/// <param name="value">The string to parse</param>
		/// <param name="result">
		/// The extracted item or the default value of the item if extraction was unsuccessful
		/// </param>
		/// <returns>True if the hash was successfully extracted, false otherwise</returns>
		bool TryParse(string value, out T result);
	}

	/// <summary>
	/// An interface for deserializing and parsing items
	/// </summary>
	/// <typeparam name="T">The type of object to be parsed</typeparam>
	public interface IDeserializerParser<T> : IDeserializer<T>, IParser<T> where T : ISerializable
	{
	}
}